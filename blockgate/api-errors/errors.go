package apierrors

import (
	"net/http"
	"runtime/debug"

	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/status"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Error represents an error with an associated HTTP status code.
type Error struct {
	Cause  error  `json:"-"`                // The raw errors.Error.
	Code   string `json:"code,omitempty"`   // An application-specific error code, expressed as a string value.
	Source string `json:"source,omitempty"` // The source module of the error - blockcore | blockgate
	Status int32  `json:"status,omitempty"` // The HTTP status code applicable to this problem, expressed as a string value.
	Title  string `json:"title,omitempty"`  // A short, human-readable summary of the problem. It SHOULD NOT change from occurrence to occurrence of the problem.
	Detail string `json:"detail,omitempty"` // A human-readable explanation specific to this occurrence of the problem.
}

// NewFromStatusErr returns an apierror.Error from a fabric SDK status.Status error.
func NewFromStatusErr(statusErr *status.Status) *Error {
	return &Error{
		Cause:  statusErr,
		Detail: statusErr.Message,
		Source: "blockcore",
		Status: statusErr.Code,
		Title:  statusErr.Group.String(),
	}
}

// APIError is an interface for our custom errors so we cna spot them.
type APIError interface {
	Error() string
	MarshalLogObject(zapcore.ObjectEncoder) error
	StackTrace() errors.StackTrace
	StatusCode() int
}

type stackTracer interface {
	StackTrace() errors.StackTrace
}

// Error allows Error to satisfy the default error interface.
func (e *Error) Error() string {
	return e.Cause.Error()
}

// StackTrace returns the error stacktrace and satisfies the errors.Stacktrace interface.
func (e *Error) StackTrace() errors.StackTrace {
	var cause stackTracer
	if ok := errors.As(e.Cause, &cause); ok {
		return cause.StackTrace()
	}

	return nil
}

// StatusCode returns the status code and satisfies the Go Kit StatusCoder interface.
func (e *Error) StatusCode() int {
	return int(e.Status)
}

// MarshalLogObject satisfies the zap logger ObjectEncoder interface to log error details.
func (e *Error) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	// Don't do via json marshalling for performance
	if e.Code != "" {
		enc.AddString("code", e.Code)
	}

	if e.Status != 0 {
		enc.AddInt32("status", e.Status)
	}

	if e.Title != "" {
		enc.AddString("title", e.Title)
	}

	if e.Detail != "" {
		enc.AddString("detail", e.Detail)
	}

	return nil
}

// LogError checks for an error and then logs the details of the apierrors.Error, along with the stacktrace.
func LogError(logger *zap.Logger, msg string, err error) {
	if err != nil {
		var apiErr APIError
		if ok := errors.As(err, &apiErr); ok {
			logger.Error(msg, zap.Object("error", apiErr))
		} else {
			logger.Error(msg, zap.String("error", err.Error()))
		}

		DebugTrace(logger, err)
	}
}

// DebugTrace outputs the stacktrace of the error to the supplied logger.
func DebugTrace(logger *zap.Logger, err error) {
	var e stackTracer
	if ok := errors.As(err, &e); ok {
		len := 6

		st := e.StackTrace()
		cap := cap(st)

		if cap < len {
			len = cap
		}

		logger.Sugar().Debugf("------------ StackTrace ------------%+v", st[0:len])
	}
}

// PanicTrace outputs the stacktrace of the panic to the supplied logger.
func PanicTrace(logger *zap.Logger) {
	st := string(debug.Stack())
	logger.Sugar().Debugf("------------ Panic StackTrace ------------\n%v", st)
}

func newError(err error, status int32) *Error {
	return &Error{
		Cause:  err,
		Source: "blockgate",
		Status: status,
		Title:  err.Error(),
	}
}

// UserIDRequired returns a specific StatusUnauthorized error with user id required message.
func UserIDRequired() *Error {
	err := errors.New("userID is required")

	return UserAuthentication(err)
}

// UserGroupsRequired returns a specific StatusUnauthorized error with user groups required message.
func UserGroupsRequired() *Error {
	err := errors.New("userGroups is required")

	return UserAuthentication(err)
}

// UserAuthentication returns a formatted StatusUnauthorized error.
func UserAuthentication(err error) *Error {
	err = errors.Wrap(err, "user authentication failed")

	return newError(err, http.StatusUnauthorized)
}

// BadRequest returns a formatted StatusBadRequest error.
func BadRequest() *Error {
	err := errors.New("request is not valid")

	return newError(err, http.StatusBadRequest)
}

// BadRequestWrap returns a formatted StatusBadRequest error with original error.
func BadRequestWrap(err error) *Error {
	err = errors.Wrap(err, "request is not valid")

	return newError(err, http.StatusBadRequest)
}

// InternalServerError returns a formatted StatusInternalServerError error.
func InternalServerError(err error) *Error {
	err = errors.WithStack(err)

	return newError(err, http.StatusInternalServerError)
}

// Panic returns a generic StatusInternalServerError error.
func Panic() *Error {
	err := errors.New(http.StatusText(http.StatusInternalServerError))

	return newError(err, http.StatusInternalServerError)
}
