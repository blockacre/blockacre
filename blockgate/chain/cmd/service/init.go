package service

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	kitPrometheus "github.com/go-kit/kit/metrics/prometheus"
	group "github.com/oklog/oklog/pkg/group"
	opentracinggo "github.com/opentracing/opentracing-go"
	zipkinot "github.com/openzipkin-contrib/zipkin-go-opentracing"
	zipkin "github.com/openzipkin/zipkin-go"
	zipkinhttp "github.com/openzipkin/zipkin-go/reporter/http"
	errors "github.com/pkg/errors"
	prometheus "github.com/prometheus/client_golang/prometheus"
	zap "go.uber.org/zap"
	zapcore "go.uber.org/zap/zapcore"
)

func initLogger() (*zap.Logger, error) {
	config := zap.NewDevelopmentConfig()
	// config := zap.NewProductionConfig()
	config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	config.DisableCaller = true
	config.DisableStacktrace = true

	logger, err := config.Build()
	if err != nil {
		return nil, errors.Wrap(err, "initLogger")
	}

	return logger, nil
}

func initTracer(zipkinURL string) (tracer opentracinggo.Tracer, err error) {
	if zipkinURL != "" {
		// set up a span reporter
		reporter := zipkinhttp.NewReporter(zipkinURL)
		defer reporter.Close() //nolint:errcheck
		// create our local service endpoint
		endpoint, err := zipkin.NewEndpoint("chain", "localhost:80")
		if err != nil {
			return nil, errors.Wrap(err, "unable to create local endpoint")
		}
		// initialize our tracer
		nativeTracer, err := zipkin.NewTracer(reporter, zipkin.WithLocalEndpoint(endpoint))
		if err != nil {
			return nil, errors.Wrap(err, "unable to create tracer")
		}
		// use zipkin-go-opentracing to wrap our tracer
		tracer = zipkinot.Wrap(nativeTracer)
	} else {
		tracer = opentracinggo.GlobalTracer()
	}

	return tracer, nil
}

func initCancelInterrupt(g *group.Group) {
	cancelInterrupt := make(chan struct{})

	g.Add(func() error {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		select {
		case sig := <-c:
			return fmt.Errorf("received signal %s", sig)
		case <-cancelInterrupt:
			return nil
		}
	}, func(error) {
		close(cancelInterrupt)
	})
}

func initPrometheus() (counter *kitPrometheus.Counter, duration *kitPrometheus.Summary) {
	fieldKeys := []string{"method", "success"}
	counter = kitPrometheus.NewCounterFrom(prometheus.CounterOpts{
		Namespace: "BlockConnect",
		Subsystem: "chain",
		Name:      "request_count",
		Help:      "Number of requests received.",
	}, fieldKeys)
	duration = kitPrometheus.NewSummaryFrom(prometheus.SummaryOpts{
		Help:      "Request duration in seconds.",
		Name:      "request_duration_seconds",
		Namespace: "BlockConnect",
		Subsystem: "chain",
	}, fieldKeys)

	return counter, duration
}
