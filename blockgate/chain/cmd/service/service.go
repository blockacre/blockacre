package service

import (
	"flag"
	"net"
	"net/http"
	"os"

	group "github.com/oklog/oklog/pkg/group"
	promhttp "github.com/prometheus/client_golang/prometheus/promhttp"
	chainEp "gitlab.com/blockacre/blockcore/blockgate/chain/pkg/endpoint"
	chainHTTP "gitlab.com/blockacre/blockcore/blockgate/chain/pkg/http"
	chainSvc "gitlab.com/blockacre/blockcore/blockgate/chain/pkg/service"
	zap "go.uber.org/zap"
)

// Define flags.
var (
	fs        = flag.NewFlagSet("chain", flag.ExitOnError)
	debugAddr = fs.String("debug.addr", ":8080", "Debug and metrics listen address")
	httpAddr  = fs.String("http-addr", ":8081", "HTTP listen address")
	hydrate   = fs.Bool("hydrate", false, "If set, 4 users will be registered in the Fabric CA.")
	zipkinURL = fs.String("zipkin-url", "", "Enable Zipkin tracing via a collector URL e.g. http://localhost:9411/api/v1/spans")
)

// Run starts and runs the service.
func Run() {
	err := fs.Parse(os.Args[1:])
	if err != nil {
		panic(err)
	}

	// logger will be passed to all components that use it, as a dependency
	logger, err := initLogger()
	if err != nil {
		panic(err)
	}
	defer logger.Sync() //nolint:errcheck

	// tracer will be passed to all components that use it, as a dependency
	tracer, err := initTracer(*zipkinURL)
	if err != nil {
		logger.Fatal(err.Error())
	}

	// Create new chain service
	svc, err := chainSvc.New(logger.Named("service"))
	if err != nil {
		logger.Fatal(err.Error())
	}
	defer svc.Close() //nolint:errcheck

	// Create chain endpoints
	counter, duration := initPrometheus()
	endpoints := chainEp.New(svc, logger.Named("endpoint"), counter, duration)

	// Setup a run group
	g := &group.Group{}

	// Initialize HTTP Handler
	httpListener, err := net.Listen("tcp", *httpAddr)
	if err != nil {
		logger.Fatal(err.Error())
	}

	httpHandler := chainHTTP.NewHTTPHandler(endpoints, logger.Named("http"), tracer)

	g.Add(func() error {
		logger.Info("listening", zap.String("HTTP", *httpAddr))

		return http.Serve(httpListener, withSecureHTTPHeaders(httpHandler))
	}, func(error) {
		httpListener.Close() //nolint:errcheck,gosec
	})

	// Initialize Metrics Endpoint Handler
	debugListener, err := net.Listen("tcp", *debugAddr)
	if err != nil {
		logger.Fatal(err.Error())
	}

	http.DefaultServeMux.Handle("/metrics", promhttp.Handler())

	g.Add(func() error {
		logger.Info("listening", zap.String("debug/HTTP", *debugAddr))

		return http.Serve(debugListener, withSecureHTTPHeaders(http.DefaultServeMux))
	}, func(error) {
		debugListener.Close() //nolint:errcheck,gosec
	})

	if *hydrate {
		hydrateUsers(svc)
	}

	initCancelInterrupt(g)
	logger.Warn("exit", zap.Error(g.Run()))
}
