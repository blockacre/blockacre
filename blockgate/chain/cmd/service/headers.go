package service

import "net/http"

func withSecureHTTPHeaders(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.Header().Set("X-API-VERSION", "BLOCKGATEV1")

		// Add CORS when needed
		// w.Header().Set("Access-Control-Allow-Origin", "*")
		// w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
		// w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")
		// if r.Method == "OPTIONS" {
		// 	return
		// }

		handler.ServeHTTP(w, r)
	})
}
