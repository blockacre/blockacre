package service

import (
	"context"

	chainSvc "gitlab.com/blockacre/blockcore/blockgate/chain/pkg/service"
	"gitlab.com/blockacre/blockcore/blockgate/chain/pkg/types"
)

//nolint
func hydrateUsers(svc chainSvc.ChainService) {
	svc.WalletCreate(context.Background(), types.WalletCreateRequest{UserGroups: []string{"guest"}, UserID: "guest"})
	svc.WalletCreate(context.Background(), types.WalletCreateRequest{UserGroups: []string{"admin"}, UserID: "admin1"})
	svc.WalletCreate(context.Background(), types.WalletCreateRequest{UserGroups: []string{"investor"}, UserID: "investor1"})
	svc.WalletCreate(context.Background(), types.WalletCreateRequest{UserGroups: []string{"investor"}, UserID: "investor2"})
	svc.WalletCreate(context.Background(), types.WalletCreateRequest{UserGroups: []string{"investor"}, UserID: "investor3"})
	svc.WalletCreate(context.Background(), types.WalletCreateRequest{UserGroups: []string{"vendor"}, UserID: "vendor1"})
	svc.WalletCreate(context.Background(), types.WalletCreateRequest{UserGroups: []string{"vendor"}, UserID: "vendor2"})
	svc.WalletCreate(context.Background(), types.WalletCreateRequest{UserGroups: []string{"vendor"}, UserID: "vendor3"})
}
