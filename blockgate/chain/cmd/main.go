package main

import "gitlab.com/blockacre/blockcore/blockgate/chain/cmd/service"

func main() {
	service.Run()
}
