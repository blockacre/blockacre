package types

// Args defines the type needed by the invoke function for args.
type Args = map[string]interface{}

// InvokeRequest collects the request parameters for the GET and POST Invoke methods.
type InvokeRequest struct {
	Args       Args     `json:"args"`
	Execute    bool     `json:"execute"`
	Function   string   `json:"function"`
	UserGroups []string `json:"userGroups"` // Invoke doesn't directly use UserGroups but needs them in case the user doesn't yet exist
	UserID     string   `json:"userID"`
}

// WalletCreateRequest collects the request parameters for the WalletCreate method.
type WalletCreateRequest struct {
	UserGroups []string `json:"userGroups"`
	UserID     string   `json:"userID"`
}
