package http

import (
	"context"
	"encoding/json"
	"net/http"

	kitEp "github.com/go-kit/kit/endpoint"
	"github.com/gorilla/mux"
	apierrors "gitlab.com/blockacre/blockcore/blockgate/api-errors"
	"gitlab.com/blockacre/blockcore/blockgate/chain/pkg/types"
	"gitlab.com/blockacre/blockcore/blockgate/defaults"
)

// ************************** Request Decoders **************************

// decodeInvokePOSTRequest is a transport/http.DecodeRequestFunc that decodes a JSON-encoded request from the HTTP request body.
func decodeInvokePOSTRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req types.InvokeRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, apierrors.InternalServerError(err)
	}

	req.Function = mux.Vars(r)["function"]

	return req, nil
}

// decodeWalletCreateRequest is a transport/http.DecodeRequestFunc that decodes a JSON-encoded request from the HTTP request body.
func decodeWalletCreateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req types.WalletCreateRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, apierrors.InternalServerError(err)
	}

	return req, nil
}

// EncodeJSONResponse is a transport/http.EncodeResponseFunc that encodes the response as JSON to the response writer.
func EncodeJSONResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	var (
		contentType = "application/json; charset=utf-8"
		statusCode  = http.StatusOK
		respData    = response
		respErr     error
	)

	switch resp := response.(type) {
	case defaults.Response:
		// Check if the response is already our custom response type and split resp data and err
		respData = resp.Data
		respErr = resp.Err
	case kitEp.Failer:
		// Maybe it still implements the Go Kit failer interface
		if err := resp.Failed(); err != nil {
			respErr = err
		}
	case error:
		// Maybe it's a normal error
		respErr = resp
	}

	// If there's an error, send to the default error encoder and exit
	if respErr != nil {
		defaults.EncodeError(ctx, respErr, w)

		return nil
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(statusCode)

	return json.NewEncoder(w).Encode(defaults.Response{
		Data: respData,
		Err:  respErr,
	})
}
