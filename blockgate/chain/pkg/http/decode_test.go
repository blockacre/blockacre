package http_test

import (
	"context"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	apierrors "gitlab.com/blockacre/blockcore/blockgate/api-errors"
	chainHTTP "gitlab.com/blockacre/blockcore/blockgate/chain/pkg/http"
	"gitlab.com/blockacre/blockcore/blockgate/defaults"
)

type encJSONTest struct {
	Msg       string
	Resp      interface{}
	ExpBody   string
	ExpStatus int
}

var encJSONTests = []encJSONTest{
	{
		"defaults.Response shape without error",
		newDefResp(data, nil), `{"data":{"someField":"somevalue"}}`,
		http.StatusOK,
	},
	{
		"defaults.Response shape with known apierrors.BadRequest error",
		newDefResp(data, badReq),
		`{"error":{"source":"blockgate","status":400,"title":"request is not valid"}}`,
		http.StatusBadRequest,
	},

	{
		"defaults.Response shape with known apierros.InternalServer error",
		newDefResp(data, intErr),
		`{"error":{"source":"blockgate","status":500,"title":"__TEST__"}}`,
		http.StatusInternalServerError,
	},

	{
		"defaults.Response shape with unknown error without statusCoder",
		newDefResp(data, errors.New("__TEST1__")),
		`{"error":{"source":"blockgate","status":500,"title":"__TEST1__"}}`,
		http.StatusInternalServerError,
	},
	{
		"Unknown shape but implements go kit failer interface with statusCoder",
		newUnkResp(badReq),
		`{"error":{"source":"blockgate","status":400,"title":"request is not valid"}}`,
		http.StatusBadRequest,
	},
	{
		"Unknown shape but implements go kit failer interface without statusCoderr",
		newUnkResp(errors.New("__TEST2__")),
		`{"error":{"source":"blockgate","status":500,"title":"__TEST2__"}}`,
		http.StatusInternalServerError,
	},
	{
		"No shape, just an error, doesn't implement failer",
		errors.New("__TEST3__"),
		`{"error":{"source":"blockgate","status":500,"title":"__TEST3__"}}`,
		http.StatusInternalServerError,
	},
	{
		"No shape, just an error, implements statusCoder",
		badReq,
		`{"error":{"source":"blockgate","status":400,"title":"request is not valid"}}`,
		http.StatusBadRequest,
	},
	{
		"Unknown shape, not an error with no error present",
		"Unknown response type",
		`{"data": "Unknown response type"}`,
		http.StatusOK,
	},
}

type UnknownResponse struct {
	Err error
}

func (r UnknownResponse) Failed() error {
	return r.Err
}

var data = map[string]interface{}{
	"someField": "somevalue",
}

var (
	badReq = apierrors.BadRequest()
	intErr = apierrors.InternalServerError(errors.New("__TEST__"))
)

func newDefResp(data interface{}, err error) defaults.Response {
	return defaults.Response{Data: data, Err: err}
}

func newUnkResp(err error) UnknownResponse {
	return UnknownResponse{Err: err}
}

func TestEncodeJSONResponse(t *testing.T) {
	for _, test := range encJSONTests {
		t.Log(test.Msg)

		w := httptest.NewRecorder()
		ctx := context.Background()
		err := chainHTTP.EncodeJSONResponse(ctx, w, test.Resp)
		assert.NoError(t, err)

		result := w.Result()
		defer result.Body.Close()
		body, _ := ioutil.ReadAll(result.Body)
		assert.Equal(t, test.ExpStatus, result.StatusCode)
		assert.JSONEq(t, test.ExpBody, string(body))
	}
}
