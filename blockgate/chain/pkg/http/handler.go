package http

import (
	"net/http"

	kitHTTP "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	opentracinggo "github.com/opentracing/opentracing-go"
	"gitlab.com/blockacre/blockcore/blockgate/chain/pkg/endpoint"
	"gitlab.com/blockacre/blockcore/blockgate/defaults"
	"go.uber.org/zap"
)

// NewHTTPHandler returns a router with the defined routes for chain.
func NewHTTPHandler(endpoints endpoint.Endpoints, logger *zap.Logger, tracer opentracinggo.Tracer) http.Handler {
	invokeHandler := kitHTTP.NewServer(endpoints.InvokeEndpoint, decodeInvokePOSTRequest, EncodeJSONResponse, defaults.HTTPOptions("invoke", logger, tracer)...)
	walletCreateHandler := kitHTTP.NewServer(endpoints.WalletCreateEndpoint, decodeWalletCreateRequest, EncodeJSONResponse, defaults.HTTPOptions("walletCreate", logger, tracer)...)

	router := mux.NewRouter()
	router.Handle("/walletcreate", walletCreateHandler).Methods("POST")
	router.Handle("/{function}", invokeHandler).Methods("POST")

	return router
}
