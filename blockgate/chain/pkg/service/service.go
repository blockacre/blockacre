package service

import (
	"context"
	"os"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
	"github.com/pkg/errors"
	"gitlab.com/blockacre/blockcore/blockgate/chain/pkg/types"
	"go.uber.org/zap"
)

// Env is the configuration needed to set up the Fabric SDK & CA.
type Env struct {
	ChannelID   string
	ChaincodeID string
	CAName      string
}

// ChainService describes the service.
type ChainService interface {
	Close() error
	Invoke(ctx context.Context, request types.InvokeRequest) (*channel.Response, error)
	WalletCreate(ctx context.Context, request types.WalletCreateRequest) (*channel.Response, error)
}

type basicChainService struct {
	caClient *msp.Client
	env      *Env
	logger   *zap.Logger
	sdk      *fabsdk.FabricSDK
}

func (b *basicChainService) Close() error {
	b.sdk.Close()

	return nil
}

// NewBasicChainService returns a naive, stateless implementation of ChainService.
func NewBasicChainService(logger *zap.Logger) (ChainService, error) {
	envConfig := Env{
		ChannelID:   os.Getenv("APP_CHANNEL_NAME"),
		ChaincodeID: os.Getenv("APP_CHAINCODE_ID"),
		CAName:      os.Getenv("APP_CANAME"),
	}

	// Init the fabric SDK with our network config
	networkConfig := config.FromFile("/profiles/" + os.Getenv("APP_CONNECTION_PROFILE"))

	sdk, err := fabsdk.New(networkConfig)
	if err != nil {
		return nil, errors.Wrap(err, "NewBasicChainService new fabsdk")
	}

	// Init the msp client
	caClient, err := msp.New(sdk.Context())
	if err != nil {
		return nil, errors.Wrap(err, "NewBasicChainService new msp")
	}

	b := &basicChainService{
		caClient: caClient,
		env:      &envConfig,
		logger:   logger,
		sdk:      sdk,
	}

	return b, nil
}

// New returns a ChainService with all of the expected middleware wired in.
func New(logger *zap.Logger) (ChainService, error) {
	svc, err := NewBasicChainService(logger)
	if err != nil {
		return nil, err
	}

	return NewServiceLogging(logger)(svc), nil
}
