package service

import (
	"context"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	apierrors "gitlab.com/blockacre/blockcore/blockgate/api-errors"
	"gitlab.com/blockacre/blockcore/blockgate/chain/pkg/types"
	"go.uber.org/zap"
)

// Middleware describes a service (as opposed to endpoint) middleware.
type Middleware func(ChainService) ChainService

type serviceLogging struct {
	logger *zap.Logger
	next   ChainService
}

// NewServiceLogging takes a logger as a dependency and returns a service Middleware.
func NewServiceLogging(logger *zap.Logger) Middleware {
	return func(next ChainService) ChainService {
		return &serviceLogging{logger, next}
	}
}

func (mw *serviceLogging) Close() (err error) {
	// No need to log anything for close
	return mw.next.Close()
}

func (mw *serviceLogging) Invoke(ctx context.Context, req types.InvokeRequest) (resp *channel.Response, err error) {
	defer func() {
		apierrors.LogError(mw.logger, "invoke", err)

		if panic := recover(); panic != nil {
			apierrors.PanicTrace(mw.logger.Named("invoke"))
			err = apierrors.Panic()
		}
	}()

	return mw.next.Invoke(ctx, req)
}

func (mw *serviceLogging) WalletCreate(ctx context.Context, req types.WalletCreateRequest) (resp *channel.Response, err error) {
	defer func() {
		apierrors.LogError(mw.logger, "walletcreate", err)

		if panic := recover(); panic != nil {
			apierrors.PanicTrace(mw.logger.Named("walletcreate"))
			err = apierrors.Panic()
		}
	}()

	return mw.next.WalletCreate(ctx, req)
}
