package service

import (
	"context"
	"os"
	"strings"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
	"github.com/pkg/errors"
	apierrors "gitlab.com/blockacre/blockcore/blockgate/api-errors"
	"gitlab.com/blockacre/blockcore/blockgate/chain/pkg/types"
)

// WalletCreate registers a user with the CA client and then creates their wallet on the ledger.
func (b *basicChainService) WalletCreate(ctx context.Context, req types.WalletCreateRequest) (*channel.Response, error) {
	if req.UserGroups == nil || len(req.UserGroups) == 0 {
		return nil, apierrors.UserGroupsRequired()
	}
	// Save the user groups from Cognito as attribues in the MSP (joined here, is split again later in the chaincode)
	attrs := []msp.Attribute{
		{
			Name:  "ba.groups",
			Value: strings.Join(req.UserGroups, ","),
			ECert: true,
		},
	}

	// Register the user with the CA
	secret, err := b.caClient.Register(&msp.RegistrationRequest{
		Name:           req.UserID,
		Type:           os.Getenv("APP_REGISTER_USER_TYPE"),
		MaxEnrollments: -1,
		Affiliation:    os.Getenv("APP_REGISTER_AFFILIATION"),
		Attributes:     attrs,
		CAName:         b.env.CAName,
	})
	if err != nil {
		return nil, apierrors.UserAuthentication(err)
	}
	// NOCOMMIT - use this to get log passwords in case they're needed
	// fmt.Println(secret)

	err = b.caClient.Enroll(req.UserID, msp.WithSecret(secret))
	if err != nil {
		return nil, apierrors.UserAuthentication(err)
	}

	reqInvoke := types.InvokeRequest{
		Execute:    true,
		Function:   "walletcreate",
		UserGroups: req.UserGroups,
		UserID:     req.UserID,
	}

	// Add the user to the ledger
	response, err := b.Invoke(ctx, reqInvoke)
	// If there is an error adding to the ledger, remove the user from the CA
	if err != nil {
		_, removalErr := b.caClient.RemoveIdentity(&msp.RemoveIdentityRequest{
			ID:    req.UserID,
			Force: true,
		})
		if removalErr != nil {
			// Return both initial invoke error appended with the removalError
			err = errors.WithMessage(err, removalErr.Error())
		}
		// Removal was successful, return just the initial error from invoke
		return nil, errors.Wrap(err, "WalletCreate")
	}

	return response, nil
}
