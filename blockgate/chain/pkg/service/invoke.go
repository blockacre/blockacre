package service

import (
	"context"
	"encoding/json"
	goErrors "errors"
	"net/http"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	clientMsp "github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/status"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
	"github.com/pkg/errors"
	apierrors "gitlab.com/blockacre/blockcore/blockgate/api-errors"
	"gitlab.com/blockacre/blockcore/blockgate/chain/pkg/types"
	"go.uber.org/zap"
)

func (b *basicChainService) Invoke(ctx context.Context, req types.InvokeRequest) (*channel.Response, error) {
	signingIdentity, err := b.getIdentity(ctx, req)
	if err != nil {
		return nil, err
	}

	// Setup the channel context using the current userID - this also sets up the signingIdentity in the context
	clientChannelContext := b.sdk.ChannelContext(
		b.env.ChannelID,
		fabsdk.WithIdentity(signingIdentity),
	)

	channelClient, err := channel.New(clientChannelContext)
	if err != nil {
		return nil, apierrors.InternalServerError(err)
	}

	args, err := convertArgs(req.Args)
	if err != nil {
		return nil, apierrors.InternalServerError(err)
	}

	// Setup parameters for calls to the ledger
	request := channel.Request{
		ChaincodeID: b.env.ChaincodeID,
		Fcn:         req.Function,
		Args:        args,
	}
	options := channel.WithRetry(retry.DefaultChannelOpts)

	// Decide which function to use, Query or Execute
	var fn func(request channel.Request, options ...channel.RequestOption) (channel.Response, error)
	if req.Execute {
		fn = channelClient.Execute

		b.logger.Debug("execute", zap.Any("req", req))
	} else {
		fn = channelClient.Query
		b.logger.Debug("query", zap.Any("req", req))
	}

	// Call the ledger
	result, chainErr := fn(request, options)

	// Handle the error and convert to our own api error
	if chainErr != nil {
		statusErr, ok := status.FromError(chainErr)
		if ok {
			err = apierrors.NewFromStatusErr(statusErr)
		} else {
			err = &apierrors.Error{
				Status: http.StatusBadGateway,
				Detail: chainErr.Error(),
				Cause:  chainErr,
			}
		}

		return nil, err
	}

	return &result, nil
}

func (b *basicChainService) getIdentity(ctx context.Context, req types.InvokeRequest) (msp.SigningIdentity, error) {
	if req.UserID == "" {
		return nil, apierrors.UserIDRequired()
	}

	// First try
	signingIdentity, err := b.caClient.GetSigningIdentity(req.UserID)
	if err != nil {
		if !isNotFoundErr(err) {
			// If the error is not a usernotfound error
			return nil, apierrors.InternalServerError(err)
		}
		// User was not found in the CA so we can try to create them
		ucReq := types.WalletCreateRequest{
			UserGroups: req.UserGroups,
			UserID:     req.UserID,
		}

		// Call the walletcreate function which will add the user to the CA and create a wallet on the ledger
		if _, err := b.WalletCreate(ctx, ucReq); err != nil {
			return nil, err
		}
		// If the walletcreate function was successful, then try getting the identity again (but just return an error this time instead of re-trying to create the user)
		// Second try
		signingIdentity, err = b.caClient.GetSigningIdentity(req.UserID)
		if err != nil {
			return nil, apierrors.UserAuthentication(err)
		}
	}

	// NOCOMMIT - can use this to log certs to use for testing
	// fmt.Println(string(signingIdentity.EnrollmentCertificate()))
	return signingIdentity, nil
}

func isNotFoundErr(err error) bool {
	cause := errors.Cause(err)
	// Error normally comes from the clientMsp but include the other msp here too
	// Check both the error and the cause for sure-ity
	return goErrors.Is(cause, clientMsp.ErrUserNotFound) ||
		goErrors.Is(cause, msp.ErrUserNotFound) ||
		goErrors.Is(err, clientMsp.ErrUserNotFound) ||
		goErrors.Is(err, msp.ErrUserNotFound)
}

func convertArgs(args map[string]interface{}) ([][]byte, error) {
	if len(args) == 0 {
		return nil, nil
	}

	argsAsBytes, err := json.Marshal(args)
	if err != nil {
		return nil, apierrors.BadRequestWrap(err)
	}

	return [][]byte{argsAsBytes}, nil
}
