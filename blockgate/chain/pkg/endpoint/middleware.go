package endpoint

import (
	"context"
	"fmt"
	"time"

	kitEp "github.com/go-kit/kit/endpoint"
	kitMetrics "github.com/go-kit/kit/metrics"
	apierrors "gitlab.com/blockacre/blockcore/blockgate/api-errors"
	zap "go.uber.org/zap"
)

// endpointInstrumenting returns an endpoint middleware that records
// the duration of each invocation to the passed histogram. The middleware adds
// a single field: "success", which is "true" if no error is returned, and
// "false" otherwise.
func endpointInstrumenting(endpointName string, counter kitMetrics.Counter, duration kitMetrics.Histogram) kitEp.Middleware {
	return func(next kitEp.Endpoint) kitEp.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			defer func(begin time.Time) {
				counter.With("method", endpointName, "success", fmt.Sprint(err == nil)).Add(1)
				duration.With("method", endpointName, "success", fmt.Sprint(err == nil)).Observe(time.Since(begin).Seconds())
			}(time.Now())

			return next(ctx, request)
		}
	}
}

// endpointLogging returns an endpoint middleware that logs the
// duration of each invocation, and the resulting error, if any.
func endpointLogging(endpointName string, logger *zap.Logger) kitEp.Middleware {
	return func(next kitEp.Endpoint) kitEp.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			defer func(begin time.Time) {
				if err != nil {
					logger.Error(endpointName, zap.Error(err), zap.Duration("took", time.Since(begin)))
					apierrors.DebugTrace(logger, err)
				} else {
					logger.Info(endpointName, zap.Error(err), zap.Duration("took", time.Since(begin)))
				}

				logger.Debug(endpointName, zap.Any("request", request))
				logger.Debug(endpointName, zap.Any("response", response))
			}(time.Now())

			return next(ctx, request)
		}
	}
}
