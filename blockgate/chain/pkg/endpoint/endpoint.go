package endpoint

import (
	"context"
	"encoding/json"

	kitEp "github.com/go-kit/kit/endpoint"
	kitMetrics "github.com/go-kit/kit/metrics"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	apierrors "gitlab.com/blockacre/blockcore/blockgate/api-errors"
	"gitlab.com/blockacre/blockcore/blockgate/chain/pkg/service"
	"gitlab.com/blockacre/blockcore/blockgate/chain/pkg/types"
	"gitlab.com/blockacre/blockcore/blockgate/defaults"
	zap "go.uber.org/zap"
)

// Endpoints collects all of the endpoints that compose a profile service.
// It's meant to be used as a helper struct, to collect all of the endpoints into a single parameter.
type Endpoints struct {
	InvokeEndpoint       kitEp.Endpoint
	WalletCreateEndpoint kitEp.Endpoint
}

// MakeInvokeEndpoint returns an endpoint that invokes Invoke on the service.
func MakeInvokeEndpoint(s service.ChainService) kitEp.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(types.InvokeRequest)
		if !ok {
			return shaper(nil, apierrors.BadRequest()), nil
		}

		return shaper(s.Invoke(ctx, req)), nil
	}
}

// MakeWalletCreateEndpoint returns an endpoint that invokes WalletCreate on the service.
func MakeWalletCreateEndpoint(s service.ChainService) kitEp.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(types.WalletCreateRequest)
		if !ok {
			return shaper(nil, apierrors.BadRequest()), nil
		}

		return shaper(s.WalletCreate(ctx, req)), nil
	}
}

func shaper(resp *channel.Response, err error) *defaults.Response {
	// Convert the service response and errors here so that we don't trigger an endpoint middleware error log
	if err != nil {
		return &defaults.Response{Err: err}
	}

	var data interface{}
	if err := json.Unmarshal(resp.Payload, &data); err != nil {
		// If there is an err in unmarshalling, it's because the resp.Payload is not in json form, probably just a string
		data = string(resp.Payload)
	}

	return &defaults.Response{Data: data}
}

// New returns an Endpoints struct that wraps the provided service, and wires in all of the expected endpoint middlewares.
func New(s service.ChainService, logger *zap.Logger, counter kitMetrics.Counter, duration kitMetrics.Histogram) Endpoints {
	invoke := kitEp.Chain(
		endpointInstrumenting("invoke", counter, duration),
		endpointLogging("invoke", logger),
	)(MakeInvokeEndpoint(s))

	walletcreate := kitEp.Chain(
		endpointInstrumenting("walletcreate", counter, duration),
		endpointLogging("walletcreate", logger),
	)(MakeWalletCreateEndpoint(s))

	eps := Endpoints{
		InvokeEndpoint:       invoke,
		WalletCreateEndpoint: walletcreate,
	}

	return eps
}
