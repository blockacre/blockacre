#!/bin/sh
set -e

echo "Running blockgate tests..."

mkdir -p coverage
go test ./... -outputdir=coverage -coverprofile=coverage.out

echo
echo "***************************"
echo "**** Function Coverage ****"
echo "***************************"
echo

go tool cover -func coverage/coverage.out