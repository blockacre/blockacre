module gitlab.com/blockacre/blockcore/blockgate

go 1.14

require (
	github.com/cactus/go-statsd-client/statsd v0.0.0-20200423205355-cb0885a1018c // indirect
	github.com/cloudflare/cfssl v0.0.0-20180323000720-5d63dbd981b5 // indirect; needs to be this version to prevent "invalid operation: kr == nil" error
	github.com/go-kit/kit v0.10.0
	github.com/google/certificate-transparency-go v1.1.0 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/hyperledger/fabric-sdk-go v0.0.0-20181024114358-42e17991cdc4 // needs to be this version to prevent "invalid operation: kr == nil" error
	github.com/m3db/prometheus_client_golang v0.8.1 // indirect
	github.com/m3db/prometheus_client_model v0.1.0 // indirect
	github.com/m3db/prometheus_common v0.1.0 // indirect
	github.com/m3db/prometheus_procfs v0.8.1 // indirect
	github.com/oklog/oklog v0.3.2
	github.com/opentracing/opentracing-go v1.1.0
	github.com/openzipkin-contrib/zipkin-go-opentracing v0.4.5
	github.com/openzipkin/zipkin-go v0.2.2
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.3.0
	github.com/stretchr/testify v1.4.0
	github.com/uber-go/tally v3.3.16+incompatible // indirect
	go.uber.org/zap v1.13.0
)
