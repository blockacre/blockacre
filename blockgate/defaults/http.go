package defaults

import (
	"context"

	kitZap "github.com/go-kit/kit/log/zap"
	kitOpentracing "github.com/go-kit/kit/tracing/opentracing"
	kitHTTP "github.com/go-kit/kit/transport/http"
	opentracinggo "github.com/opentracing/opentracing-go"
	apierrors "gitlab.com/blockacre/blockcore/blockgate/api-errors"
	zap "go.uber.org/zap"
	zapcore "go.uber.org/zap/zapcore"
)

// Response describes the shape we will always return from this service.
type Response struct {
	Data interface{} `json:"data,omitempty"`
	Err  error       `json:"error,omitempty"`
}

// Failed implements Go Kit Failer.
func (r Response) Failed() error {
	return r.Err
}

// LogErrorHandler is a transport error handler implementation which logs an error.
// It is used for logging errors from the transport layer (e.g. in Decode request etc.).
type LogErrorHandler struct {
	logger *zap.Logger
}

// Handle enables us to satisfy the interface go-kit expects.
func (h *LogErrorHandler) Handle(ctx context.Context, err error) {
	h.logger.Error(err.Error())
	apierrors.DebugTrace(h.logger, err)
}

func newLogErrorHandler(logger *zap.Logger) *LogErrorHandler {
	return &LogErrorHandler{logger}
}

// HTTPOptions returns a set of default options as well as setting up the http logger.
func HTTPOptions(endpointName string, logger *zap.Logger, tracer opentracinggo.Tracer) []kitHTTP.ServerOption {
	kitLogger := kitZap.NewZapSugarLogger(logger.Named("tracing"), zapcore.ErrorLevel)

	return []kitHTTP.ServerOption{
		kitHTTP.ServerBefore(kitOpentracing.HTTPToContext(tracer, endpointName, kitLogger)),
		// Send server errors to the logger
		kitHTTP.ServerErrorHandler(newLogErrorHandler(logger)),
		kitHTTP.ServerErrorEncoder(EncodeError),
	}
}
