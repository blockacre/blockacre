package defaults_test

import (
	"context"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	apierrors "gitlab.com/blockacre/blockcore/blockgate/api-errors"
	"gitlab.com/blockacre/blockcore/blockgate/defaults"
)

func TestEncodeError(t *testing.T) {
	type test struct {
		Msg       string
		Err       error
		ExpBody   string
		ExpStatus int
	}

	tests := []test{
		{
			"Plain Go Error, no statusCoder",
			errors.New("__TEST__"),
			`{"error":{"source":"blockgate","status":500,"title":"__TEST__"}}`,
			http.StatusInternalServerError,
		},
		{
			"API Error, custom status code",
			apierrors.BadRequest(),
			`{"error":{"source":"blockgate","status":400,"title":"request is not valid"}}`,
			http.StatusBadRequest,
		},
		{
			"API Error, 500 status",
			apierrors.InternalServerError(errors.New("__TEST__")),
			`{"error":{"source":"blockgate","status":500,"title":"__TEST__"}}`,
			http.StatusInternalServerError,
		},
	}

	for _, test := range tests {
		t.Log(test.Msg)

		w := httptest.NewRecorder()
		ctx := context.Background()
		defaults.EncodeError(ctx, test.Err, w)

		result := w.Result()
		defer result.Body.Close()
		body, _ := ioutil.ReadAll(result.Body)
		assert.Equal(t, test.ExpStatus, result.StatusCode)
		assert.JSONEq(t, test.ExpBody, string(body))
	}
}

func TestEncodeErrorPanics(t *testing.T) {
	t.Log("Should Panic")

	w := httptest.NewRecorder()
	ctx := context.Background()

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	defaults.EncodeError(ctx, nil, w)
}
