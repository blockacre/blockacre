package defaults

import (
	"context"
	"encoding/json"
	"net/http"

	kitHTTP "github.com/go-kit/kit/transport/http"
	"github.com/pkg/errors"
	apierrors "gitlab.com/blockacre/blockcore/blockgate/api-errors"
)

// EncodeError ensures the correct server response in case of errors.
func EncodeError(_ context.Context, err error, w http.ResponseWriter) {
	var statusCode int

	if err == nil {
		panic("EncodeError with nil error")
	}

	var errWithSC kitHTTP.StatusCoder
	if ok := errors.As(err, &errWithSC); ok {
		// Error has its own statusCode so use it.
		// Leave err alone because it is of the correct form already
		statusCode = errWithSC.StatusCode()
	} else {
		// Error has unknown status code, create a new internal server error
		newErr := apierrors.InternalServerError(err)
		statusCode = newErr.StatusCode()
		err = newErr
	}

	w.Header().Set("Content-Type", "application/problem+json; charset=utf-8")
	w.WriteHeader(statusCode)

	err = json.NewEncoder(w).Encode(Response{
		Err: err,
	})
	if err != nil {
		panic("Error writing error response")
	}
}
