package ledger_test

import (
	"math/big"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestCounter(t *testing.T) {
	const limit = 25
	one := big.NewInt(1)
	stub := testtools.InitEmptyStub(t)
	stub.MockTransactionStart("__TEST_TX__")

	for i := 1; i <= limit; i++ {
		_, err := ledger.CounterAdd(stub, records.WalletCounter, "", one)
		if err != nil {
			t.Fatal(err)
		}
		countBytes, err := ledger.GetRaw(stub, false, records.WalletCounter, "")
		if err != nil {
			t.Fatal(err)
		}
		expCount := new(big.Int).Mul(one, big.NewInt(int64(i)))
		assert.Equal(t, expCount.String(), string(countBytes))
	}
	testtools.LogEntireState(t, stub)
	for i := limit; i >= 1; i-- {
		_, err := ledger.CounterSub(stub, records.WalletCounter, "", one)
		if err != nil {
			t.Fatal(err)
		}
		countBytes, err := ledger.GetRaw(stub, false, records.WalletCounter, "")
		if err != nil {
			t.Fatal(err)
		}
		expCount := new(big.Int).Mul(one, big.NewInt(int64(i-1)))
		assert.Equal(t, expCount.String(), string(countBytes))
	}
	testtools.LogEntireState(t, stub)
}
