package ledger

import (
	"math/big"

	"github.com/hyperledger/fabric-chaincode-go/shim"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

// counterGet is used to get the running total of various elements in the system, e.g running total of orders and wallet IDs.
func counterGet(stub shim.ChaincodeStubInterface, objectType records.DocType, id string) (*big.Int, error) {
	countBytes, err := GetRaw(stub, false, objectType, id)
	if err != nil {
		return nil, err
	}
	if countBytes == nil {
		countBytes = []byte("0")
	}
	count, ok := new(big.Int).SetString(string(countBytes), 10)
	if !ok {
		return nil, ccerrors.FormatBigInt()
	}

	return count, err
}

// counterPut is used to update the running total of various elements in the system, e.g running total of orders and wallet IDs.
func counterPut(stub shim.ChaincodeStubInterface, objectType records.DocType, id string, newCount *big.Int) (*big.Int, error) {
	newCountBytes, rawErr := newCount.MarshalText()
	if rawErr != nil {
		return nil, ccerrors.FormatMarshal(rawErr)
	}
	if err := PutRaw(stub, objectType, id, newCountBytes); err != nil {
		return nil, err
	}

	return newCount, nil
}

// CounterAdd an integer to the count of the given objectType and ID.
func CounterAdd(stub shim.ChaincodeStubInterface, objectType records.DocType, id string, delta *big.Int) (*big.Int, error) {
	// Get current count.
	count, err := counterGet(stub, objectType, id)
	if err != nil {
		return nil, err
	}
	// Add to count.
	newCount := new(big.Int).Add(count, delta)
	// Commit new count.
	return counterPut(stub, objectType, id, newCount)
}

// CounterSub subtracts an integer from the count of the given objectType and ID.
func CounterSub(stub shim.ChaincodeStubInterface, objectType records.DocType, id string, delta *big.Int) (*big.Int, error) {
	// Get current count.
	count, err := counterGet(stub, objectType, id)
	if err != nil {
		return nil, err
	}
	// Subtract from count.
	newCount := new(big.Int).Sub(count, delta)
	// Commit new count.
	return counterPut(stub, objectType, id, newCount)
}
