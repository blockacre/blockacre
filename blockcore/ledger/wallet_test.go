package ledger_test

import (
	"encoding/json"
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestGenerateNewWalletRef(t *testing.T) {
	stub := testtools.InitEmptyStub(t)
	stub.MockTransactionStart("__TEST_TX__")

	for _, test := range testdata.WalletRefs {
		ref, err := ledger.WalletNewRef(stub)
		if err != nil {
			t.Fatal(err)
		}
		assert.Equal(t, test, ref)
	}
}

func TestCreateWallet(t *testing.T) {
	type test struct {
		msg     string
		creator string
		product bool
	}
	tests := []test{
		{
			msg:     "Test creation with regular user",
			creator: testdata.CertInvestor1,
			product: false,
		},
		{
			msg:     "Test product wallet creation (should use txid as index)",
			creator: testdata.CertVendor1,
			product: true,
		},
		{
			msg:     "Test 2nd product wallet creation with same product owner (should use txid as index)",
			creator: testdata.CertVendor1,
			product: true,
		},
		{
			msg:     "Test 2nd creation with empty owner",
			creator: testdata.CertInvestor2,
			product: false,
		},
	}
	stub := testtools.InitEmptyStub(t)

	for i, test := range tests {
		txID := fake.CharactersN(24)
		stub.MockTransactionStart(txID)
		t.Log(test.msg)
		testtools.SetCreator(t, stub, test.creator)
		w, err := ledger.WalletCreate(stub, test.product)
		assert.NoError(t, err)
		t.Log("Check ledger state is stored under correct key and is as expected")
		var wal assets.Wallet
		json.Unmarshal(w, &wal)
		expFn := testdata.ExpWalletNew
		if test.product {
			expFn = testdata.ExpWalletNewProduct
		}
		testtools.CheckState(t, stub, expFn(*wal.Default, testdata.WalletRefs[i]), &wal)
		stub.MockTransactionEnd(txID)
	}
}

func TestCreateWalletFailures(t *testing.T) {
	type test struct {
		msg     string
		creator string
		product bool
	}
	tests := []test{
		{
			msg:     "Test duplicate creation with empty owner fails",
			creator: testdata.CertInvestor1,
			product: false,
		},
		{
			msg:     "Test duplicate product wallet creation fails",
			creator: testdata.CertVendor1,
			product: true,
		},
	}
	stub := testtools.InitEmptyStub(t)
	stub.MockTransactionStart("__TEST_TX__")

	for _, test := range tests {
		t.Log(test.msg)
		testtools.SetCreator(t, stub, test.creator)
		_, err := ledger.WalletCreate(stub, test.product)
		assert.NoError(t, err)
		_, err = ledger.WalletCreate(stub, test.product)
		assert.Error(t, err)
	}
}
