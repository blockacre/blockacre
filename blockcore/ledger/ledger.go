package ledger

import (
	"bytes"
	"encoding/json"

	"github.com/hyperledger/fabric-chaincode-go/shim"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

// CreateKey used for creating composite keys.
func CreateKey(stub shim.ChaincodeStubInterface, objectType records.DocType, attrs []string) (string, error) {
	key, rawErr := stub.CreateCompositeKey(objectType.String(), attrs)
	if rawErr != nil {
		return "", ccerrors.StubKey(rawErr)
	}

	return key, nil
}

// GetRawWithKey uses stub.GetState to get data by supplied key.
// Setting `errNF` to `true` returns an error if the request is good but resource is not found (404).
func GetRawWithKey(stub shim.ChaincodeStubInterface, errNF bool, key string) ([]byte, error) {
	result, rawErr := stub.GetState(key)
	if rawErr != nil {
		return nil, ccerrors.StubGetState(rawErr)
	}
	if errNF && result == nil {
		objectType, ids, err := stub.SplitCompositeKey(key)
		if err != nil {
			return nil, ccerrors.StubKey(err)
		}

		return nil, ccerrors.StubGetStateNF(objectType, ids)
	}

	return result, nil
}

// GetRaw creates a key and calls GetRawWithKey.
// GetRawWithKey uses stub.GetState to get data by supplied key.
// Setting `errNF` to `true` returns an error if the request is good but resource is not found (404).
func GetRaw(stub shim.ChaincodeStubInterface, errNF bool, objectType records.DocType, id string) ([]byte, error) {
	key, err := CreateKey(stub, objectType, []string{id})
	if err != nil {
		return nil, err
	}

	return GetRawWithKey(stub, errNF, key)
}

// GetObject creates a key, calls GetRawWithKey and then Unmarshals the bytes result on to the supplied objectPointer.
// GetRawWithKey uses stub.GetState to get data by supplied key.
// Returns a 404 error if the object is not found.
func GetObject(stub shim.ChaincodeStubInterface, objectPointer records.Record) error {
	key, err := CreateKey(stub, objectPointer.GetObjectType(), objectPointer.GetKeyAttrs())
	if err != nil {
		return err
	}
	resultAsByte, err := GetRawWithKey(stub, true, key)
	if err != nil {
		return err
	}
	if rawErr := json.Unmarshal(resultAsByte, objectPointer); rawErr != nil {
		return ccerrors.FormatMarshal(rawErr)
	}

	return nil
}

// PutRawWithKey uses stub.Putstate to put data on the ledger, using the supplied key.
func PutRawWithKey(stub shim.ChaincodeStubInterface, key string, value []byte) error {
	rawErr := stub.PutState(key, value)
	if rawErr != nil {
		return ccerrors.StubPutState(rawErr)
	}

	return nil
}

// PutRaw creates a key and calls PutRawWithKey.
// PutRawWithKey uses stub.Putstate to put data on the ledger, using the supplied key.
func PutRaw(stub shim.ChaincodeStubInterface, objectType records.DocType, id string, value []byte) error {
	key, err := CreateKey(stub, objectType, []string{id})
	if err != nil {
		return err
	}

	return PutRawWithKey(stub, key, value)
}

// PutObject converts an object in to bytes and creates a key from the object's GetObjectType and GetKeyAttrs methods, then calls PutRawWithKey.
// PutRawWithKey uses stub.Putstate to put data on the ledger, using the created key.
func PutObject(stub shim.ChaincodeStubInterface, object records.Record) ([]byte, error) {
	objectAsByte, rawErr := json.Marshal(object)
	if rawErr != nil {
		return nil, ccerrors.FormatMarshal(rawErr)
	}
	key, err := CreateKey(stub, object.GetObjectType(), object.GetKeyAttrs())
	if err != nil {
		return nil, err
	}

	return objectAsByte, PutRawWithKey(stub, key, objectAsByte)
}

// DeleteObject uses stub.DelState to delete data by id.
func DeleteObject(stub shim.ChaincodeStubInterface, object records.Record) error {
	key, err := CreateKey(stub, object.GetObjectType(), object.GetKeyAttrs())
	if err != nil {
		return err
	}
	if rawErr := stub.DelState(key); rawErr != nil {
		return ccerrors.StubDelState(rawErr)
	}

	return nil
}

// Query uses stub.GetQueryResult for rich couchdb queries.
// Beware that querying state does NOT create a readset so phantom writes can occur.
func Query(stub shim.ChaincodeStubInterface, q string) ([]byte, error) {
	iter, rawErr := stub.GetQueryResult(q)
	if rawErr != nil {
		return nil, ccerrors.StubGetQueryResult(rawErr)
	}

	return ConstructArrayResults(iter)
}

// ConstructArrayResults returns the results of a state query iterator in the form of a json array (in a []byte).
func ConstructArrayResults(iter shim.StateQueryIteratorInterface) ([]byte, error) {
	defer iter.Close() //nolint:errcheck
	var buf bytes.Buffer
	buf.WriteRune('[')
	isFirst := true
	for iter.HasNext() {
		kv, rawErr := iter.Next()
		if rawErr != nil {
			return nil, ccerrors.Iter(rawErr)
		}
		if !isFirst {
			buf.WriteRune(',')
		}
		isFirst = false
		buf.Write(kv.GetValue())
	}
	buf.WriteRune(']')

	return buf.Bytes(), nil
}
