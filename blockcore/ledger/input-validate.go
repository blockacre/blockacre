package ledger

import (
	"encoding/json"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
)

type inputInterface interface {
	IsInput()
}

// InputValidate takes raw args and returns input object.
func InputValidate(args []string, inputPointer inputInterface) error {
	// Validate args
	if len(args) != 1 {
		return ccerrors.NumberOfArguments()
	}

	// Unmarshal into inputPointer
	if err := json.Unmarshal([]byte(args[0]), inputPointer); err != nil {
		return ccerrors.FormatMarshal(err)
	}

	return nil
}
