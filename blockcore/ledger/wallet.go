package ledger

import (
	"fmt"
	"math/big"

	"github.com/hyperledger/fabric-chaincode-go/pkg/cid"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/speps/go-hashids"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

// walletGet returns a wallet object, given a userID or productID.
func walletGet(stub shim.ChaincodeStubInterface, id string) (*assets.Wallet, error) {
	w := &assets.Wallet{
		Default: &records.Default{
			CreatedBy: id,
			DocType:   records.Wallet,
		},
	}
	if err := GetObject(stub, w); err != nil {
		return nil, err
	}

	return w, nil
}

// WalletGetByRef looks up a userID from the given wallet reference then returns the result from walletGet.
// walletGet returns a wallet object, given a userID.
func WalletGetByRef(stub shim.ChaincodeStubInterface, ref string) (*assets.Wallet, error) {
	b, err := GetRaw(stub, true, records.WalletIndex, ref)
	if err != nil {
		return nil, err
	}

	return walletGet(stub, string(b))
}

// WalletGetThisUser returns the wallet of the currently logged in user (the creator).
func WalletGetThisUser(stub shim.ChaincodeStubInterface) (*assets.Wallet, error) {
	userID, rawErr := cid.GetID(stub)
	if rawErr != nil {
		return nil, ccerrors.UserAuthentication(rawErr)
	}

	return walletGet(stub, userID)
}

// WalletCreate creates a Blockacre 'wallet' on our  Does NOT create an identity wallet.
// Accepts a 'product' bool. True sets the record key to the record ID and false sets it to the CreatedBy property.
// Increments Wallet Counter, Commits Wallet RefToID Index, Commits Wallet.
func WalletCreate(stub shim.ChaincodeStubInterface, product bool) ([]byte, error) {
	// Setup recordDefaults.
	recordDefaults, err := records.New(stub, records.StatusActive, records.Wallet)
	if err != nil {
		return nil, errors.Wrap(err, "WalletCreate")
	}

	// Create the new wallet object.
	newWallet := &assets.Wallet{
		Default: recordDefaults,
		Product: product,
	}
	key := newWallet.GetPrimaryKey()

	// Check that a wallet with this ID doesn't already exist.
	// We're checking for the LACK of an error here.
	if err := walletExists(stub, key); err == nil {
		return nil, ccerrors.WalletExists()
	}

	// Generate wallet reference.
	ref, err := WalletNewRef(stub)
	if err != nil {
		return nil, err
	}
	newWallet.Ref = ref

	// Put Wallet Ref -> Wallet Key lookup map.
	if err = PutRaw(stub, records.WalletIndex, ref, []byte(key)); err != nil {
		return nil, err
	}
	// Put the new wallet.
	return PutObject(stub, newWallet)
}

// walletExists returns an error if a wallet does not yet exist.
func walletExists(stub shim.ChaincodeStubInterface, id string) error {
	_, err := GetRaw(stub, true, records.Wallet, id)
	if err != nil {
		return ccerrors.WalletNotExists(err)
	}

	return nil
}

// WalletStore is a temporary cache for storing wallets during transactions.
type WalletStore struct {
	stub    shim.ChaincodeStubInterface
	wallets map[string]*assets.Wallet
}

// WalletNewStore returns a temporary wallet store used when creating orders.
func WalletNewStore(stub shim.ChaincodeStubInterface) *WalletStore {
	return &WalletStore{stub, make(map[string]*assets.Wallet)}
}

// Get calls walletGet and stores the result in the walletstore.
func (s WalletStore) Get(id string) (*assets.Wallet, error) {
	_, ok := s.wallets[id]
	if !ok {
		w, err := walletGet(s.stub, id)
		if err != nil {
			return nil, err
		}
		s.wallets[id] = w
	}

	return s.wallets[id], nil
}

// GetThisUser gets the current user's ID and calls Get.
// Get calls Get and stores the result in the walletstore.
func (s WalletStore) GetThisUser() (*assets.Wallet, error) {
	userID, rawErr := cid.GetID(s.stub)
	if rawErr != nil {
		return nil, ccerrors.UserAuthentication(rawErr)
	}

	return s.Get(userID)
}

// Put calls putObject for each wallet in the walletstore.
func (s WalletStore) Put() error {
	for _, wallet := range s.wallets {
		if _, err := PutObject(s.stub, wallet); err != nil {
			return err
		}
	}

	return nil
}

// WalletNewRef generates a wallet reference from its integer incremented counter value.
func WalletNewRef(stub shim.ChaincodeStubInterface) (string, error) {
	count, err := CounterAdd(stub, records.WalletCounter, "", big.NewInt(1))
	if err != nil {
		return "", err
	}

	hd := hashids.NewData()
	hd.MinLength = 5
	hd.Alphabet = "BLOKACRE1234567890"
	h, rawErr := hashids.NewWithData(hd)
	if rawErr != nil {
		return "", ccerrors.Generic(rawErr)
	}
	ref, rawErr := h.Encode([]int{int(count.Int64())})
	if rawErr != nil {
		return "", ccerrors.Generic(rawErr)
	}

	return fmt.Sprintf("BA%s", ref), nil
}
