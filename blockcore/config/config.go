package config

import (
	"github.com/shopspring/decimal"
)

const (
	// Precision defines the number of decimal places used in some calculations (typically division operations).
	Precision int32 = 9
)

// MinimumWithdrawal is the minimum withdrawable amount.
var MinimumWithdrawal = decimal.RequireFromString("5.00") // 5 pounds
