package contracts

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/types"
)

func (t *BlockCore) get(stub shim.ChaincodeStubInterface, args []string, auth rbac.AuthServiceInterface) ([]byte, error) {
	// Get Input Object
	var input types.GetInput
	if err := ledger.InputValidate(args, &input); err != nil {
		return nil, errors.Wrap(err, "get")
	}

	key, err := ledger.CreateKey(stub, input.ObjectType, input.Keys)
	if err != nil {
		return nil, errors.Wrap(err, "get")
	}

	return ledger.GetRawWithKey(stub, true, key)
}
