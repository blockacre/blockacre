package contracts

import (
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/records"
)

var adminCTPerms = rbac.ContractPermissions{
	contractPartial:          true,
	contractGet:              true,
	contractQuery:            true,
	contractWalletcreate:     true,
	contractDeposit:          true,
	contractWithdraw:         false,
	contractOrdercreate:      false,
	contractProductcreate:    false,
	contractProductupdate:    false,
	contractOrderissuecreate: false,
	contractYield:            false,
}

var adminQPerms = rbac.QueryPermissions{
	records.Order.String():         ruleAllow,
	records.OrderBook.String():     ruleAllow,
	records.Product.String():       ruleAllow,
	records.Transaction.String():   ruleAllow,
	records.Transfer.String():      ruleAllow,
	records.Wallet.String():        ruleAllow,
	records.WalletCounter.String(): ruleAllow,
	records.WalletIndex.String():   ruleAllow,
}

var vendorCTPerms = rbac.ContractPermissions{
	contractPartial:          false,
	contractGet:              false,
	contractQuery:            true,
	contractWalletcreate:     true,
	contractDeposit:          false,
	contractWithdraw:         false,
	contractOrdercreate:      false,
	contractProductcreate:    true,
	contractProductupdate:    true,
	contractOrderissuecreate: true,
	contractYield:            true,
}

var vendorQPerms = rbac.QueryPermissions{
	records.Order.String():       ruleAllow,
	records.OrderBook.String():   ruleAllow,
	records.Product.String():     ruleAllow,
	records.Transaction.String(): ruleInTransaction,
	records.Transfer.String():    ruleInTransfer,
	records.Wallet.String():      ruleOwner,
}

var investorCTPerms = rbac.ContractPermissions{
	contractPartial:          false,
	contractGet:              false,
	contractQuery:            true,
	contractWalletcreate:     true,
	contractDeposit:          false,
	contractWithdraw:         true,
	contractOrdercreate:      true,
	contractProductcreate:    false,
	contractProductupdate:    false,
	contractOrderissuecreate: false,
	contractYield:            false,
}

var investorQPerms = rbac.QueryPermissions{
	records.Order.String():       ruleAllow,
	records.OrderBook.String():   ruleAllow,
	records.Product.String():     ruleAllow,
	records.Transaction.String(): ruleInTransaction,
	records.Transfer.String():    ruleInTransfer,
	records.Wallet.String():      ruleOwner,
}

var guestCTPerms = rbac.ContractPermissions{
	contractQuery: true,
}

var guestQPerms = rbac.QueryPermissions{
	records.Order.String():     ruleAllow,
	records.OrderBook.String(): ruleAllow,
	records.Product.String():   ruleAllow,
}

var (
	roleAdmin    = "admin"
	roleVendor   = "vendor"
	roleInvestor = "investor"
	roleGuest    = "guest"
)

// appPermissions is a variable containing all the application permissions.
var appPermissions = rbac.RolePermissions{
	roleAdmin: {
		ContractPermissions: adminCTPerms,
		QueryPermissions:    adminQPerms,
	},
	roleVendor: {
		ContractPermissions: vendorCTPerms,
		QueryPermissions:    vendorQPerms,
	},
	roleInvestor: {
		ContractPermissions: investorCTPerms,
		QueryPermissions:    investorQPerms,
	},
	roleGuest: {
		ContractPermissions: guestCTPerms,
		QueryPermissions:    guestQPerms,
	},
}
