package contracts

import "github.com/stickypixel/hyperledger/rbac"

func ruleAllow(userID string, userRoles []string) rbac.QueryRule {
	return rbac.QueryRule{Allow: true}
}

func ruleOwner(userID string, userRoles []string) rbac.QueryRule {
	return rbac.QueryRule{
		Allow: true,
		SelectorAppend: rbac.CDBSelector{
			"createdBy": userID,
		},
	}
}

func ruleInTransfer(userID string, userRoles []string) rbac.QueryRule {
	return rbac.QueryRule{
		Allow: true,
		SelectorAppend: rbac.CDBSelector{
			"$or": []rbac.CDBSelector{
				{"createdBy": userID},
				{"asset.from": userID},
				{"asset.to": userID},
				{"payment.from": userID},
				{"payment.to": userID},
			},
		},
	}
}

func ruleInTransaction(userID string, userRoles []string) rbac.QueryRule {
	return rbac.QueryRule{
		Allow: true,
		SelectorAppend: rbac.CDBSelector{
			"$or": []rbac.CDBSelector{
				{"createdBy": userID},
				{"from": userID},
				{"to": userID},
			},
		},
	}
}
