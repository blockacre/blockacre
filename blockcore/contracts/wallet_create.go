package contracts

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
)

// walletcreate creates a new user wallet.
func (t *BlockCore) walletcreate(stub shim.ChaincodeStubInterface, args []string, auth rbac.AuthServiceInterface) ([]byte, error) {
	if len(args) != 0 {
		return nil, ccerrors.NumberOfArguments()
	}

	return ledger.WalletCreate(stub, false)
}
