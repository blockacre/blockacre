package contracts

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/config"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/types"
)

// Creates a product entry in the ledger and a wallet for the product.
func (t *BlockCore) productcreate(stub shim.ChaincodeStubInterface, args []string, auth rbac.AuthServiceInterface) ([]byte, error) {
	// Get Input Object
	var input types.ProductCreateInput
	if err := ledger.InputValidate(args, &input); err != nil {
		return nil, errors.Wrap(err, "productcreate")
	}

	if input.UnitPrice.IsZero() {
		return nil, ccerrors.Price()
	}

	if input.Minimum.IsZero() {
		input.Minimum = decimal.RequireFromString("1")
	}

	recordDefaults, err := records.New(stub, records.StatusActive, records.Product)
	if err != nil {
		return nil, errors.Wrap(err, "productcreate")
	}

	product := &types.Product{
		ProductCreateInput: &input,
		Default:            recordDefaults,
		Issued:             decimal.Zero,
		PartPrice:          input.UnitPrice.DivRound(input.PartsPerUnit, config.Precision),
	}

	if _, err = ledger.WalletCreate(stub, true); err != nil {
		return nil, errors.Wrap(err, "productcreate")
	}

	if err = orders.CreateOrderBooks(stub); err != nil {
		return nil, errors.Wrap(err, "productcreate")
	}

	return ledger.PutObject(stub, product)
}
