package contracts

import (
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
)

const (
	contractPartial          = "partial"
	contractGet              = "get"
	contractQuery            = "query"
	contractWalletcreate     = "walletcreate"
	contractDeposit          = "deposit"
	contractWithdraw         = "withdraw"
	contractOrdercreate      = "ordercreate"
	contractProductcreate    = "productcreate"
	contractProductupdate    = "productupdate"
	contractOrderissuecreate = "orderissuecreate"
	contractYield            = "yield"
)

func getContract(t *BlockCore, contractName string) (rbac.ContractFunc, error) {
	availableContracts := map[string]rbac.ContractFunc{
		contractPartial:          t.partial,
		contractGet:              t.get,
		contractQuery:            t.query,
		contractWalletcreate:     t.walletcreate,
		contractDeposit:          t.deposit,
		contractWithdraw:         t.withdraw,
		contractOrdercreate:      t.ordercreate,
		contractProductcreate:    t.productcreate,
		contractProductupdate:    t.productupdate,
		contractOrderissuecreate: t.orderissuecreate,
	}
	c, ok := availableContracts[contractName]
	if !ok {
		err := ccerrors.FuncUnknown(contractName)

		return nil, err
	}

	return c, nil
}
