package contracts

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/types"
)

func (t *BlockCore) ordercreate(stub shim.ChaincodeStubInterface, args []string, auth rbac.AuthServiceInterface) ([]byte, error) {
	// Get Input Object
	var input orders.OrderInput
	if err := ledger.InputValidate(args, &input); err != nil {
		return nil, errors.Wrap(err, "ordercreate")
	}

	// Get product details so we can verify against them
	product := &types.Product{
		Default: &records.Default{
			DocType: records.Product,
			ID:      input.ProductID,
		},
	}
	if err := ledger.GetObject(stub, product); err != nil {
		return nil, errors.Wrap(err, "ordercreate")
	}

	// PreparedOrder is an object containing details to create an order
	preparedOrder, err := orders.Prepare(stub, &input, product, false)
	if err != nil {
		return nil, errors.Wrap(err, "ordercreate")
	}

	// Now, check for matching orders in the orderbook
	// walletStore and order are updated by reference
	if err := orders.Match(stub, preparedOrder); err != nil {
		return nil, errors.Wrap(err, "ordercreate")
	}

	return orders.Put(stub, preparedOrder)
}
