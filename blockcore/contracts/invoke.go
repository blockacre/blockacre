package contracts

import (
	"fmt"
	"net/http"
	"time"

	"github.com/hyperledger/fabric-chaincode-go/pkg/cid"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-protos-go/peer"
	"github.com/pkg/errors"
	"github.com/stickypixel/hyperledger/rbac"
	"go.uber.org/zap"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
)

// statusCoder is used for checking if an error can provide a custom status code.
type statusCoder interface {
	StatusCode() int32
}

// stackTracer is used for checking if an error can provide a StackTrace.
// type stackTracer interface {
// 	StackTrace() errors.StackTrace
// }

// BlockCore defines the HLFabric chaincode.
type BlockCore struct {
	logger *zap.Logger
}

// Init is needed and called when the chaincode is being installed.
func (t *BlockCore) Init(stub shim.ChaincodeStubInterface) peer.Response {
	// It seems like Init is only called once per channel, not once per peer.
	// Therefore things can not be attached to the chaincode here, such as a logger as it will not be available on all peers.
	fmt.Println("BlockCore initiating")

	return shim.Success(nil)
}

// Invoke is the function that is called by the Fabric Go SDK.
func (t *BlockCore) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	var (
		appAuth  rbac.AuthService
		clientID cid.ClientIdentity
		contract rbac.ContractFunc
		err      error
		payload  []byte
		response peer.Response
	)

	// Get the contractName and args.
	contractName, args := stub.GetFunctionAndParameters()

	// Initialise logger.
	t.logger = initLogger().Named("invoke")
	defer t.logger.Sync() //nolint:errcheck

	// Log requests, including stack trace in case of error.
	defer func(begin time.Time) {
		f := zap.String("contract", contractName)
		a := zap.Strings("args", args)
		d := zap.Duration("took", time.Since(begin))
		p := zap.ByteString("payload", payload)
		r := zap.Strings("roles", appAuth.GetUserRoles())

		switch {
		case err != nil:
			// Log the error
			t.logger.Debug("error", f, a, r, zap.Int32("status code:", response.GetStatus()), zap.Error(err), d)

			// Check if the error has a stack trace and log it too
			// if errST, ok := err.(stackTracer); ok {
			// 	st := errST.StackTrace()
			// 	len := math.MaxInt(6, cap(st))
			// 	t.logger.Sugar().Debugf("------------ StackTrace ------------%+v", st[0:len])
			// }
		default:
			t.logger.Debug("success", f, a, r, p, d)
		}
	}(time.Now())

	// Setup the clientID so we can pass it to the RBAC service
	clientID, err = cid.New(stub)
	if err != nil {
		// Error comes from 3rd party so will not have our wrapping
		err = ccerrors.UserAuthentication(err)
		response = errorResponse(err)

		return response
	}

	// Setup the appAuth
	appAuth, err = rbac.New(stub, clientID, appPermissions, "ba.groups")
	if err != nil {
		response = errorResponse(err)

		return response
	}

	// Check the requested contract exists
	contract, err = getContract(t, contractName)
	if err != nil {
		response = errorResponse(err)

		return response
	}

	// Run the requested contract with RBAC control
	payload, err = appAuth.WithContractAuth(contractName, args, contract)
	if err != nil {
		response = errorResponse(err)

		return response
	}

	return shim.Success(payload)
}

func errorResponse(e error) peer.Response {
	r := peer.Response{
		Message: errors.Wrap(e, "blockcore").Error(),
		Status:  http.StatusInternalServerError,
	}

	var errSC statusCoder
	if ok := errors.As(e, &errSC); ok {
		r.Status = errSC.StatusCode()
	}

	// Return the response with error message and status code.
	return r
}
