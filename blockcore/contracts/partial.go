package contracts

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/types"
)

func (t *BlockCore) partial(stub shim.ChaincodeStubInterface, args []string, auth rbac.AuthServiceInterface) ([]byte, error) {
	// Get Input Object
	var input types.GetInput
	if err := ledger.InputValidate(args, &input); err != nil {
		return nil, errors.Wrap(err, "partial")
	}

	iter, err := stub.GetStateByPartialCompositeKey(input.ObjectType.String(), input.Keys)
	if err != nil {
		return nil, ccerrors.StubGetState(err)
	}

	return ledger.ConstructArrayResults(iter)
}
