package contracts

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/config"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/types"
)

// Updates a product entry in the ledger.
func (t *BlockCore) productupdate(stub shim.ChaincodeStubInterface, args []string, auth rbac.AuthServiceInterface) ([]byte, error) {
	// Get Input Object
	var input types.ProductUpdateInput
	if err := ledger.InputValidate(args, &input); err != nil {
		return nil, errors.Wrap(err, "productupdate")
	}

	if input.UnitPrice.IsZero() {
		return nil, ccerrors.Price()
	}

	product := &types.Product{
		Default: &records.Default{
			DocType: records.Product,
			ID:      input.ID,
		},
	}
	if err := ledger.GetObject(stub, product); err != nil {
		return nil, errors.Wrap(err, "productupdate")
	}

	// If this product is not owned by the current user, return
	if product.CreatedBy != auth.GetUserID() {
		return nil, ccerrors.UserOwner()
	}

	modified, err := records.GetTimeStamp(stub)
	if err != nil {
		return nil, errors.Wrap(err, "productupdate")
	}

	product.UnitPrice = input.UnitPrice
	product.PartPrice = input.UnitPrice.DivRound(product.PartsPerUnit, config.Precision)
	product.Modified = modified

	return ledger.PutObject(stub, product)
}
