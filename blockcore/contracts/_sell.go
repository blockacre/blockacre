package contracts

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-chaincode-go/shim"

	"gitlab.com/blockacre/blockacre/blockcore/calc"
	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/util"
	"gitlab.com/blockacre/blockacre/common/types"
)

type inputSell struct {
	Amount   assets.Money
	Currency types.Currency
	ProductID   string
}

// nolint: funlen, gocyclo, gocognit
func (t *BlockCore) sell(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 1 {
		return nil, ccerrors.ErrInvalidNumberOfArguments()
	}

	input := new(inputPurchase)
	if err := json.Unmarshal([]byte(args[0]), &input); err != nil {
		return nil, ccerrors.ErrFormatUnmarshal(err)
	}

	if err := util.AssertUserType(stub, types.Consumer); err != nil {
		return nil, err
	}

	wallets := util.NewWalletStore(stub)
	sellerWallet, err := wallets.GetUserWallet()
	if err != nil {
		return nil, err
	}

	fund, err := util.GetFund(stub, input.ProductID)
	if err != nil {
		return nil, err
	}

	// check the currency is the one units are traded in
	if fund.Price.Currency != input.Currency {
		return nil, ccerrors.ErrCurrency()
	}

	// parse & check amount
	if input.Amount < 1 {
		return nil, ccerrors.ErrAmount()
	}

	amount := &assets.Money{
		Amount:   input.Amount,
		Currency: input.Currency,
	}
	units := calc.Units(amount.Amount, fund.Price.Amount)

	// check seller has enough units
	if !sellerWallet.Assets.Has(input.ProductID, units) {
		return nil, ccerrors.ErrInsufficientUnits()
	}

	timestamp, err := util.GetTimestamp(stub)
	if err != nil {
		return nil, err
	}

	sellOrder := types.Order{
		ID:         stub.GetTxID(),
		Type:       types.OrderTypeSale,
		ProductID:     input.ProductID,
		Amount:     amount,
		FullAmount: amount,
		Status:     records.StatusCreated,
		Owner:      sellerWallet.ID,
		Created:    timestamp,
	}

	// get matching sell orders (secondary market)
	iter, err := stub.GetStateByPartialCompositeKey(util.TypePurchase, []string{input.ProductID})
	if err != nil {
		return nil, ccerrors.ErrStubGetState(err)
	}
	defer iter.Close()

	// match orders
	for i := uint64(0); iter.HasNext(); i++ {
		kv, err := iter.Next()
		if err != nil {
			return nil, ccerrors.ErrIter(err)
		}

		purchaseOrder, err := util.GetOrder(stub, string(kv.GetValue()))
		if err != nil {
			return nil, err
		}

		buyerWallet, err := wallets.Get(purchaseOrder.Owner)
		if err != nil {
			return nil, err
		}

		value, fees := calc.FeesTotal(purchaseOrder.Amount.Amount, fee)
		if value > sellOrder.Amount.Amount {
			value, fees = sellOrder.Amount.Amount, calc.Fees(sellOrder.Amount.Amount, fee)
		}

		tx := &types.Transfer{
			ID:           fmt.Sprintf("%s-%s", stub.GetTxID(), util.UintToBase32(i)),
			From:         buyerWallet.ID,
			To:           sellerWallet.ID,
			DebitAmount:  &assets.Money{value + fees, sellOrder.Amount.Currency},
			CreditAmount: &assets.Money{value, sellOrder.Amount.Currency},
			Product: &types.Product{
				ID:    input.ProductID,
				Units: calc.Units(value, fund.Price.Amount),
			},
			Timestamp: timestamp,
			Type:      types.TransactionTypeSale,
			Status:    records.StatusFulfilled,
		}

		// check buyer has enough cash
		if !buyerWallet.Cash.Has(tx.DebitAmount) {
			continue
		}

		// move assets
		buyerWallet.Cash.Sub(tx.DebitAmount)
		sellerWallet.Cash.Add(tx.CreditAmount)
		sellerWallet.CashAvailable.Add(tx.CreditAmount)
		sellerWallet.Assets.Sub(input.ProductID, tx.Product.Units)
		buyerWallet.Assets.Add(input.ProductID, tx.Product.Units)

		// store tx
		if err = util.PutTransaction(stub, tx); err != nil {
			return nil, err
		}

		// update order amounts
		purchaseOrder.Amount.Amount -= tx.DebitAmount.Amount
		sellOrder.Amount.Amount -= tx.CreditAmount.Amount

		if purchaseOrder.Amount.Amount == 0 {
			// purchase order is fulfilled
			purchaseOrder.Status = records.StatusFulfilled
			if err = stub.DelState(kv.GetKey()); err != nil {
				return nil, ccerrors.ErrStubDelState(err)
			}
		}

		// store sell order updates
		purchaseOrder.Updated = timestamp
		if err = util.PutOrder(stub, purchaseOrder); err != nil {
			return nil, err
		}

		if sellOrder.Amount.Amount == 0 {
			// purchase order is fulfilled
			sellOrder.Status = records.StatusFulfilled
			break
		}
	}

	// store wallet updates after processing all orders in case more than one
	// order affects a wallet
	if err = wallets.Save(); err != nil {
		return nil, err
	}

	// convert to byte
	sellOrderAsByte, err := json.Marshal(sellOrder)
	if err != nil {
		return nil, ccerrors.ErrFormatMarshal(err)
	}

	if err = util.PutRaw(stub, util.TypeOrder, sellOrder.ID, sellOrderAsByte); err != nil {
		return nil, err
	}

	if sellOrder.Amount.Amount > 0 {
		// create index record
		if err = util.CreateIndex(stub, util.TypeSale, input.ProductID, sellOrder.ID); err != nil {
			return nil, err
		}
		if err = util.CounterAdd(stub, util.TypeSale, input.ProductID, uint64(sellOrder.Amount.Amount)); err != nil {
			return nil, err
		}
	}

	// subtract sold amount from purchase orders total
	if sellOrder.Amount.Amount != sellOrder.FullAmount.Amount {
		soldAmount := sellOrder.FullAmount.Amount - sellOrder.Amount.Amount
		if err = util.CounterSub(stub, util.TypePurchase, input.ProductID, uint64(soldAmount)); err != nil {
			return nil, err
		}
	}

	return sellOrderAsByte, nil
}
