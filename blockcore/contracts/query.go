package contracts

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
)

func (t *BlockCore) query(stub shim.ChaincodeStubInterface, args []string, auth rbac.AuthServiceInterface) ([]byte, error) {
	if len(args) != 1 {
		return nil, ccerrors.NumberOfArguments()
	}

	query, err := auth.ValidateQueryPerms(args[0])
	if err != nil {
		return nil, errors.Wrap(err, "query")
	}

	return ledger.Query(stub, query)
}
