package contracts

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/types"
)

func (t *BlockCore) orderissuecreate(stub shim.ChaincodeStubInterface, args []string, auth rbac.AuthServiceInterface) ([]byte, error) {
	// Get Input Object
	var input orders.OrderIssueInput
	if err := ledger.InputValidate(args, &input); err != nil {
		return nil, errors.Wrap(err, "orderissuecreate")
	}

	// Get product details so we can verify against them
	product := &types.Product{
		Default: &records.Default{
			DocType: records.Product,
			ID:      input.ProductID,
		},
	}
	if err := ledger.GetObject(stub, product); err != nil {
		return nil, errors.Wrap(err, "orderissuecreate")
	}

	// If this product is not owned by the current user, return
	if product.CreatedBy != auth.GetUserID() {
		return nil, ccerrors.UserOwner()
	}

	// When vendor creates a sell order, buyers should pay whichever is the lower of their order limit and the vendor's order limit

	// Create an OrderInput using the minimum parts needed to create an issuance request (this might be modified later)
	issueOrderInput := &orders.OrderInput{
		Asset: assets.Asset{
			ProductID: input.ProductID,
		},
		Side: orders.OrderSideSell,
	}
	// PreparedOrder is an object containing details to create an order
	preparedOrder, err := orders.Prepare(stub, issueOrderInput, product, true)
	if err != nil {
		return nil, errors.Wrap(err, "orderissuecreate")
	}

	// Now, check for matching orders in the orderbook
	// walletStore and order are updated by reference
	if err := orders.Match(stub, preparedOrder); err != nil {
		return nil, errors.Wrap(err, "orderissuecreate")
	}

	// Sanity check: verify that the created issue order is fulfilled, otherwise return an error
	if preparedOrder.Order.Status != records.StatusFulfilled {
		return nil, ccerrors.Sanity("preparedOrder.Order.Status != records.StatusFulfilled", preparedOrder.Order.GetID())
	}

	// Commit the product object as its Issue field will have been updated
	if _, err := ledger.PutObject(stub, product); err != nil {
		return nil, errors.Wrap(err, "orderissuecreate")
	}

	return orders.Put(stub, preparedOrder)
}
