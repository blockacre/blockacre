package contracts_test

import (
	"net/http"
	"testing"

	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestProductCreate(t *testing.T) {
	// Setup
	stub := testutils.InitBlockCore(t)
	sides := []orders.OrderSide{orders.OrderSideBuy, orders.OrderSideSell}

	// Create Vendor's Wallet
	testtools.SetCreator(t, stub, testdata.CertVendor1)

	// Create Product
	_, prRec := testtools.CheckInvoke(t, stub, "productcreate", testdata.RqProductCreate())

	// Test that the product and its wallet are created on the ledger
	testtools.CheckState(t, stub, testdata.ExpStateProduct(prRec), &prRec)
	testutils.CheckProductWallet(t, stub, testdata.ExpWalletNewProduct(prRec, testdata.WalletRefs[0]), prRec)

	// Check OrderBook creation
	for _, side := range sides {
		obRec := orders.OrderBook{
			Default: testutils.ChangeDocType(prRec, records.OrderBook),
			Side:    side,
		}
		expState := testdata.ExpStateEmptyOrderBook(prRec, side)
		testtools.CheckState(t, stub, expState, &obRec)
	}
}

func TestProductCreateAuth(t *testing.T) {
	stub := testutils.InitBlockCore(t)

	t.Log("No user")
	testtools.CheckBadInvoke(t, stub, http.StatusUnauthorized, "productcreate", testdata.RqProductCreate())

	t.Log("Investor User")
	testtools.SetCreator(t, stub, testdata.CertInvestor1)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "productcreate", testdata.RqProductCreate())

	t.Log("Admin User")
	testtools.SetCreator(t, stub, testdata.CertAdmin1)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "productcreate", testdata.RqProductCreate())
}
