package contracts_test

import (
	"net/http"
	"testing"

	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

const req = `{"objectType":"wallet"}`

func TestPartial(t *testing.T) {
	stub := testutils.InitBlockCore(t)

	testtools.SetCreator(t, stub, testdata.CertAdmin1)
	testtools.CheckInvoke(t, stub, "partial", req)
}

func TestPartialAuth(t *testing.T) {
	stub := testutils.InitBlockCore(t)

	t.Log("No user")
	testtools.CheckBadInvoke(t, stub, http.StatusUnauthorized, "partial", req)

	t.Log("Investor User")
	testtools.SetCreator(t, stub, testdata.CertInvestor1)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "partial", req)

	t.Log("Vendor User")
	testtools.SetCreator(t, stub, testdata.CertVendor1)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "partial", req)
}
