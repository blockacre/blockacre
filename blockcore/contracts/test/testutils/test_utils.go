package testutils

import (
	"testing"

	"github.com/hyperledger/fabric-chaincode-go/shim"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func ChangeDocType(rec records.Default, objectType records.DocType) *records.Default {
	def := &records.Default{
		DocType:   objectType,
		ID:        rec.ID,
		CreatedBy: rec.CreatedBy,
		Created:   rec.Created,
		Modified:  rec.Modified,
	}

	return def
}

// CheckWall converts the supplied default record to a user wallet record and then calls testtools.CheckState using the wallet record.
func CheckWall(t *testing.T, stub shim.ChaincodeStubInterface, expected string, rec records.Default) {
	waRec := ToWallet(rec, false)
	testtools.CheckState(t, stub, expected, &waRec)
}

// CheckProductWallet converts the supplied default record to a product wallet record and then calls testtools.CheckState using the wallet record.
func CheckProductWallet(t *testing.T, stub shim.ChaincodeStubInterface, expected string, rec records.Default) {
	waRec := ToWallet(rec, true)
	testtools.CheckState(t, stub, expected, &waRec)
}

// ToWallet converts the supplied default record to a wallet.
func ToWallet(rec records.Default, product bool) (waRec assets.Wallet) {
	waRec.Default = ChangeDocType(rec, records.Wallet)
	waRec.Product = product

	return waRec
}
