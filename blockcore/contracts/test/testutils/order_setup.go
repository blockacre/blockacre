package testutils

import (
	"testing"

	"github.com/hyperledger/fabric-chaincode-go/shimtest"

	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

type TestOrder struct {
	BobRec, SobRec orders.OrderBook
	PrRec          records.Default
	Refs           []string
	URecs          []records.Default
}

func OrderSetup(t *testing.T) (stub *shimtest.MockStub, s TestOrder) {
	stub = InitBlockCore(t)
	sfResult := SeedProducts(t, stub, 1)
	s.PrRec = sfResult[0]
	s.BobRec = orders.OrderBook{
		Default: ChangeDocType(s.PrRec, records.OrderBook),
		Side:    orders.OrderSideBuy,
	}
	s.SobRec = orders.OrderBook{
		Default: ChangeDocType(s.PrRec, records.OrderBook),
		Side:    orders.OrderSideSell,
	}
	s.Refs, s.URecs = SeedConsWithDep(t, stub)

	return stub, s
}
