package testutils

import (
	"encoding/json"
	"testing"

	"github.com/hyperledger/fabric-chaincode-go/shimtest"
	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/contracts"
	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

type SeedOrderRequest struct {
	Cert      string
	Parts     decimal.Decimal
	ProductID string
}

func InitBlockCore(t *testing.T) (stub *shimtest.MockStub) {
	var cc contracts.BlockCore
	stub = shimtest.NewMockStub("__TEST__", &cc)
	stub.MockInit("__TEST_INIT__", nil)

	return stub
}

func SeedProducts(t *testing.T, stub *shimtest.MockStub, count int) (recs []records.Default) {
	testtools.SetCreator(t, stub, testdata.CertVendor1)
	for i := 0; i < count; i++ {
		_, rec := testtools.CheckInvoke(t, stub, "productcreate", testdata.RqProductCreate())
		recs = append(recs, rec)
	}

	return recs
}

func SeedConsWithDep(t *testing.T, stub *shimtest.MockStub) (refs []string, recs []records.Default) {
	// Create users and deposit balance
	for _, investor := range testdata.InvestorCerts {
		testtools.SetCreator(t, stub, investor)
		walletcreate, rec := testtools.CheckInvoke(t, stub, "walletcreate")
		var wallet assets.Wallet
		_ = json.Unmarshal(walletcreate.Payload, &wallet)
		// Switch to admin for deposit
		testtools.SetCreator(t, stub, testdata.CertAdmin1)
		testtools.CheckInvoke(t, stub, "deposit", testdata.RqDeposit(wallet.Ref))
		refs = append(refs, wallet.Ref)
		recs = append(recs, rec)
	}

	return refs, recs
}

func SeedBuyOrders(t *testing.T, stub *shimtest.MockStub, reqs []SeedOrderRequest) (recs []records.Default) {
	for _, req := range reqs {
		testtools.SetCreator(t, stub, req.Cert)
		_, rec := testtools.CheckInvoke(t, stub, "ordercreate", testdata.RqOrder(req.ProductID, orders.OrderSideBuy, req.Parts))
		recs = append(recs, rec)
	}

	return recs
}

func SeedIssuableBuyOrders(t *testing.T, stub *shimtest.MockStub, productID string) (recs []records.Default) {
	for _, cert := range testdata.InvestorCerts {
		testtools.SetCreator(t, stub, cert)
		_, rec := testtools.CheckInvoke(t, stub, "ordercreate", testdata.RqOrder(productID, orders.OrderSideBuy, testdata.PartsIssueOK))
		recs = append(recs, rec)
	}

	return recs
}
