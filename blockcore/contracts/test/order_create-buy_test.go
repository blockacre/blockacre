package contracts_test

import (
	"testing"

	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestOrderCreateBuy(t *testing.T) {
	stub, s := testutils.OrderSetup(t)
	var boIDs []string

	for i, investorCert := range testdata.InvestorCerts {
		testtools.SetCreator(t, stub, investorCert)
		parts := testdata.PartsBuyOrderOK.Mul(decimal.NewFromInt(int64(i + 1)))

		// Create buy orders
		req := testdata.RqOrder(s.PrRec.GetID(), orders.OrderSideBuy, testdata.PartsBuyOrderOK)
		_, boRec := testtools.CheckInvoke(t, stub, "ordercreate", req)
		boIDs = append(boIDs, boRec.GetID())

		// Check orders exist
		expStateO := testdata.ExpStateOrder(boRec, s.PrRec.GetID(), orders.OrderSideBuy, testdata.PartsBuyOrderOK)
		testtools.CheckState(t, stub, expStateO, &boRec)

		// Check orderbooks are updated correctly
		expStateBOB := testdata.ExpStateOrderBook(s.PrRec, orders.OrderSideBuy, testdata.PricePart, parts, boIDs)
		expStateSOB := testdata.ExpStateEmptyOrderBook(s.PrRec, orders.OrderSideSell)
		testtools.CheckState(t, stub, expStateBOB, &s.BobRec)
		testtools.CheckState(t, stub, expStateSOB, &s.SobRec)

		// Check wallets are updated correctly
		expWallet := testdata.ExpWalletCashOnly(s.URecs[i], s.Refs[i], testdata.CashDeposit, testdata.CashAfterBuyOrder)
		testutils.CheckWall(t, stub, expWallet, s.URecs[i])
	}
}

func TestOrderCreateBuyFailures(t *testing.T) {
	stub, s := testutils.OrderSetup(t)

	for i, investorCert := range testdata.InvestorCerts {
		testtools.SetCreator(t, stub, investorCert)
		expWallet := testdata.ExpWalletDeposit(s.URecs[i], s.Refs[i])

		// Test when user has insufficient cash to complete order
		testtools.CheckBadInvoke(t, stub, 422, "ordercreate", testdata.RqOrder(s.PrRec.GetID(), orders.OrderSideBuy, testdata.PartsBuyOrderInsufficientCash))
		testutils.CheckWall(t, stub, expWallet, s.URecs[i])

		// Test when buy order does not exceed the minimum buy order value
		testtools.CheckBadInvoke(t, stub, 422, "ordercreate", testdata.RqOrder(s.PrRec.GetID(), orders.OrderSideBuy, testdata.PartsBuyOrderInsufficientTotal))
		testutils.CheckWall(t, stub, expWallet, s.URecs[i])
	}
}
