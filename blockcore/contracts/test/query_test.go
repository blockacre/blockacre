package contracts_test

import (
	"net/http"
	"testing"

	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestQueryByID(t *testing.T) {
	stub := testutils.InitBlockCore(t)
	sfResult := testutils.SeedProducts(t, stub, 3)
	testtools.SetCreator(t, stub, testdata.CertInvestor1)

	for _, rec := range sfResult {
		t.Skip("This test will fail because querying is not implemented in the mock stub")
		testtools.CheckQuery(t, stub, testdata.ExpStateProduct(rec), testdata.RqProductQuery(rec.GetID()))
	}
}

func TestQueryDoctypeFail(t *testing.T) {
	stub := testutils.InitBlockCore(t)
	testtools.SetCreator(t, stub, testdata.CertInvestor1)
	testtools.CheckBadQuery(t, stub, http.StatusBadRequest, testdata.RqQuery(""))
	testtools.CheckBadQuery(t, stub, http.StatusForbidden, testdata.RqQuery("walletindex"))
	testtools.CheckBadQuery(t, stub, http.StatusForbidden, testdata.RqQuery("unknownType"))
	t.Skip("This test will fail because querying is not implemented in the mock stub")
	testtools.CheckBadQuery(t, stub, http.StatusOK, testdata.RqQuery("transfer"))
}
