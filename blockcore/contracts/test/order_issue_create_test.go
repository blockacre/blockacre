package contracts_test

import (
	"net/http"
	"testing"

	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestOrderIssueCreateSingle(t *testing.T) {
	stub, s := testutils.OrderSetup(t)
	// Test the issuance occurs, order book is correct and transfer is recorded
	boReq := testutils.SeedOrderRequest{
		Cert:      testdata.CertInvestor1,
		ProductID: s.PrRec.GetID(),
		Parts:     testdata.PartsIssueOK,
	}

	BoRecs := testutils.SeedBuyOrders(t, stub, []testutils.SeedOrderRequest{boReq})
	testtools.SetCreator(t, stub, testdata.CertVendor1)

	_, oicDRec := testtools.CheckInvoke(t, stub, "orderissuecreate", testdata.RqIssue(s.PrRec.GetID()))
	oicRec := assets.Transfer{
		Default:  testutils.ChangeDocType(oicDRec, records.Transfer),
		Sequence: 1,
	}

	// Check issue order exists
	expStateO := testdata.ExpStateProductOrder(oicDRec, s.PrRec.GetID(), orders.OrderSideSell, testdata.PartsIssueOK)
	testtools.CheckState(t, stub, expStateO, &oicDRec)

	// Check that the orderbooks are empty
	expStateBOB := testdata.ExpStateEmptyOrderBook(s.PrRec, orders.OrderSideBuy)
	expStateSOB := testdata.ExpStateEmptyOrderBook(s.PrRec, orders.OrderSideSell)
	testtools.CheckState(t, stub, expStateBOB, &s.BobRec)
	testtools.CheckState(t, stub, expStateSOB, &s.SobRec)

	// Check that the product's wallet is updated
	expProductWallet := testdata.ExpWalletProduct(s.PrRec, testdata.WalletRefs[0], s.PrRec.GetID(), testdata.CashIssueOKValue, testdata.CashIssueOKValue, decimal.Zero)
	testutils.CheckProductWallet(t, stub, expProductWallet, s.PrRec)

	// Check that the user's wallet is updated
	expUserWallet := testdata.ExpWallet(s.URecs[0], s.Refs[0], s.PrRec.GetID(), testdata.CashAfterIssueOrder, testdata.CashAfterIssueOrder, boReq.Parts)
	testutils.CheckWall(t, stub, expUserWallet, s.URecs[0])

	// Check that the transfer record is on the ledger
	expStateTxIssue := testdata.ExpStateTxIssue(oicDRec, s.URecs[0].CreatedBy, s.PrRec.GetID(), BoRecs[0].GetID(), oicRec.Sequence, boReq.Parts)
	testtools.CheckState(t, stub, expStateTxIssue, &oicRec)
}

func TestOrderIssueCreateMultiple(t *testing.T) {
	stub, s := testutils.OrderSetup(t)
	// Test that the issuance occurs, order book is correct and transfer is recorded
	boReqs := []testutils.SeedOrderRequest{
		{
			Cert:      testdata.CertInvestor1,
			ProductID: s.PrRec.GetID(),
			Parts:     testdata.PartsBuyOrderOK,
		},
		{
			Cert:      testdata.CertInvestor2,
			ProductID: s.PrRec.GetID(),
			Parts:     testdata.PartsIssueOK,
		},
	}

	tests := []struct {
		transferParts decimal.Decimal
	}{
		{boReqs[0].Parts},
		{testdata.PartsIssueOK.Sub(boReqs[0].Parts)},
	}

	// Setup expected values
	totalParts := boReqs[0].Parts.Add(boReqs[1].Parts) // Expected total parts of all orders for the orderbook

	// Create first buy order for 1 part
	BoRecs := testutils.SeedBuyOrders(t, stub, boReqs)
	boIDs := []string{BoRecs[0].GetID(), BoRecs[1].GetID()}

	// Check that buy orderbook contains both orders and sell orderbook is empty
	expStateBOB := testdata.ExpStateOrderBook(s.PrRec, orders.OrderSideBuy, testdata.PricePart, totalParts, boIDs)
	expStateSOB := testdata.ExpStateEmptyOrderBook(s.PrRec, orders.OrderSideSell)
	testtools.CheckState(t, stub, expStateBOB, &s.BobRec)
	testtools.CheckState(t, stub, expStateSOB, &s.SobRec)

	// Create an orderissue
	testtools.SetCreator(t, stub, testdata.CertVendor1)
	_, oicDRec := testtools.CheckInvoke(t, stub, "orderissuecreate", testdata.RqIssue(s.PrRec.GetID()))

	// Check that the product's wallet is updated
	expProductWallet := testdata.ExpWalletProduct(s.PrRec, testdata.WalletRefs[0], s.PrRec.GetID(), testdata.CashIssueOKValue, testdata.CashIssueOKValue, decimal.Zero)
	testutils.CheckProductWallet(t, stub, expProductWallet, s.PrRec)

	// Check that the orderbook has just one entry, with the ID of the later buy order and sell orderbook is empty
	expStateBOB = testdata.ExpStateOrderBook(s.PrRec, orders.OrderSideBuy, testdata.PricePart, testdata.PartsBuyOrderOK, boIDs[1:2])
	testtools.CheckState(t, stub, expStateBOB, &s.BobRec)
	testtools.CheckState(t, stub, expStateSOB, &s.SobRec)

	for i, test := range tests {
		oicRec := assets.Transfer{
			Default:  testutils.ChangeDocType(oicDRec, records.Transfer),
			Sequence: i + 1,
		}
		// Setup expected values
		expUserPortfolioParts := testdata.PricePart.Mul(test.transferParts) // Number of parts the user should receive
		expUserCashBal := testdata.CashDeposit.Sub(expUserPortfolioParts)   // Expected cash balance after the transfer
		expUserSpend := boReqs[i].Parts.Mul(testdata.PricePart)             // Money the user has reserved for the assets on their buy order
		expUserCashAvBal := testdata.CashDeposit.Sub(expUserSpend)          // Expected cash available balance after the transfer

		// Check that the transfer records are on the ledger
		expStateTxIssue := testdata.ExpStateTxIssue(oicDRec, s.URecs[i].CreatedBy, s.PrRec.GetID(), BoRecs[i].GetID(), oicRec.Sequence, test.transferParts)
		testtools.CheckState(t, stub, expStateTxIssue, &oicRec)

		// Check that the user's wallets are updated correctly
		expUserWallet := testdata.ExpWallet(s.URecs[i], s.Refs[i], s.PrRec.GetID(), expUserCashBal, expUserCashAvBal, test.transferParts)
		testutils.CheckWall(t, stub, expUserWallet, s.URecs[i])
	}
}

func TestOrderCreateIssueFailures(t *testing.T) {
	stub, s := testutils.OrderSetup(t)
	boReqs1 := []testutils.SeedOrderRequest{
		{
			Cert:      testdata.CertInvestor1,
			ProductID: s.PrRec.GetID(),
			Parts:     testdata.PartsIssueInsufficient,
		},
	}
	boReqs2 := []testutils.SeedOrderRequest{
		{
			Cert:      testdata.CertInvestor2,
			ProductID: s.PrRec.GetID(),
			Parts:     testdata.PartsIssueInsufficient,
		},
	}

	// Test failure if user is not a vendor
	testtools.SetCreator(t, stub, testdata.CertInvestor1)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "orderissuecreate", testdata.RqIssue(s.PrRec.GetID()))

	// Test failure if user is not creator of the product
	testtools.SetCreator(t, stub, testdata.CertVendor2)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "orderissuecreate", testdata.RqIssue(s.PrRec.GetID()))

	// Check for failure when no purchase orders for sale
	testtools.SetCreator(t, stub, testdata.CertVendor1)
	testtools.CheckBadInvoke(t, stub, http.StatusUnprocessableEntity, "orderissuecreate", testdata.RqIssue(s.PrRec.GetID()))

	// Test failure when buy orders are 1 below minimum parts for full unit
	testutils.SeedBuyOrders(t, stub, boReqs1)
	testtools.SetCreator(t, stub, testdata.CertVendor1)
	testtools.CheckBadInvoke(t, stub, http.StatusUnprocessableEntity, "orderissuecreate", testdata.RqIssue(s.PrRec.GetID()))

	// Update product to a new price
	testtools.SetCreator(t, stub, testdata.CertVendor1)
	testtools.CheckInvoke(t, stub, "productupdate", testdata.RqProductUpdate(s.PrRec.GetID()))

	// Test failure when more buy orders from a new investor, and with a new price, but total is still 1 below minimum parts
	testtools.SetCreator(t, stub, testdata.CertAdmin1)
	testutils.SeedBuyOrders(t, stub, boReqs2)
	testtools.SetCreator(t, stub, testdata.CertVendor1)
	testtools.CheckBadInvoke(t, stub, http.StatusUnprocessableEntity, "orderissuecreate", testdata.RqIssue(s.PrRec.GetID()))

	// Check product wallet has not changed from new
	expWal := testdata.ExpWalletNewProduct(s.PrRec, testdata.WalletRefs[0])
	testutils.CheckProductWallet(t, stub, expWal, s.PrRec)
}
