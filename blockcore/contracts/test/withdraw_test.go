package contracts_test

import (
	"net/http"
	"testing"

	"github.com/hyperledger/fabric-chaincode-go/shimtest"
	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/config"
	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func withdrawSetup(t *testing.T) (stub *shimtest.MockStub, expRef string, usRec records.Default) {
	stub = testutils.InitBlockCore(t)
	expRef = testdata.WalletRefs[0]
	_, scRecs := testutils.SeedConsWithDep(t, stub)
	usRec = scRecs[0]
	testtools.SetCreator(t, stub, testdata.CertInvestor1)

	return stub, expRef, usRec
}

func TestWithdrawPartial(t *testing.T) {
	stub, expRef, usRec := withdrawSetup(t)
	withdrawAmount := testdata.CashDeposit.Sub(testdata.PriceAdjuster)
	_, wdRec := testtools.CheckInvoke(t, stub, "withdraw", testdata.RqWithdraw(withdrawAmount))

	// Test transaction has been committed to the ledger
	testtools.CheckState(t, stub, testdata.ExpStateTxWithdraw(wdRec, usRec.CreatedBy, expRef, withdrawAmount), &wdRec)
	// Test wallet updated
	expBal := testdata.PriceAdjuster
	testutils.CheckWall(t, stub, testdata.ExpWalletCashOnly(usRec, expRef, expBal, expBal), usRec)
}

func TestWithdrawEntire(t *testing.T) {
	stub, expRef, usRec := withdrawSetup(t)
	withdrawAmount := testdata.CashDeposit
	_, wdRec := testtools.CheckInvoke(t, stub, "withdraw", testdata.RqWithdraw(withdrawAmount))

	// Test transaction has been committed to the ledger
	testtools.CheckState(t, stub, testdata.ExpStateTxWithdraw(wdRec, usRec.CreatedBy, expRef, withdrawAmount), &wdRec)
	// Test wallet updated
	expBal := decimal.Zero
	testutils.CheckWall(t, stub, testdata.ExpWalletCashOnly(usRec, expRef, expBal, expBal), usRec)
}

func TestWithdrawFailures(t *testing.T) {
	stub, expRef, usRec := withdrawSetup(t)
	type test struct {
		amount decimal.Decimal
		code   int32
	}
	tests := []test{
		// Test insufficient balance
		{testdata.CashDeposit.Add(testdata.PriceAdjuster), http.StatusUnprocessableEntity},
		// Test insufficient withdraw amount
		{config.MinimumWithdrawal.Sub(testdata.PriceAdjuster), http.StatusBadRequest},
	}

	for _, test := range tests {
		testtools.CheckBadInvoke(t, stub, test.code, "withdraw", testdata.RqWithdraw(test.amount))
		// Test wallet not updated
		testutils.CheckWall(t, stub, testdata.ExpWalletDeposit(usRec, expRef), usRec)
	}
}
