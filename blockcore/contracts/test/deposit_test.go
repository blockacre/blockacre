package contracts_test

import (
	"encoding/json"
	"net/http"
	"testing"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestDeposit(t *testing.T) {
	type test struct {
		curr      string
		msg       string
		req       string
		expWallet string
	}
	stub := testutils.InitBlockCore(t)

	// Set creator identity to an investor
	testtools.SetCreator(t, stub, testdata.CertInvestor1)
	// Create a wallet with this identity
	walletcreate, usRec := testtools.CheckInvoke(t, stub, "walletcreate")
	var wallet assets.Wallet
	_ = json.Unmarshal(walletcreate.Payload, &wallet)

	// Set creator to admin
	testtools.SetCreator(t, stub, testdata.CertAdmin1)

	tests := []test{
		{
			msg:       "Deposit first balance",
			curr:      "GBP",
			req:       testdata.RqDeposit(wallet.Ref),
			expWallet: testdata.ExpWalletDeposit(usRec, wallet.Ref),
		},
		{
			msg:       "Deposit another balance in the same currency",
			curr:      "GBP",
			req:       testdata.RqDeposit(wallet.Ref),
			expWallet: testdata.ExpWalletDeposit2(usRec, wallet.Ref),
		},
		{
			msg:       "Deposit a balance in a new currency",
			curr:      "USD",
			req:       testdata.RqDepositUSD(wallet.Ref),
			expWallet: testdata.ExpWalletDeposit3(usRec, wallet.Ref),
		},
	}

	for _, test := range tests {
		t.Log(test.msg)
		// Deposit balance
		_, deRec := testtools.CheckInvoke(t, stub, "deposit", test.req)

		// Check the transaction has been committed to the ledger
		expTx := testdata.ExpStateTxDeposit(deRec, usRec.CreatedBy, wallet.Ref, test.curr)
		testtools.CheckState(t, stub, expTx, &deRec)

		// Check the wallet has been updated correctly
		testutils.CheckWall(t, stub, test.expWallet, usRec)
	}
}

func TestDepositAuth(t *testing.T) {
	stub := testutils.InitBlockCore(t)

	// No creator
	testtools.CheckBadInvoke(t, stub, http.StatusUnauthorized, "deposit", testdata.RqDeposit("XXX"))

	testtools.SetCreator(t, stub, testdata.CertInvestor1)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "deposit", testdata.RqDeposit("XXX"))

	testtools.SetCreator(t, stub, testdata.CertVendor1)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "deposit", testdata.RqDeposit("XXX"))
}
