package contracts_test

import (
	"net/http"
	"testing"

	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestInvokeFailure(t *testing.T) {
	stub := testutils.InitBlockCore(t)
	testtools.SetCreator(t, stub, testdata.CertInvestor1)
	testtools.CheckBadInvoke(t, stub, http.StatusBadRequest, "notafunction")
}
