package contracts_test

import (
	"net/http"
	"testing"

	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestProductUpdate(t *testing.T) {
	stub := testutils.InitBlockCore(t)
	testtools.SetCreator(t, stub, testdata.CertVendor1)

	_, prCrRec := testtools.CheckInvoke(t, stub, "productcreate", testdata.RqProductCreate())
	_, prUpRec := testtools.CheckInvoke(t, stub, "productupdate", testdata.RqProductUpdate(prCrRec.GetID()))

	testtools.CheckState(t, stub, testdata.ExpStateUpdateProduct(prUpRec), &prUpRec)
}

func TestProductUpdateFailures(t *testing.T) {
	stub := testutils.InitBlockCore(t)
	testtools.SetCreator(t, stub, testdata.CertVendor1)

	_, rec := testtools.CheckInvoke(t, stub, "productcreate", testdata.RqProductCreate())

	// Change creators and check a non-owner of product can't update it
	testtools.SetCreator(t, stub, testdata.CertVendor2)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "productupdate", testdata.RqProductUpdate(rec.GetID()))
}
