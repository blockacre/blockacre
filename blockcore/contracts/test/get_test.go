package contracts_test

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestGetProducts(t *testing.T) {
	stub := testutils.InitBlockCore(t)
	sfResult := testutils.SeedProducts(t, stub, 5)
	testtools.SetCreator(t, stub, testdata.CertAdmin1)

	for _, prRec := range sfResult {
		testtools.CheckInvoke(t, stub, "get", testdata.RqGet(records.Product, prRec.GetKeyAttrs()))
	}
}

func TestGetWallets(t *testing.T) {
	stub := testutils.InitBlockCore(t)
	refs, scRecs := testutils.SeedConsWithDep(t, stub)
	testtools.SetCreator(t, stub, testdata.CertAdmin1)

	for i, usRec := range scRecs {
		t.Log("Check that we put / get the right wallet ref to ID map", i)

		waRec := testutils.ToWallet(usRec, false)

		wiRes, _ := testtools.CheckInvoke(t, stub, "get", testdata.RqGet(records.WalletIndex, []string{refs[i]}))
		assert.Equal(t, usRec.CreatedBy, string(wiRes.Payload))

		wRes, _ := testtools.CheckInvoke(t, stub, "get", testdata.RqGet(waRec.GetObjectType(), waRec.GetKeyAttrs()))
		assert.JSONEq(t, testdata.ExpWalletDeposit(usRec, refs[i]), string(wRes.Payload))
	}
}

func TestGetFailure(t *testing.T) {
	stub := testutils.InitBlockCore(t)
	testtools.SetCreator(t, stub, testdata.CertAdmin1)
	t.Log("Check the raw ledger state for a product that won't exist")
	testtools.CheckBadInvoke(t, stub, http.StatusNotFound, "get", testdata.RqGet(records.Product, []string{"xxxxx"}))
}

func TestGetAuth(t *testing.T) {
	stub := testutils.InitBlockCore(t)

	t.Log("No user")
	testtools.CheckBadInvoke(t, stub, http.StatusUnauthorized, "get", testdata.RqGet(records.Product, []string{"xxxxx"}))

	t.Log("Investor User")
	testtools.SetCreator(t, stub, testdata.CertInvestor1)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "get", testdata.RqGet(records.Product, []string{"xxxxx"}))

	t.Log("Vendor User")
	testtools.SetCreator(t, stub, testdata.CertVendor1)
	testtools.CheckBadInvoke(t, stub, http.StatusForbidden, "get", testdata.RqGet(records.Product, []string{"xxxxx"}))
}
