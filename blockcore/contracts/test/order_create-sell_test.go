package contracts_test

import (
	"testing"

	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestOrderCreateSellSingle(t *testing.T) {
	stub, s := testutils.OrderSetup(t)
	var soIDs []string

	// Setup
	testutils.SeedIssuableBuyOrders(t, stub, s.PrRec.GetID())
	testtools.SetCreator(t, stub, testdata.CertVendor1)
	testtools.CheckInvoke(t, stub, "orderissuecreate", testdata.RqIssue(s.PrRec.GetID()))

	// Create a single sell order for each user
	for i, investorCert := range testdata.InvestorCerts {
		testtools.SetCreator(t, stub, investorCert)
		parts := testdata.PartsSellOrderOK.Mul(decimal.NewFromInt(int64(i + 1)))

		// Create sell orders
		_, soRec := testtools.CheckInvoke(t, stub, "ordercreate", testdata.RqOrder(s.PrRec.GetID(), orders.OrderSideSell, testdata.PartsSellOrderOK))
		soIDs = append(soIDs, soRec.GetID())

		// Check orders exist
		expStateO := testdata.ExpStateOrder(soRec, s.PrRec.GetID(), orders.OrderSideSell, testdata.PartsSellOrderOK)
		testtools.CheckState(t, stub, expStateO, &soRec)

		// Check orderbooks are updated correctly
		expStateBOB := testdata.ExpStateEmptyOrderBook(s.PrRec, orders.OrderSideBuy)
		expStateSOB := testdata.ExpStateOrderBook(s.PrRec, orders.OrderSideSell, testdata.PricePart, parts, soIDs)
		testtools.CheckState(t, stub, expStateBOB, &s.BobRec)
		testtools.CheckState(t, stub, expStateSOB, &s.SobRec)

		// Check wallets are updated correctly
		expWallet := testdata.ExpWallet(s.URecs[i], s.Refs[i], s.PrRec.GetID(), testdata.CashAfterSellOrder, testdata.CashAfterSellOrder, testdata.PartsIssueOK)
		testutils.CheckWall(t, stub, expWallet, s.URecs[i])
	}
}

func TestOrderCreateSellFailures(t *testing.T) {
	stub, s := testutils.OrderSetup(t)
	testutils.SeedIssuableBuyOrders(t, stub, s.PrRec.GetID())
	testtools.SetCreator(t, stub, testdata.CertVendor1)
	testtools.CheckInvoke(t, stub, "orderissuecreate", testdata.RqIssue(s.PrRec.GetID()))

	for i, investorCert := range testdata.InvestorCerts {
		testtools.SetCreator(t, stub, investorCert)

		// Test when sell order does not exceed the minimum sell order value
		testtools.CheckBadInvoke(t, stub, 422, "ordercreate", testdata.RqOrder(s.PrRec.GetID(), orders.OrderSideSell, testdata.PartsSellOrderInsufficientTotal))

		// Test when user has insufficient assets to complete order
		testtools.CheckBadInvoke(t, stub, 422, "ordercreate", testdata.RqOrder(s.PrRec.GetID(), orders.OrderSideSell, testdata.PartsSellOrderInsufficientAssets))

		// Test wallets aren't changed
		expWallet := testdata.ExpWallet(s.URecs[i], s.Refs[i], s.PrRec.GetID(), testdata.CashAfterIssueOrder, testdata.CashAfterIssueOrder, testdata.PartsIssueOK)
		testutils.CheckWall(t, stub, expWallet, s.URecs[i])
	}
}
