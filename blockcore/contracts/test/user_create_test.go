package contracts_test

import (
	"testing"

	"gitlab.com/blockacre/blockacre/blockcore/contracts/test/testutils"
	"gitlab.com/blockacre/blockacre/blockcore/test/testdata"
	"gitlab.com/blockacre/blockacre/blockcore/test/testtools"
)

func TestWalletCreate(t *testing.T) {
	stub := testutils.InitBlockCore(t)

	for i, cert := range testdata.InvestorCerts {
		testtools.SetCreator(t, stub, cert)
		_, usRec := testtools.CheckInvoke(t, stub, "walletcreate")
		testutils.CheckWall(t, stub, testdata.ExpWalletNew(usRec, testdata.WalletRefs[i]), usRec)
	}
}
