package contracts

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/config"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

func (t *BlockCore) withdraw(stub shim.ChaincodeStubInterface, args []string, auth rbac.AuthServiceInterface) ([]byte, error) {
	// Get Input Object
	var input assets.WithdrawInput
	if err := ledger.InputValidate(args, &input); err != nil {
		return nil, errors.Wrap(err, "withdraw")
	}

	if input.Amount.LessThan(config.MinimumWithdrawal) {
		return nil, ccerrors.Amount()
	}

	withdrawWallet, err := ledger.WalletGetThisUser(stub)
	if err != nil {
		return nil, errors.Wrap(err, "withdraw")
	}

	transaction := &assets.MoneyTransfer{
		Money: assets.Money{
			Amount:   input.Amount,
			Currency: input.Currency,
		},
		From: withdrawWallet.GetPrimaryKey(),
		To:   "BankAccNumber",
	}

	// create transaction
	recordDefaults, err := records.New(stub, records.StatusFulfilled, records.Transaction)
	if err != nil {
		return nil, errors.Wrap(err, "withdraw")
	}
	withdrawal := &assets.Transaction{
		Default:       recordDefaults,
		MoneyTransfer: transaction,
		Type:          assets.TransactionTypeWithdrawal,
		WalletRef:     withdrawWallet.Ref,
	}

	// update balance
	err = withdrawWallet.CashAvailable.Sub(withdrawal.Money)
	if err != nil {
		return nil, errors.Wrap(err, "withdraw")
	}
	err = withdrawWallet.Cash.Sub(withdrawal.Money)
	if err != nil {
		return nil, errors.Wrap(err, "withdraw")
	}

	// Put withdrawWallet updates
	if _, err := ledger.PutObject(stub, withdrawWallet); err != nil {
		return nil, errors.Wrap(err, "withdraw")
	}

	// Put transaction
	return ledger.PutObject(stub, withdrawal)
}
