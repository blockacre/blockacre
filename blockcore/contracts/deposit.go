package contracts

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/stickypixel/hyperledger/rbac"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

func (t *BlockCore) deposit(stub shim.ChaincodeStubInterface, args []string, auth rbac.AuthServiceInterface) ([]byte, error) {
	// Get Input Object
	var input assets.DepositInput
	if err := ledger.InputValidate(args, &input); err != nil {
		return nil, errors.Wrap(err, "deposit")
	}

	receivingWallet, err := ledger.WalletGetByRef(stub, input.WalletRef)
	if err != nil {
		return nil, errors.Wrap(err, "deposit")
	}

	transfer := &assets.MoneyTransfer{
		Money: assets.Money{
			Amount:   input.Amount,
			Currency: input.Currency,
		},
		To:   receivingWallet.GetPrimaryKey(),
		From: "BankAccNumber",
	}

	recordDefaults, err := records.New(stub, records.StatusFulfilled, records.Transaction)
	if err != nil {
		return nil, errors.Wrap(err, "deposit")
	}

	// create transaction record
	deposit := &assets.Transaction{
		Default:       recordDefaults,
		MoneyTransfer: transfer,
		Type:          assets.TransactionTypeDeposit,
		WalletRef:     input.WalletRef,
	}

	// update balance
	receivingWallet.Cash.Add(deposit.Money)
	receivingWallet.CashAvailable.Add(deposit.Money)

	// Put receivingWallet updates
	if _, err := ledger.PutObject(stub, receivingWallet); err != nil {
		return nil, errors.Wrap(err, "deposit")
	}

	// Put transaction
	return ledger.PutObject(stub, deposit)
}
