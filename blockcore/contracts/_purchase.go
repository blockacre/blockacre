package contracts

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-chaincode-go/shim"

	"gitlab.com/blockacre/blockacre/blockcore/calc"
	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/util"
	"gitlab.com/blockacre/blockacre/common/types"
)

type inputPurchase struct {
	Amount   assets.Money
	Currency types.Currency
	ProductID   string
	// Todo add some details for the bank api calls
}

// purchase creates a purchase order for a user
// loops through existing sales orders and immediately purchases as many as possible
// TODO: might be an idea to split out the p2p logic
// nolint: funlen, gocyclo, gocognit
func (t *BlockCore) purchase(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 1 {
		return nil, ccerrors.ErrInvalidNumberOfArguments()
	}

	input := new(inputPurchase)
	if err := json.Unmarshal([]byte(args[0]), &input); err != nil {
		return nil, ccerrors.ErrFormatUnmarshal(err)
	}

	if err := util.AssertUserType(stub, types.Consumer); err != nil {
		return nil, err
	}

	wallets := util.NewWalletStore(stub)
	buyerWallet, err := wallets.GetUserWallet()
	if err != nil {
		return nil, err
	}

	// get fund from ledger
	fund, err := util.GetFund(stub, input.ProductID)
	if err != nil {
		return nil, err
	}

	// check the currency is the one units are traded in
	if fund.Price.Currency != input.Currency {
		return nil, ccerrors.ErrCurrency()
	}

	// check amount
	if input.Amount < minimumPurchase {
		return nil, ccerrors.ErrMinPurchase()
	}

	amount := &assets.Money{
		Amount:   input.Amount,
		Currency: input.Currency,
	}

	// check buyer has sufficient balance
	if !buyerWallet.CashAvailable.Has(amount) {
		return nil, ccerrors.ErrInsufficientBalance()
	}
	buyerWallet.CashAvailable.Sub(amount)

	// get current timestamp
	timestamp, err := util.GetTimestamp(stub)
	if err != nil {
		return nil, err
	}

	// create purchase order
	purchaseOrder := types.Order{
		ID:         stub.GetTxID(),
		Type:       types.OrderTypePurchase,
		ProductID:     input.ProductID,
		Amount:     amount,
		FullAmount: amount,
		Status:     records.StatusCreated,
		Owner:      buyerWallet.ID,
		Created:    timestamp,
		Updated:    timestamp,
	}

	// get matching sell orders (secondary market)
	iter, err := stub.GetStateByPartialCompositeKey(util.TypeSale, []string{input.ProductID})
	if err != nil {
		return nil, ccerrors.ErrStubGetState(err)
	}
	defer iter.Close()

	// match orders
	for i := uint64(0); iter.HasNext(); i++ {
		kv, err := iter.Next()
		if err != nil {
			return nil, ccerrors.ErrIter(err)
		}

		sellOrder, err := util.GetOrder(stub, string(kv.GetValue()))
		if err != nil {
			return nil, err
		}

		sellerWallet, err := wallets.Get(sellOrder.Owner)
		if err != nil {
			return nil, err
		}

		value, fees := calc.FeesTotal(purchaseOrder.Amount.Amount, fee)
		if value > sellOrder.Amount.Amount {
			value, fees = sellOrder.Amount.Amount, calc.Fees(sellOrder.Amount.Amount, fee)
		}
		units := calc.Units(value, fund.Price.Amount)

		// check seller has enough units - if not get as much as possible
		if !sellerWallet.Assets.Has(input.ProductID, units) {
			value = calc.Cost(sellerWallet.Assets[input.ProductID], fund.Price.Amount)
			fees = calc.Fees(value, fee)
			sellOrder.Amount.Amount = value // amend order amount to maximum available
			units = calc.Units(value, fund.Price.Amount)
		}

		if util.IsZero(units) {
			// seller has no units
			// TODO: close the order
			continue
		}

		tx := &types.Transfer{
			ID:           fmt.Sprintf("%s-%s", stub.GetTxID(), util.UintToBase32(i)),
			From:         buyerWallet.ID,
			To:           sellerWallet.ID,
			DebitAmount:  &assets.Money{value + fees, purchaseOrder.Amount.Currency},
			CreditAmount: &assets.Money{value, purchaseOrder.Amount.Currency},
			Product: &types.Product{
				ID:    input.ProductID,
				Units: units,
			},
			Timestamp: timestamp,
			Type:      types.TransactionTypeSale,
			Status:    records.StatusFulfilled,
		}
		if err = util.PutTransaction(stub, tx); err != nil {
			return nil, err
		}

		// move assets
		buyerWallet.Cash.Sub(tx.DebitAmount)
		sellerWallet.Cash.Add(tx.CreditAmount)
		sellerWallet.CashAvailable.Add(tx.CreditAmount)
		sellerWallet.Assets.Sub(input.ProductID, units)
		buyerWallet.Assets.Add(input.ProductID, units)

		// update order amounts
		purchaseOrder.Amount.Amount -= tx.DebitAmount.Amount
		sellOrder.Amount.Amount -= tx.CreditAmount.Amount

		if sellOrder.Amount.Amount == 0 {
			// sell order is fulfilled
			sellOrder.Status = records.StatusFulfilled
			if err = stub.DelState(kv.GetKey()); err != nil {
				return nil, ccerrors.ErrStubDelState(err)
			}
		}

		// store sell order updates
		sellOrder.Updated = timestamp
		if err = util.PutOrder(stub, sellOrder); err != nil {
			return nil, err
		}

		if purchaseOrder.Amount.Amount == 0 {
			// purchase order is fulfilled
			purchaseOrder.Status = records.StatusFulfilled
			break
		}
	}

	// store wallet updates after processing all orders in case more than one
	// order affects a wallet
	if err = wallets.Save(); err != nil {
		return nil, err
	}

	// convert to byte
	purchaseOrderAsByte, err := json.Marshal(purchaseOrder)
	if err != nil {
		return nil, ccerrors.ErrFormatMarshal(err)
	}

	// store purchase order
	if err = util.PutRaw(stub, util.TypeOrder, purchaseOrder.ID, purchaseOrderAsByte); err != nil {
		return nil, err
	}

	// create index record & update purchase orders total
	if purchaseOrder.Amount.Amount > 0 {
		if err = util.CreateIndex(stub, util.TypePurchase, input.ProductID, purchaseOrder.ID); err != nil {
			return nil, err
		}
		if err = util.CounterAdd(stub, util.TypePurchase, input.ProductID, uint64(purchaseOrder.Amount.Amount)); err != nil {
			return nil, err
		}
	}

	// update sell orders total
	if purchaseOrder.Amount.Amount != purchaseOrder.FullAmount.Amount {
		purchasedAmount := purchaseOrder.FullAmount.Amount - purchaseOrder.Amount.Amount
		if err = util.CounterSub(stub, util.TypeSale, input.ProductID, uint64(purchasedAmount)); err != nil {
			return nil, err
		}
	}

	return purchaseOrderAsByte, nil
}
