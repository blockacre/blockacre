package contracts

import (
	"encoding/json"
	"fmt"
	"math/big"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/calc"
	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/types"
	"gitlab.com/blockacre/blockacre/blockcore/util"
)

type inputYield struct {
	Amount   decimal.Decimal
	Currency types.Currency
	ProductID   string
}

// nolint: funlen, unparam
func (t *BlockCore) yield(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 1 {
		return nil, ccerrors.ErrInvalidNumberOfArguments()
	}

	input := new(inputYield)
	if err := json.Unmarshal([]byte(args[0]), &input); err != nil {
		return nil, ccerrors.ErrFormatUnmarshal(err)
	}

	// only allow vendor
	if err := util.AssertUserType(stub, types.Vendor); err != nil {
		return nil, err
	}

	// get fund from ledger
	fund, err := util.GetFund(stub, input.ProductID)
	if err != nil {
		return nil, err
	}

	// check user owns fund
	userID, err := util.GetID(stub)
	if err != nil {
		return nil, err
	}
	if fund.Owner != userID {
		return nil, ccerrors.ErrUserOwner()
	}

	total := &assets.Money{
		input.Amount * assets.Money(fund.Sold),
		input.Currency,
	}

	// get fund wallet from ledger
	fundWallet, err := util.GetWallet(stub, input.ProductID)
	if err != nil {
		return nil, err
	}

	// check fund has sufficient balance
	if !fundWallet.CashAvailable.Has(total) {
		return nil, ccerrors.ErrInsufficientBalance()
	}

	// get current timestamp for transactions
	timestamp, err := util.GetTimestamp(stub)
	if err != nil {
		return nil, err
	}

	units := types.Convert(types.Unit, types.NanoUnit, big.NewInt(int64(fund.Sold)))

	// get all wallets
	iter, err := stub.GetStateByPartialCompositeKey(util.TypeWallet, nil)
	if err != nil {
		return nil, ccerrors.ErrStubGetState(err)
	}
	defer iter.Close()

	var i uint64 // counter for use in tx IDs

	// iterate wallets until all owners of units are paid
	for iter.HasNext() && util.IsPositive(units) {
		kv, err := iter.Next()
		if err != nil {
			return nil, ccerrors.ErrIter(err)
		}

		// parse wallet
		wallet := new(types.Wallet)
		if err = json.Unmarshal(kv.GetValue(), &wallet); err != nil {
			return nil, ccerrors.ErrFormatUnmarshal(err)
		}
		_, keys, err := stub.SplitCompositeKey(kv.GetKey())
		if err != nil {
			return nil, ccerrors.ErrStubKey(err)
		}
		wallet.ID = keys[0] // wallet ID isn't stored in JSON

		if wallet.ID == fund.ID {
			continue // skip fund wallet
		}

		// check wallet has units
		u, ok := wallet.Assets[input.ProductID]
		if !ok || !util.IsPositive(u) {
			continue
		}
		units.Sub(units, u) // subtract from total units left to serve
		i++                 // increase tx counter

		// calculate yield
		value := calc.Cost(u, input.Amount)
		yield := &assets.Money{
			value,
			input.Currency,
		}

		// create & store tx
		tx := &types.Transfer{
			ID:           fmt.Sprintf("%s-%s", stub.GetTxID(), util.UintToBase32(i)),
			From:         fund.ID,
			To:           wallet.ID,
			DebitAmount:  yield,
			CreditAmount: yield,
			Created:      timestamp,
			Type:         types.TransactionTypeYield,
			Status:       records.StatusFulfilled,
		}
		if err = util.PutTransaction(stub, tx); err != nil {
			return nil, err
		}

		// update & store wallet
		fundWallet.Cash.Sub(total)
		fundWallet.CashAvailable.Sub(total)
		wallet.Cash.Add(yield)
		wallet.CashAvailable.Add(yield)
		if err = util.PutWallet(stub, wallet); err != nil {
			return nil, err
		}

		total.Amount -= yield.Amount // update remaining total
	}

	// TODO: remove when we know this can't happen
	if !util.IsZero(units) {
		err := errors.Errorf("unit mismatch: %s", units)
		return nil, ccerrors.ErrGeneric(err)
	}
	if total.Amount != 0 {
		err := errors.Errorf("amount mismatch: %s", total.Amount)
		return nil, ccerrors.ErrGeneric(err)
	}

	if err = util.PutWallet(stub, fundWallet); err != nil {
		return nil, err
	}

	return nil, nil
}
