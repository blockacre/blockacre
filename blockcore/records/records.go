package records

//go:generate enumer -type=Status -trimprefix=Status -transform=lower -json -text -output=status_generated.go

import (
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/hyperledger/fabric-chaincode-go/pkg/cid"
	"github.com/hyperledger/fabric-chaincode-go/shim"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
)

// Generally decided to keep this part as a separate package to avoid import cycles - it is used in many places.

// Status represents the current status of a transaction/transfer/order/product.
type Status int

// Possible statuses.
const (
	StatusActive Status = iota + 1 // Start from 1 or json marshalling treats value as a nil.
	StatusFulfilled
	StatusFailed
)

// Record is an interface - describes the methods of ledger records for retrieving various details.
type Record interface {
	GetObjectType() DocType
	GetKeyAttrs() []string
	GetID() string
}

// Default contains the default fields that ledger records contain.
type Default struct {
	DocType   DocType    `json:"docType,omitempty"`
	ID        string     `json:"id,omitempty"`
	CreatedBy string     `json:"createdBy,omitempty"`
	Created   *time.Time `json:"created,omitempty"`
	Modified  *time.Time `json:"modified,omitempty"`
	Status    Status     `json:"status,omitempty"`
}

// GetObjectType Enables Default to satisfy the Record interface. Used in key creation.
func (d *Default) GetObjectType() DocType {
	return d.DocType
}

// GetKeyAttrs Enables Default to satisfy the Record interface. Used in key creation.
func (d *Default) GetKeyAttrs() []string {
	return []string{d.ID}
}

// GetID Enables Default to satisfy the Record interface.
func (d *Default) GetID() string {
	return d.ID
}

// New returns a Default struct used for creating new records on the ledger.
func New(stub shim.ChaincodeStubInterface, status Status, doctype DocType) (*Default, error) {
	created, err := GetTimeStamp(stub)
	if err != nil {
		return nil, ccerrors.Generic(err)
	}
	createdBy, rawErr := cid.GetID(stub)
	if rawErr != nil {
		return nil, ccerrors.UserAuthentication(rawErr)
	}
	recordDefaults := &Default{
		ID:        stub.GetTxID(),
		Created:   created,
		CreatedBy: createdBy,
		DocType:   doctype,
		Status:    status,
	}

	return recordDefaults, nil
}

// GetTimeStamp returns a formatted timestamp.
func GetTimeStamp(stub shim.ChaincodeStubInterface) (*time.Time, error) {
	ts, rawErr := stub.GetTxTimestamp()
	if rawErr != nil {
		return nil, ccerrors.Stub(rawErr)
	}
	created, rawErr := ptypes.Timestamp(ts)
	if rawErr != nil {
		return nil, ccerrors.Generic(rawErr)
	}

	return &created, nil
}
