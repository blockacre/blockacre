package records

//go:generate enumer -type=DocType -transform=lower -json -text -output=doctypes_generated.go

// DocType is used for couchdb doctypes.
type DocType int

// A list of available DocTypes.
const (
	Order DocType = iota + 1 // Start from 1 else json marshalling treats value as a nil
	OrderBook
	Product
	Transaction
	Transfer
	Wallet
	WalletCounter
	WalletIndex
)
