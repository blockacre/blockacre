package ccerrors

import (
	"net/http"

	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
)

var moneyPlaces int32 = 2

// Error represents an error with an associated HTTP status code.
// Implements a default error interface.
type Error struct {
	Status int32
	Err    error
}

// stackTracer is used for checking if an error can provide a StackTrace.
type stackTracer interface {
	StackTrace() errors.StackTrace
}

// Error allows Error to satisfy the default error interface.
func (e *Error) Error() string {
	return e.Err.Error()
}

// StackTrace returns the error stacktrace and satisfies the erros.Stacktrace interface.
func (e *Error) StackTrace() errors.StackTrace {
	var errST stackTracer
	if ok := errors.As(e, &errST); ok && errST != nil {
		return errST.StackTrace()
	}

	return nil
}

// StatusCode returns the status code and satisfies the Go Kit StatusCoder interface. Also we can spot our errors from this interface.
func (e *Error) StatusCode() int32 {
	return e.Status
}

// 400 Bad Request Errors.
func badRequest(err error) *Error {
	return &Error{
		Err:    err,
		Status: http.StatusBadRequest,
	}
}

// Amount error.
func Amount() *Error {
	err := errors.New("Invalid Amount")

	return badRequest(err)
}

// Currency error.
func Currency() *Error {
	err := errors.New("Mismatched Currency")

	return badRequest(err)
}

// FormatMarshal error.
func FormatMarshal(err error) *Error {
	err = errors.Wrap(err, "Marshaling failed")

	return badRequest(err)
}

// FuncUnknown error.
func FuncUnknown(contractName string) *Error {
	err := errors.New("Received unknown contract invocation: `" + contractName + "`")

	return badRequest(err)
}

// NumberOfArguments error.
func NumberOfArguments() *Error {
	err := errors.New("Invalid number of arguments")

	return badRequest(err)
}

// StubGetState error.
func StubGetState(err error) *Error {
	err = errors.Wrap(err, "Ledger Get failed")

	return badRequest(err)
}

// StubGetQueryResult error.
func StubGetQueryResult(err error) *Error {
	err = errors.Wrap(err, "Ledger Query failed")

	return badRequest(err)
}

// StubPutState error.
func StubPutState(err error) *Error {
	err = errors.Wrap(err, "Ledger Write failed")

	return badRequest(err)
}

// StubDelState error.
func StubDelState(err error) *Error {
	err = errors.Wrap(err, "Ledger Delete failed")

	return badRequest(err)
}

// Price error.
func Price() *Error {
	err := errors.New("Price must be greater than 0")

	return badRequest(err)
}

// 401 Unauthenticated errors.
func unauthenticated(err error) *Error {
	return &Error{
		Err:    err,
		Status: http.StatusUnauthorized,
	}
}

// UserAuthentication error.
func UserAuthentication(err error) *Error {
	err = errors.Wrap(err, "User authentication failed")

	return unauthenticated(err)
}

// 403 Unauthorized Errors.
func unauthorized(err error) *Error {
	return &Error{
		Err:    err,
		Status: http.StatusForbidden,
	}
}

// UserOwner error.
func UserOwner() *Error {
	err := errors.New("User must be owner of resource")

	return unauthorized(err)
}

// WalletNotExists error.
func WalletNotExists(err error) *Error {
	err = errors.Wrap(err, "Wallet does not exist, user is not registered")

	return unauthorized(err)
}

// 404 Not Found Errors.
func notFound(err error) *Error {
	return &Error{
		Err:    err,
		Status: http.StatusNotFound,
	}
}

// StubGetStateNF error.
func StubGetStateNF(objectType string, keys []string) *Error {
	err := errors.Errorf("Resource not found on ledger. Type: `%s`, KeyID: `%s`", objectType, keys)

	return notFound(err)
}

// 409 Conflict errors.
func conflict(err error) *Error {
	return &Error{
		Err:    err,
		Status: http.StatusConflict,
	}
}

// WalletExists error.
func WalletExists() *Error {
	err := errors.New("Wallet exists, user is already registered")

	return conflict(err)
}

// 422 Unprocessable errors.
func unprocessable(err error) *Error {
	return &Error{
		Err:    err,
		Status: http.StatusUnprocessableEntity,
	}
}

// InsufficientBalance error.
func InsufficientBalance(id string, have, required decimal.Decimal) *Error {
	err := errors.Errorf("Insufficient balance to complete transaction. Asset: %v, Have: %v, Required: %v", id, have.Round(moneyPlaces).String(), required.Round(moneyPlaces).String())

	return unprocessable(err)
}

// InsufficientOrder error.
func InsufficientOrder(minOrderValue, minRequired decimal.Decimal) *Error {
	err := errors.Errorf("Order value is not sufficient. Value is %v, minimum is %v", minOrderValue.Round(moneyPlaces).String(), minRequired.Round(moneyPlaces).String())

	return unprocessable(err)
}

// InsufficientBuyOrders error.
func InsufficientBuyOrders(have, required decimal.Decimal) *Error {
	err := errors.Errorf("Insufficient total parts of buy orders above price to reach minimum unit issuance limit. Have: %v, Required: %v", have, required)

	return unprocessable(err)
}

// 500.
func internalServer(err error) *Error {
	return &Error{
		Err:    err,
		Status: http.StatusInternalServerError,
	}
}

// FormatBigInt used for big.Int package errors.
func FormatBigInt() *Error {
	err := errors.New("Big Int Conversion Error")

	return internalServer(err)
}

// Generic used for any generic errors.
func Generic(err error) *Error {
	err = errors.Wrap(err, "Generic Error")

	return internalServer(err)
}

// Iter used for iterator errors.
func Iter(err error) *Error {
	err = errors.Wrap(err, "Iterator Error")

	return internalServer(err)
}

// Stub used for general stub errors.
func Stub(err error) *Error {
	err = errors.Wrap(err, "General Stub Error")

	return internalServer(err)
}

// StubKey used for stub key errors.
func StubKey(err error) *Error {
	err = errors.Wrap(err, "Stub Key Error")

	return internalServer(err)
}

// Sanity used for any errors caused by business logic sanity checks.
func Sanity(msg, orderID string) *Error {
	err := errors.Errorf("Sanity Check Error. OrderID: %v. %v", orderID, msg)

	return internalServer(err)
}
