package assets

import (
	"encoding/json"
	"sort"

	"github.com/pkg/errors"
	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
)

// Asset represents a number of parts referenced to a product.
type Asset struct {
	Parts     decimal.Decimal `json:"parts,omitempty"`
	ProductID string          `json:"productID,omitempty"`
}

// AssetTransfer describes the details of an asset transfer.
type AssetTransfer struct {
	Asset
	PartPrice decimal.Decimal `json:"partPrice,omitempty"`
	From      string          `json:"from,omitempty"` // should be the primary key of the from wallet.
	To        string          `json:"to,omitempty"`   // should be the primary key of the to wallet.
}

// Portfolio is a map of fiat currencies [ProductID]: Amount.
type Portfolio map[string]decimal.Decimal

// Add or increment asset to a Portfolio.
func (pf *Portfolio) Add(a Asset) {
	// In case Portfolio is completely empty.
	if len(*pf) == 0 {
		*pf = make(Portfolio)
	}
	// Add parts.
	(*pf)[a.ProductID] = (*pf)[a.ProductID].Add(a.Parts)
}

// Sub subtract asset from a Portfolio.
func (pf *Portfolio) Sub(a Asset) error {
	if err := pf.Has(a); err != nil {
		return errors.Wrap(err, "Portfolio Sub")
	}
	(*pf)[a.ProductID] = (*pf)[a.ProductID].Sub(a.Parts)

	return nil
}

// Has verifies if a Portfolio has Asset of the required amount.
func (pf *Portfolio) Has(a Asset) error {
	fbAmount, ok := (*pf)[a.ProductID]
	if !ok || fbAmount.LessThan(a.Parts) {
		return ccerrors.InsufficientBalance(a.ProductID, (*pf)[a.ProductID], a.Parts)
	}

	return nil
}

// MarshalJSON implements the json.Marshaler interface for Portfolio.
func (pf Portfolio) MarshalJSON() ([]byte, error) {
	assetArr := make([]Asset, len(pf))
	i := 0
	for c, a := range pf {
		assetArr[i] = Asset{
			ProductID: c,
			Parts:     a,
		}
		i++
	}
	// Ensures array order is always the same.
	sort.Slice(assetArr, func(i, j int) bool {
		return assetArr[i].ProductID < assetArr[j].ProductID
	})

	return json.Marshal(assetArr)
}

// UnmarshalJSON implements the json.Unmarshaler interface for TransactionType.
func (pf *Portfolio) UnmarshalJSON(data []byte) error {
	(*pf) = make(Portfolio)
	var assetArr []Asset
	if err := json.Unmarshal(data, &assetArr); err != nil {
		return ccerrors.FormatMarshal(err)
	}
	for _, bal := range assetArr {
		(*pf)[bal.ProductID] = bal.Parts
	}

	return nil
}
