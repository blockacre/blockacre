package assets

import (
	"encoding/json"
	"sort"

	"github.com/pkg/errors"
	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
)

//go:generate enumer -type=Currency -json -text -output=money_generated.go

// Currency represents an ISO 4217 currency code.
type Currency int

// Currency values.
const (
	GBP Currency = iota + 1 // Start from 1 or json marshalling treats value as a nil.
	USD
)

// Money represents an amount with currency.
type Money struct {
	Amount   decimal.Decimal `json:"amount,omitempty"`
	Currency Currency        `json:"currency,omitempty"`
}

// MoneyTransfer describes the details of m money transfer.
type MoneyTransfer struct {
	Money
	From string `json:"from,omitempty"` // should be the primary key of the from wallet.
	To   string `json:"to,omitempty"`   // should be the primary key of the to wallet.
}

// FiatBalance is a map of fiat currencies [Currency]: Amount.
type FiatBalance map[Currency]decimal.Decimal

// Add or increment money to a FiatBalance.
func (fb *FiatBalance) Add(m Money) {
	// In case FiatBalance is completely empty.
	if len(*fb) == 0 {
		*fb = make(FiatBalance)
	}
	// Add balance.
	(*fb)[m.Currency] = (*fb)[m.Currency].Add(m.Amount)
}

// Sub subtract money from a FiatBalance.
func (fb *FiatBalance) Sub(m Money) error {
	if err := fb.Has(m); err != nil {
		return errors.Wrap(err, "FiatBalance Sub")
	}
	(*fb)[m.Currency] = (*fb)[m.Currency].Sub(m.Amount)

	return nil
}

// Has verifies if a FiatBalance has Money of the required amount.
func (fb *FiatBalance) Has(m Money) error {
	fbAmount, ok := (*fb)[m.Currency]
	if !ok || fbAmount.LessThan(m.Amount) {
		return ccerrors.InsufficientBalance(m.Currency.String(), (*fb)[m.Currency], m.Amount)
	}

	return nil
}

// MarshalJSON implements the json.Marshaler interface for FiatBalance.
func (fb FiatBalance) MarshalJSON() ([]byte, error) {
	moneyArr := make([]Money, len(fb))
	i := 0
	for c, a := range fb {
		moneyArr[i] = Money{
			Amount:   a,
			Currency: c,
		}
		i++
	}
	// Ensures array order is always the same.
	sort.Slice(moneyArr, func(i, j int) bool {
		return moneyArr[i].Currency < moneyArr[j].Currency
	})

	return json.Marshal(moneyArr)
}

// UnmarshalJSON implements the json.Unmarshaler interface for TransactionType.
func (fb *FiatBalance) UnmarshalJSON(data []byte) error {
	(*fb) = make(FiatBalance)
	var moneyArr []Money
	if err := json.Unmarshal(data, &moneyArr); err != nil {
		return ccerrors.FormatMarshal(err)
	}
	for _, bal := range moneyArr {
		(*fb)[bal.Currency] = bal.Amount
	}

	return nil
}
