package assets

import (
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

// Wallet defines a user or product wallet in the system - it has a short reference and can contain cash, available cash and portfolios.
type Wallet struct {
	*records.Default
	Product       bool        `json:"product,omitempty"`       // used in the GetKeyAttrs method. Set to 'CreatedBy' for user wallets and 'ID' for product wallets.
	Ref           string      `json:"ref,omitempty"`           // payment reference to use in BACS transfers.
	Cash          FiatBalance `json:"cash,omitempty"`          // total cash, including uncompleted buy orders.
	CashAvailable FiatBalance `json:"cashAvailable,omitempty"` // cash available to withdraw or spend.
	Portfolio     Portfolio   `json:"portfolio,omitempty"`     // total assets.
	// AssetsPending map[string]Money `json:"assetsPending,omitempty"` // assets for sale.
}

// GetPrimaryKey used to get the owner's id of the wallet. Set to 'CreatedBy' for user wallets and 'ID' for product wallets.
func (w *Wallet) GetPrimaryKey() string {
	if w.Product {
		return w.GetID()
	}

	return w.CreatedBy
}

// GetKeyAttrs overrides the method on records.Default to satisfy the records.Record interface.
func (w *Wallet) GetKeyAttrs() []string {
	return []string{w.GetPrimaryKey()}
}
