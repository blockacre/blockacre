package assets_test

import (
	"encoding/json"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

type portfolioTest struct {
	msg   string
	delta assets.Asset
	exp   assets.Portfolio
}

func hydratedPortfolio() assets.Portfolio {
	return assets.Portfolio{
		"productOneId": decimal.RequireFromString("500"),
		"productTwoId": decimal.RequireFromString("25"),
	}
}

func TestPortfolioAdd(t *testing.T) {
	var userPortfolio assets.Portfolio

	tests := []portfolioTest{
		{
			msg: "Adds Parts to an empty Portfolio",
			delta: assets.Asset{
				ProductID: "productOneId",
				Parts:     decimal.RequireFromString("500"),
			},
			exp: assets.Portfolio{
				"productOneId": decimal.RequireFromString("500"),
			},
		},
		{
			msg: "Adds Parts to an existing Portfolio",
			delta: assets.Asset{
				ProductID: "productOneId",
				Parts:     decimal.RequireFromString("30"),
			},
			exp: assets.Portfolio{
				"productOneId": decimal.RequireFromString("530"),
			},
		},
		{
			msg: "Adds Parts to a new Portfolio",
			delta: assets.Asset{
				ProductID: "productTwoId",
				Parts:     decimal.RequireFromString("25"),
			},
			exp: assets.Portfolio{
				"productOneId": decimal.RequireFromString("530"),
				"productTwoId": decimal.RequireFromString("25"),
			},
		},
	}
	for _, test := range tests {
		t.Log(test.msg)
		userPortfolio.Add(test.delta)
		assert.Equal(t, test.exp, userPortfolio)
	}
}

func TestPortfolioSubAll(t *testing.T) {
	userPortfolio := hydratedPortfolio()
	subParts := decimal.RequireFromString("25")
	test := portfolioTest{
		msg: "Subtracts entire Parts from a Portfolio",
		delta: assets.Asset{
			ProductID: "productTwoId",
			Parts:     subParts,
		},
		exp: assets.Portfolio{
			"productOneId": decimal.RequireFromString("500"),
			"productTwoId": subParts.Sub(subParts), // Using a direct zero value here results in it not being equal, even though the value is zero
		},
	}
	t.Log(test.msg)
	userPortfolio.Sub(test.delta)
	assert.EqualValues(t, test.exp, userPortfolio)
}

func TestPortfolioSubSome(t *testing.T) {
	userPortfolio := hydratedPortfolio()
	tests := []portfolioTest{
		{
			msg: "Subtracts some Parts from a Portfolio",
			delta: assets.Asset{
				ProductID: "productOneId",
				Parts:     decimal.RequireFromString("250"),
			},
			exp: assets.Portfolio{
				"productOneId": decimal.RequireFromString("250"),
				"productTwoId": decimal.RequireFromString("25"),
			},
		},
		{
			msg: "Subtracts some more Parts from a Portfolio",
			delta: assets.Asset{
				ProductID: "productOneId",
				Parts:     decimal.RequireFromString("249"),
			},
			exp: assets.Portfolio{
				"productOneId": decimal.RequireFromString("1"),
				"productTwoId": decimal.RequireFromString("25"),
			},
		},
		{
			msg: "Subtracts some Parts from a different Portfolio",
			delta: assets.Asset{
				ProductID: "productTwoId",
				Parts:     decimal.RequireFromString("5"),
			},
			exp: assets.Portfolio{
				"productOneId": decimal.RequireFromString("1"),
				"productTwoId": decimal.RequireFromString("20"),
			},
		},
	}
	for _, test := range tests {
		t.Log(test.msg)
		userPortfolio.Sub(test.delta)
		assert.Equal(t, test.exp, userPortfolio)
	}
}

func TestPortfolioSubTooMuch(t *testing.T) {
	userPortfolio := hydratedPortfolio()
	expected := hydratedPortfolio()
	t.Log("Returns an error when trying to subtract too many Parts from a Portfolio")
	failDelta := assets.Asset{
		ProductID: "productOneId",
		Parts:     decimal.RequireFromString("501"),
	}
	err := userPortfolio.Sub(failDelta)
	assert.Error(t, err)

	t.Log("Portfolio remains the same if existing parts are not sufficient")
	assert.Equal(t, expected, userPortfolio)
}

func TestPortfolioHas(t *testing.T) {
	userPortfolio := hydratedPortfolio()
	failDelta := assets.Asset{
		ProductID: "productOneId",
		Parts:     decimal.RequireFromString("501"),
	}
	okDelta := assets.Asset{
		ProductID: "productTwoId",
		Parts:     decimal.RequireFromString("25"),
	}

	t.Log("Returns false if Parts > Current Portfolio")
	err := userPortfolio.Has(failDelta)
	assert.Error(t, err)

	t.Log("Returns true if Parts <= Current Portfolio")
	err = userPortfolio.Has(okDelta)
	assert.NoError(t, err)
}

func TestWalletNewAddAsset(t *testing.T) {
	var wallet assets.Wallet
	newParts1 := assets.Asset{
		ProductID: "productOneId",
		Parts:     decimal.RequireFromString("5"),
	}
	newParts2 := assets.Asset{
		ProductID: "productOneId",
		Parts:     decimal.RequireFromString("20"),
	}
	wallet.Portfolio.Add(newParts1)
	wallet.Portfolio.Add(newParts1)
	wallet.Portfolio.Add(newParts2)
	wallet.Portfolio.Add(newParts2)
	expWallet := assets.Wallet{
		Portfolio: assets.Portfolio{
			"productOneId": decimal.RequireFromString("50"),
		},
	}
	assert.Equal(t, expWallet, wallet)
}

func TestWalletExistingAddAsset(t *testing.T) {
	wallet := assets.Wallet{
		Portfolio: assets.Portfolio{
			"productOneId": decimal.RequireFromString("25"),
		},
	}
	newParts1 := assets.Asset{
		ProductID: "productOneId",
		Parts:     decimal.RequireFromString("5"),
	}
	newParts2 := assets.Asset{
		ProductID: "productOneId",
		Parts:     decimal.RequireFromString("20"),
	}
	wallet.Portfolio.Add(newParts1)
	wallet.Portfolio.Add(newParts1)
	wallet.Portfolio.Add(newParts2)
	wallet.Portfolio.Add(newParts2)
	expWallet := assets.Wallet{
		Portfolio: assets.Portfolio{
			"productOneId": decimal.RequireFromString("75"),
		},
	}
	assert.Equal(t, expWallet, wallet)
}

func TestPortfolioJSON(t *testing.T) {
	w := assets.Wallet{
		Default: &records.Default{
			DocType: records.Wallet,
		},
		Portfolio: assets.Portfolio{
			"productOneId": decimal.RequireFromString("50"),
			"productTwoId": decimal.RequireFromString("15"),
		},
	}

	expJSON := `
	{
		"docType":"wallet",
		"portfolio":[{"parts":"50","productID":"productOneId"},{"parts":"15","productID":"productTwoId"}]
	}`

	// Test marshal JSON
	wJSON, err := json.Marshal(w)
	assert.NoError(t, err)
	assert.JSONEq(t, expJSON, string(wJSON))

	// Test Unmarshal JSON
	var newW assets.Wallet
	err = json.Unmarshal([]byte(expJSON), &newW)
	assert.NoError(t, err)
	assert.Equal(t, w, newW)
}
