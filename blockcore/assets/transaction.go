package assets

import (
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

//go:generate enumer -type=TransactionType -trimprefix=TransactionType -transform=lower -json -text -output=transaction_generated.go

// TransactionType represents the type of transaction.
type TransactionType int

// transaction types.
const (
	TransactionTypeDeposit TransactionType = iota + 1 // Start from 1 or json marshalling treats value as a nil.
	TransactionTypeWithdrawal
)

// DepositInput are the inputs needed for executing a deposit.
type DepositInput struct {
	Money
	WalletRef string `json:"walletRef,omitempty"`
}

// IsInput satisfies the InputInterface and allows to identiy this type as an input. Prevents passing non-pointers to the input validate function.
func (i *DepositInput) IsInput() {}

// WithdrawInput are the inputs needed for executing a deposit.
type WithdrawInput struct {
	Money
}

// IsInput satisfies the InputInterface and allows to identiy this type as an input. Prevents passing non-pointers to the input validate function.
func (i *WithdrawInput) IsInput() {}

// Transaction is used when depositing and withdrawing money.
type Transaction struct {
	*records.Default
	*MoneyTransfer
	Type      TransactionType `json:"type,omitempty"`
	WalletRef string          `json:"walletRef,omitempty"`
}
