package assets

import (
	"strconv"

	"gitlab.com/blockacre/blockacre/blockcore/records"
)

// Transfer is used when buying / selling product parts and describes the internal transfers that make up the transfer.
type Transfer struct {
	*records.Default
	Sequence     int            `json:"sequence,omitempty"`
	Vendor       bool           `json:"vendor,omitempty"` // Used to denote if this is a vendor making this transfer (affects amounts paid)
	MatchedOrder string         `json:"matchedOrder,omitempty"`
	Asset        *AssetTransfer `json:"asset,omitempty"`
	Payment      *MoneyTransfer `json:"payment,omitempty"`
	Fee          *MoneyTransfer `json:"fee,omitempty"`
	TotalValue   *Money         `json:"totalValue,omitempty"`
}

// GetKeyAttrs ensures the sequence number is added to the key to prevent overwriting transfers during order-matching.
func (t *Transfer) GetKeyAttrs() []string {
	return []string{t.ID, strconv.Itoa(t.Sequence)}
}
