package assets_test

import (
	"encoding/json"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

type fiatBalanceTest struct {
	msg   string
	delta assets.Money
	exp   assets.FiatBalance
}

func hydratedFiatBalance() assets.FiatBalance {
	return assets.FiatBalance{
		assets.GBP: decimal.RequireFromString("500.25"),
		assets.USD: decimal.RequireFromString("25.33"),
	}
}

func TestFiatBalanceAdd(t *testing.T) {
	var userFiatBalance assets.FiatBalance

	tests := []fiatBalanceTest{
		{
			msg: "Adds Amount to an empty Balance",
			delta: assets.Money{
				Currency: assets.GBP,
				Amount:   decimal.RequireFromString("500.25"),
			},
			exp: assets.FiatBalance{
				assets.GBP: decimal.RequireFromString("500.25"),
			},
		},
		{
			msg: "Adds Amount to an existing Balance",
			delta: assets.Money{
				Currency: assets.GBP,
				Amount:   decimal.RequireFromString("30"),
			},
			exp: assets.FiatBalance{
				assets.GBP: decimal.RequireFromString("530.25"),
			},
		},
		{
			msg: "Adds Amount to a new Balance",
			delta: assets.Money{
				Currency: assets.USD,
				Amount:   decimal.RequireFromString("25.33"),
			},
			exp: assets.FiatBalance{
				assets.GBP: decimal.RequireFromString("530.25"),
				assets.USD: decimal.RequireFromString("25.33"),
			},
		},
	}
	for _, test := range tests {
		t.Log(test.msg)
		userFiatBalance.Add(test.delta)
		assert.Equal(t, test.exp, userFiatBalance)
	}
}

func TestFiatBalanceSubAll(t *testing.T) {
	userFiatBalance := hydratedFiatBalance()
	subAmount := decimal.RequireFromString("25.33")
	test := fiatBalanceTest{
		msg: "Subtracts entire Amount from a Balance",
		delta: assets.Money{
			Currency: assets.USD,
			Amount:   subAmount,
		},
		exp: assets.FiatBalance{
			assets.GBP: decimal.RequireFromString("500.25"),
			assets.USD: subAmount.Sub(subAmount), // Using a direct zero value here results in it not being equal, even though the value is zero
		},
	}
	t.Log(test.msg)
	userFiatBalance.Sub(test.delta)
	assert.EqualValues(t, test.exp, userFiatBalance)
}

func TestFiatBalanceSubSome(t *testing.T) {
	userFiatBalance := hydratedFiatBalance()
	tests := []fiatBalanceTest{
		{
			msg: "Subtracts some Amount from a Balance",
			delta: assets.Money{
				Currency: assets.GBP,
				Amount:   decimal.RequireFromString("250.25"),
			},
			exp: assets.FiatBalance{
				assets.GBP: decimal.RequireFromString("250.00"),
				assets.USD: decimal.RequireFromString("25.33"),
			},
		},
		{
			msg: "Subtracts some more Amount from a Balance",
			delta: assets.Money{
				Currency: assets.GBP,
				Amount:   decimal.RequireFromString("249"),
			},
			exp: assets.FiatBalance{
				assets.GBP: decimal.RequireFromString("1.00"),
				assets.USD: decimal.RequireFromString("25.33"),
			},
		},
		{
			msg: "Subtracts some Amount from a different Balance",
			delta: assets.Money{
				Currency: assets.USD,
				Amount:   decimal.RequireFromString("5.33"),
			},
			exp: assets.FiatBalance{
				assets.GBP: decimal.RequireFromString("1.00"),
				assets.USD: decimal.RequireFromString("20.00"),
			},
		},
	}
	for _, test := range tests {
		t.Log(test.msg)
		userFiatBalance.Sub(test.delta)
		assert.Equal(t, test.exp, userFiatBalance)
	}
}

func TestFiatBalanceSubTooMuch(t *testing.T) {
	userFiatBalance := hydratedFiatBalance()
	expected := hydratedFiatBalance()
	t.Log("Returns an error when trying to subtract too high Amount from a Fiat Balance")
	failDelta := assets.Money{
		Currency: assets.GBP,
		Amount:   decimal.RequireFromString("500.250001"),
	}
	err := userFiatBalance.Sub(failDelta)
	assert.Error(t, err)

	t.Log("Fiat Balance remains the same if the balance is not sufficient")
	assert.Equal(t, expected, userFiatBalance)
}

func TestFiatBalanceHas(t *testing.T) {
	userFiatBalance := hydratedFiatBalance()
	failDelta := assets.Money{
		Currency: assets.GBP,
		Amount:   decimal.RequireFromString("500.250001"),
	}
	okDelta := assets.Money{
		Currency: assets.USD,
		Amount:   decimal.RequireFromString("25.33"),
	}

	t.Log("Returns false if Amount > Current Balance")
	err := userFiatBalance.Has(failDelta)
	assert.Error(t, err)

	t.Log("Returns true if Amount <= Current Balance")
	err = userFiatBalance.Has(okDelta)
	assert.NoError(t, err)
}

func TestWalletNewAddFiat(t *testing.T) {
	var wallet assets.Wallet

	newAmount1 := assets.Money{
		Currency: assets.GBP,
		Amount:   decimal.RequireFromString("5"),
	}
	newAmount2 := assets.Money{
		Currency: assets.GBP,
		Amount:   decimal.RequireFromString("20"),
	}
	wallet.Cash.Add(newAmount1)
	wallet.CashAvailable.Add(newAmount1)
	wallet.Cash.Add(newAmount2)
	wallet.CashAvailable.Add(newAmount2)
	expWallet := assets.Wallet{
		Cash: assets.FiatBalance{
			assets.GBP: decimal.RequireFromString("25"),
		},
		CashAvailable: assets.FiatBalance{
			assets.GBP: decimal.RequireFromString("25"),
		},
	}
	assert.Equal(t, expWallet, wallet)
}

func TestWalletExistingAddFiat(t *testing.T) {
	wallet := assets.Wallet{
		Cash: assets.FiatBalance{
			assets.GBP: decimal.RequireFromString("25"),
		},
		CashAvailable: assets.FiatBalance{
			assets.GBP: decimal.RequireFromString("25"),
		},
	}
	newAmount1 := assets.Money{
		Currency: assets.GBP,
		Amount:   decimal.RequireFromString("5"),
	}
	newAmount2 := assets.Money{
		Currency: assets.GBP,
		Amount:   decimal.RequireFromString("20"),
	}
	wallet.Cash.Add(newAmount1)
	wallet.CashAvailable.Add(newAmount1)
	wallet.Cash.Add(newAmount2)
	wallet.CashAvailable.Add(newAmount2)
	expWallet := assets.Wallet{
		Cash: assets.FiatBalance{
			assets.GBP: decimal.RequireFromString("50"),
		},
		CashAvailable: assets.FiatBalance{
			assets.GBP: decimal.RequireFromString("50"),
		},
	}
	assert.Equal(t, expWallet, wallet)
}

func TestFiatBalanceJSON(t *testing.T) {
	w := assets.Wallet{
		Default: &records.Default{
			DocType: records.Wallet,
		},
		Cash: assets.FiatBalance{
			assets.GBP: decimal.RequireFromString("50"),
			assets.USD: decimal.RequireFromString("15"),
		},
		CashAvailable: assets.FiatBalance{
			assets.GBP: decimal.RequireFromString("100"),
			assets.USD: decimal.RequireFromString("25"),
		},
	}

	expJSON := `
	{
		"docType":"wallet",
		"cash":[{"amount":"50","currency":"GBP"},{"amount":"15","currency":"USD"}],
		"cashAvailable":[{"amount":"100","currency":"GBP"},{"amount":"25","currency":"USD"}]
	}`

	// Test marshal JSON
	wJSON, err := json.Marshal(w)
	assert.NoError(t, err)
	assert.JSONEq(t, expJSON, string(wJSON))

	// Test Unmarshal JSON
	var newW assets.Wallet
	err = json.Unmarshal([]byte(expJSON), &newW)
	assert.NoError(t, err)
	assert.Equal(t, w, newW)
}
