#!/bin/sh
set -e

echo "Running blockcore tests..."
mkdir -p coverage
echo "mode: set" > coverage/cover.out

# Run normal tests, excluding the contracts folder and save results to cover.tmp.out
go test $(go list ./... | grep -v /contracts) -coverprofile=cover.tmp.out -outputdir=coverage

# Run contracts tests with reference to the contracts package and save results to contracts_cover.tmp.out
go test  ./contracts/test -coverpkg=gitlab.com/blockacre/blockacre/blockcore/contracts -coverprofile=contracts_cover.tmp.out -outputdir=coverage

if [ -f coverage/cover.tmp.out ]; then
    # Copy cover.tmp.out content to base cover.out, skipping generated file coverage
    cat coverage/cover.tmp.out | grep -v '^mode: set\|_generated\.go' >> coverage/cover.out
fi
if [ -f coverage/contracts_cover.tmp.out ]; then
    # Copy contracts_cover.tmp.out content to base contracts_cover.out, skipping generated file coverage
    cat coverage/contracts_cover.tmp.out | grep -v '^mode: set\|_generated\.go' >> coverage/cover.out
fi

echo
echo "***************************"
echo "**** Function Coverage ****"
echo "***************************"
echo

# cover.out now contains correct coverage of all our files, without generates
go tool cover -func coverage/cover.out
