package orders

import (
	"github.com/pkg/errors"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/types"
)

func prepareIssue(preparedOrder *PreparedOrder, product *types.Product) error {
	order := preparedOrder.Order
	productWallet, err := preparedOrder.WalletStore.Get(order.GetWalletPrimaryKey())
	if err != nil {
		return errors.Wrap(err, "prepareIssue")
	}

	// Verify that it's possible to make an issuance of this product
	minPartsIssue := product.Minimum.Mul(product.PartsPerUnit)
	isIssuable, availableParts := verifyMinPartsAbovePrice(preparedOrder.MatchOrderBook, product.PartPrice, minPartsIssue)
	if !isIssuable {
		return ccerrors.InsufficientBuyOrders(availableParts, minPartsIssue)
	}

	// Calculate how many full units will be bought
	fullUnits, _ := availableParts.QuoRem(minPartsIssue, 0)
	// Sanity check: do no continue if full units is less than 1
	if fullUnits.IsZero() {
		return ccerrors.Sanity("fullUnits.IsZero", order.GetID())
	}

	// Update the product to show these issued units
	product.Issued = product.Issued.Add(fullUnits)

	// Convert fullUnits to parts
	issueParts := fullUnits.Mul(product.PartsPerUnit)

	// Set the issue order's parts and parts remaining amount
	order.Parts = issueParts
	order.Remaining = issueParts

	// If the product is open, add the required parts to the product's wallet to effectively give an unlimited supply
	if product.Type == types.ProductTypeOpen {
		openAsset := assets.Asset{
			ProductID: product.GetID(),
			Parts:     issueParts,
		}
		productWallet.Portfolio.Add(openAsset)
	}

	return nil
}
