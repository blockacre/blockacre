package orders

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"

	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

// Put submits the order, wallets and orderbook to the ledger and returns the order as bytes.
func Put(stub shim.ChaincodeStubInterface, preparedOrder *PreparedOrder) ([]byte, error) {
	order := preparedOrder.Order
	orderBook := preparedOrder.Orderbook
	matchOrderBook := preparedOrder.MatchOrderBook
	walletStore := preparedOrder.WalletStore

	// Update the order status (starts as active).
	// If the order is not fulfilled, add it to the orderbook and put the orderbook.
	if order.Status != records.StatusFulfilled {
		orderBook.add(order.Limit.Amount, order.Remaining, order.GetID())
		if _, err := ledger.PutObject(stub, orderBook); err != nil {
			return nil, errors.Wrap(err, "Put")
		}
	}
	// If some transfers have happened order remaining will have been reduced and the matchOrderBook will have been updated in the match function, so we need to put it here.
	if order.Remaining.LessThan(order.Parts) {
		if _, err := ledger.PutObject(stub, matchOrderBook); err != nil {
			return nil, errors.Wrap(err, "Put")
		}
	}

	// Sanity check: verify that the order remaining amount is never more that the original parts.
	if order.Remaining.GreaterThan(order.Parts) {
		return nil, ccerrors.Sanity("order.Remaining.GreaterThan(order.Parts)", order.GetID())
	}

	// Commit wallet updates after processing all orders in case more than one order affects a wallet.
	if err := walletStore.Put(); err != nil {
		return nil, errors.Wrap(err, "Put")
	}

	// Put order on the ledger.
	return ledger.PutObject(stub, order)
}
