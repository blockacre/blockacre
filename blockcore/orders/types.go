package orders

import (
	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

//go:generate enumer -type=OrderSide -trimprefix=OrderSide -transform=lower -json -text -output=types_generated.go

// OrderSide represents the type of order.
type OrderSide int

// Order types.
const (
	OrderSideBuy OrderSide = iota + 1 // Start from 1 or json marshalling treats value as a nil.
	OrderSideSell
)

// OrderInput defines the inputs needed for running the 'ordercreate' contract.
type OrderInput struct {
	assets.Asset
	Side OrderSide `json:"side,omitempty"` // Buy or sell side order.
}

// IsInput satisfies the InputInterface and allows to identiy this type as an input. Prevents passing non-pointers to the input validate function.
func (i *OrderInput) IsInput() {}

// OrderIssueInput defines the inputs needed for running the 'orderissuecreate' contract.
type OrderIssueInput struct {
	ProductID string `json:"productID,omitempty"`
}

// IsInput satisfies the InputInterface and allows to identiy this type as an input. Prevents passing non-pointers to the input validate function.
func (i *OrderIssueInput) IsInput() {}

// Order is an order.
type Order struct {
	*records.Default
	*OrderInput
	Vendor    bool            `json:"vendor,omitempty"`    // Is set to true if this is an issuance or redemption.
	Limit     *assets.Money   `json:"limit,omitempty"`     // Price limit that this order can trade at.
	Remaining decimal.Decimal `json:"remaining,omitempty"` // Remaining parts of the order.
}

// GetWalletPrimaryKey is used to get the string needed to construct a key to find the wallet of the order's owner. ProductID if the order is owned by a vendor, CreatedBy when by a user.
func (o *Order) GetWalletPrimaryKey() string {
	if o.Vendor {
		return o.ProductID
	}

	return o.CreatedBy
}

// OrderMinimums are the minimums for the types of order we have.
var OrderMinimums = map[OrderSide]decimal.Decimal{
	OrderSideBuy:  decimal.RequireFromString("5.00"), // 5 pounds,
	OrderSideSell: decimal.RequireFromString("0.01"), // 1 cent,
}
