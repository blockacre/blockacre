package orders

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"

	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

// CreateOrderBooks creates a buy and sell side order book for a new product and puts them on the ledger.
func CreateOrderBooks(stub shim.ChaincodeStubInterface) error {
	sides := OrderSideValues()
	for _, side := range sides {
		recordDefaults, err := records.New(stub, records.StatusActive, records.OrderBook)
		if err != nil {
			return errors.Wrap(err, "CreateOrderBooks")
		}
		orderBook := NewOrderBook(side, recordDefaults)
		if _, err := ledger.PutObject(stub, orderBook); err != nil {
			return errors.Wrap(err, "CreateOrderBooks")
		}
	}

	return nil
}

// GetOrderBooks returns a map of orderbooks for this productID.
func GetOrderBooks(stub shim.ChaincodeStubInterface, productID string) (OrderBooks, error) {
	// Get the matching CreateOrderBooks.
	sides := OrderSideValues()
	obs := make(OrderBooks, len(sides))
	for _, side := range sides {
		orderBook := &OrderBook{
			Default: &records.Default{
				DocType: records.OrderBook,
				ID:      productID,
			},
			Side: side,
		}
		if err := ledger.GetObject(stub, orderBook); err != nil {
			return nil, errors.Wrap(err, "CreateOrderBooks")
		}
		obs[side] = orderBook
	}

	return obs, nil
}
