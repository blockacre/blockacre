package orders

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

// Match takes an order, finds and loops through matchingSide orders, performing asset and cash transfers as required to fulfil those orders.
// The original Order is updated, but it is NOT put, it must be put later.
// Wallets in the walletStore are updated via the `updateWallets` functon but they are NOT put, it must be put later.
// Matching Orders are updated and put straight away.
// Transfer records are created and put straight away.
// nolint:gocognit
func Match(stub shim.ChaincodeStubInterface, preparedOrder *PreparedOrder) error {
	order := preparedOrder.Order
	matchOrderBook := preparedOrder.MatchOrderBook
	walletStore := preparedOrder.WalletStore

	priceIndex := getFilteredSortedIndex(matchOrderBook, order.Limit.Amount)
	// Loop through matched price rows in the order index
	for out, price := range priceIndex {
		row := matchOrderBook.Orders[price.String()]

		// Loop through orderIDs in the matched price row
		for in, matchedOrderID := range row.OrderIDs {
			// Create a sequence number that's used for appending to transfer keys
			sequence := (in + 1) * (out + 1)

			// Get the matchedOrder object from the matchedOrder index ID
			matchedOrder := &Order{
				Default: &records.Default{
					DocType: records.Order,
					ID:      matchedOrderID,
				},
			}
			if err := ledger.GetObject(stub, matchedOrder); err != nil {
				return errors.Wrap(err, "Match")
			}

			// Sanity check: prevent users matching against their own orders
			// Continue because it does not affect logic
			// TODO: log this error
			if order.CreatedBy == matchedOrder.CreatedBy {
				continue
			}

			// Sanity check: currencies match
			// Continue because it does not affect logic
			// TODO: log this error
			if order.Limit.Currency != matchedOrder.Limit.Currency {
				continue
			}

			// Sanity check: verify neither order has already been fulfilled
			// Return in this case because it is an error
			if order.Remaining.LessThanOrEqual(decimal.Zero) {
				return ccerrors.Sanity("order.Remaining.LessThanOrEqual(decimal.Zero)", order.GetID())
			}
			if matchedOrder.Remaining.LessThanOrEqual(decimal.Zero) {
				return ccerrors.Sanity("matchedOrder.Remaining.LessThanOrEqual(decimal.Zero)", order.GetID())
			}

			// Check how many parts should be transferred (the lesser value of the order and matchedOrder remaining amounts)
			transferParts := decimal.Min(order.Remaining, matchedOrder.Remaining)

			// Calculate the price the parts should be traded at, depending on the order side and whether this is a redemption / issuance
			partPrice := decimal.Zero
			switch order.Side {
			case OrderSideBuy:
				// User is buying
				if order.Vendor {
					// This is a redemption, user should receive highest possible amount
					partPrice = decimal.Max(order.Limit.Amount, matchedOrder.Limit.Amount)
				} else {
					// Buyer should pay the lowest price possible for their parts
					partPrice = decimal.Min(order.Limit.Amount, matchedOrder.Limit.Amount)
				}
			case OrderSideSell:
				// User is selling
				if order.Vendor {
					// This is an issuance, user should pay lowest possible amount
					partPrice = decimal.Min(order.Limit.Amount, matchedOrder.Limit.Amount)
				} else {
					// Seller should get the highest price possible for their parts
					partPrice = decimal.Max(order.Limit.Amount, matchedOrder.Limit.Amount)
				}
			}
			// Sanity check: has the partPrice been set?
			if partPrice.IsZero() {
				return ccerrors.Sanity("partPrice.IsZero", order.GetID())
			}

			// Calculate total order amount
			orderAmount := partPrice.Mul(transferParts)

			// Create defaults for the transfer record
			recordDefaults, err := records.New(stub, records.StatusFulfilled, records.Transfer)
			if err != nil {
				return errors.Wrap(err, "Match")
			}

			assetTx := &assets.AssetTransfer{
				Asset: assets.Asset{
					ProductID: order.ProductID,
					Parts:     transferParts,
				},
				PartPrice: partPrice,
				From:      order.GetWalletPrimaryKey(), // GetWalletPrimaryKey was set during order creation to be equal to wallet.GetPrimaryKey() of the order's related wallet
				To:        matchedOrder.GetWalletPrimaryKey(),
			}

			paymentTx := &assets.MoneyTransfer{
				Money: assets.Money{
					Amount:   orderAmount,
					Currency: order.Limit.Currency,
				},
				From: matchedOrder.GetWalletPrimaryKey(),
				To:   order.GetWalletPrimaryKey(),
			}

			transfer := &assets.Transfer{
				Default:      recordDefaults,
				Asset:        assetTx,
				Payment:      paymentTx,
				Fee:          nil,
				MatchedOrder: matchedOrder.GetID(),
				Sequence:     sequence,
				Vendor:       order.Vendor,
			}

			// Transfer Cash & Assets
			// walletStore is updated by reference (via its pointer)
			// updateWallets does NOT put any changes
			if err = updateWallets(transfer, walletStore); err != nil {
				return err
			}

			// Subtract the transfer amount from both orders and update the matchedOrder modified timestamp
			order.Remaining = order.Remaining.Sub(transfer.Asset.Parts)
			matchedOrder.Remaining = matchedOrder.Remaining.Sub(transfer.Asset.Parts)
			matchedOrder.Modified = transfer.Created

			// If matchedOrder is fulfilled, update status
			matchedOrderClosed := matchedOrder.Remaining.IsZero()
			if matchedOrderClosed {
				matchedOrder.Status = records.StatusFulfilled
			}

			// Delete the matchedOrder parts from the matchOrderBook. If the order is closed, it will be deleted from the orderbook
			if err := matchOrderBook.Delete(price, transfer.Asset.Parts, matchedOrder.GetID(), matchedOrderClosed); err != nil {
				return err
			}

			// Put matchedOrder updates
			if _, err := ledger.PutObject(stub, matchedOrder); err != nil {
				return errors.Wrap(err, "Match matchedOrder")
			}

			// Put the details of the transfer
			if _, err := ledger.PutObject(stub, transfer); err != nil {
				return errors.Wrap(err, "Match transfer")
			}

			// If the order.Remaining is zero, the order is fulfilled
			// No need to match anymore, return now
			if order.Remaining.IsZero() {
				order.Status = records.StatusFulfilled

				return nil
			}
		}
	}

	return nil
}
