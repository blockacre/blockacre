package orders

import (
	"encoding/json"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"

	"gitlab.com/blockacre/blockacre/blockcore/records"
)

const (
	orderID1 = "orderID1"
	orderID2 = "orderID2"
	orderID3 = "orderID3"
)

type testsAdd struct {
	price   decimal.Decimal
	parts   decimal.Decimal
	orderID string
	expOB   *OrderBook
}

type testsDel struct {
	price   decimal.Decimal
	parts   decimal.Decimal
	orderID string
	expOB   *OrderBook
	close   bool
}

var (
	base      int64 = 80
	price1Str       = "0.5895128982"
	price2Str       = "0.5687098"
	price1          = decimal.RequireFromString(price1Str)
	price2          = decimal.RequireFromString(price2Str)
	partsHalf       = decimal.NewFromInt(base / 2)
	parts           = decimal.NewFromInt(base)
)

func ob0() *OrderBook {
	return &OrderBook{
		Default:   new(records.Default),
		Side:      OrderSideBuy,
		Orders:    make(orderBookRows),
		BookTotal: decimal.Zero,
	}
}

func ob1() *OrderBook {
	return &OrderBook{
		Default: new(records.Default),
		Side:    OrderSideBuy,
		Orders: orderBookRows{
			price1Str: orderBookRow{
				RowTotal: decimal.NewFromInt(base),
				OrderIDs: []string{orderID1},
			},
		},
		BookTotal: decimal.NewFromInt(base),
	}
}

func ob2() *OrderBook {
	return &OrderBook{
		Default: new(records.Default),
		Side:    OrderSideBuy,
		Orders: orderBookRows{
			price1Str: orderBookRow{
				RowTotal: decimal.NewFromInt(base * 2),
				OrderIDs: []string{orderID1, orderID2},
			},
		},
		BookTotal: decimal.NewFromInt(base * 2),
	}
}

func ob3() *OrderBook {
	return &OrderBook{
		Default: new(records.Default),
		Side:    OrderSideBuy,
		Orders: orderBookRows{
			price1Str: orderBookRow{
				RowTotal: decimal.NewFromInt(base * 2),
				OrderIDs: []string{orderID1, orderID2},
			},
			price2Str: orderBookRow{
				RowTotal: decimal.NewFromInt(base),
				OrderIDs: []string{orderID3},
			},
		},
		BookTotal: decimal.NewFromInt(base * 3),
	}
}

func ob4() *OrderBook {
	return &OrderBook{
		Default: new(records.Default),
		Side:    OrderSideBuy,
		Orders: orderBookRows{
			price1Str: orderBookRow{
				RowTotal: partsHalf,
				OrderIDs: []string{orderID1},
			},
		},
		BookTotal: partsHalf,
	}
}

func TestOrderBookNew(t *testing.T) {
	orderBook := NewOrderBook(OrderSideBuy, new(records.Default))
	ob := ob0()
	assert.Equal(t, ob, orderBook)
}

var addTests = []testsAdd{
	{price1, parts, orderID1, ob1()},
	{price1, parts, orderID2, ob2()},
	{price2, parts, orderID3, ob3()},
}

func TestOrderBookAdd(t *testing.T) {
	ob := ob0()
	for _, test := range addTests {
		ob.add(test.price, test.parts, test.orderID)
		assert.Equal(t, test.expOB, ob)
	}
}

func TestOrderBookInvalidDelete(t *testing.T) {
	invalidTests := []testsDel{
		// Price doesn't exist
		{decimal.RequireFromString("0.0001"), decimal.NewFromInt(241), orderID3, ob3(), false},
		// OrderID doesn't exist against price
		{price2, decimal.NewFromInt(base), orderID2, ob3(), false},
		// Parts > RowTotal
		{price2, decimal.NewFromInt(base + 1), orderID3, ob3(), false},
		// Parts > BookTotal (actually won't reach this one)
		{price2, decimal.NewFromInt(100000), orderID3, ob3(), false},
		// Deleting all parts but not orderID
		{price1, decimal.NewFromInt(base * 2), orderID2, ob3(), true},
		// Deleting all orderIDs but not all parts
		{price2, decimal.NewFromInt(base - 1), orderID3, ob3(), true},
	}
	ob := ob3()
	for _, test := range invalidTests {
		err := ob.Delete(test.price, test.parts, test.orderID, test.close)
		assert.Error(t, err)
		assert.Equal(t, test.expOB, ob)
		t.Log(err)
	}
}

func TestOrderBookDelete(t *testing.T) {
	deleteTests := []testsDel{
		{price2, parts, orderID3, ob2(), true},
		{price1, parts, orderID2, ob1(), true},
		{price1, partsHalf, orderID1, ob4(), false},
		{price1, partsHalf, orderID1, ob0(), true},
	}
	// Test delete functions
	ob := ob3()
	for _, test := range deleteTests {
		ob.Delete(test.price, test.parts, test.orderID, test.close)
		exp, _ := json.Marshal(test.expOB)
		act, _ := json.Marshal(ob)
		assert.Equal(t, exp, act)
	}

	// And double check we can still add there
	for _, test := range addTests {
		ob.add(test.price, test.parts, test.orderID)
		assert.Equal(t, test.expOB, ob)
	}
}

var (
	p0 = decimal.RequireFromString("0")
	p1 = decimal.RequireFromString("0.18098098578979999998889779876")
	p2 = decimal.RequireFromString("0.6809807897")
	p3 = decimal.RequireFromString("0.6809809857895")
	p4 = decimal.RequireFromString("0.68098098578979999998889779876")
	p5 = decimal.RequireFromString("0.6809809857899")
	p6 = decimal.RequireFromString("1.6809809857897")
	p7 = decimal.RequireFromString("2")
)

func ob5(side OrderSide) *OrderBook {
	return &OrderBook{
		Side: side,
		Orders: orderBookRows{
			p4.String(): orderBookRow{
				RowTotal: parts,
				OrderIDs: []string{"orderid1", "orderid2", "orderid3", "orderid4", "orderid5"},
			},
			p3.String(): orderBookRow{
				RowTotal: parts,
				OrderIDs: []string{"orderid1", "orderid2", "orderid3", "orderid4", "orderid5"},
			},
			p5.String(): orderBookRow{
				RowTotal: parts,
				OrderIDs: []string{"orderid1", "orderid2", "orderid3", "orderid4", "orderid5"},
			},
			p1.String(): orderBookRow{
				RowTotal: parts,
				OrderIDs: []string{"orderid1", "orderid2", "orderid3", "orderid4", "orderid5"},
			},
			p6.String(): orderBookRow{
				RowTotal: parts,
				OrderIDs: []string{"orderid1", "orderid2", "orderid3", "orderid4", "orderid5"},
			},
			p2.String(): orderBookRow{
				RowTotal: parts,
				OrderIDs: []string{"orderid1", "orderid2", "orderid3", "orderid4", "orderid5"},
			},
		},
		BookTotal: parts,
	}
}

func TestGetFilteredSortedIndex(t *testing.T) {
	type test struct {
		limit decimal.Decimal
		exp   []decimal.Decimal
	}
	testSelling := []test{
		{p0, []decimal.Decimal{p6, p5, p4, p3, p2, p1}},
		{p1, []decimal.Decimal{p6, p5, p4, p3, p2, p1}},
		{p2, []decimal.Decimal{p6, p5, p4, p3, p2}},
		{p3, []decimal.Decimal{p6, p5, p4, p3}},
		{p4, []decimal.Decimal{p6, p5, p4}},
		{p5, []decimal.Decimal{p6, p5}},
		{p6, []decimal.Decimal{p6}},
		{p7, []decimal.Decimal{}},
	}
	testBuying := []test{
		{p0, []decimal.Decimal{}},
		{p1, []decimal.Decimal{p1}},
		{p2, []decimal.Decimal{p1, p2}},
		{p3, []decimal.Decimal{p1, p2, p3}},
		{p4, []decimal.Decimal{p1, p2, p3, p4}},
		{p5, []decimal.Decimal{p1, p2, p3, p4, p5}},
		{p6, []decimal.Decimal{p1, p2, p3, p4, p5, p6}},
		{p7, []decimal.Decimal{p1, p2, p3, p4, p5, p6}},
	}

	ob := ob5(OrderSideSell)
	for _, test := range testBuying {
		res := getFilteredSortedIndex(ob, test.limit)
		assert.Equal(t, test.exp, res)
	}
	ob = ob5(OrderSideBuy)
	for _, test := range testSelling {
		res := getFilteredSortedIndex(ob, test.limit)
		assert.Equal(t, test.exp, res)
	}
}

func TestGetAndVerifyPartsAbovePrice(t *testing.T) {
	type test struct {
		price     decimal.Decimal
		min       int64
		expVerify bool
		expParts  int64
	}
	tests := []test{
		{p7, base * 3, false, 0},
		{p6, base * 3, false, base},
		{p5, base * 3, false, base * 2},
		{p4, base * 3, true, base * 3},
		{p3, base * 3, true, base * 4},
		{p2, base * 3, true, base * 5},
		{p1, base * 3, true, base * 6},
		{p0, base * 3, true, base * 6},
		{p6, base + 1, false, base},
	}
	ob := ob5(OrderSideBuy)
	t.Log("Test we get correct true / false for comparison against a minimum price (also calls GetPartsAbovePrice)")
	for _, test := range tests {
		ver, totalParts := verifyMinPartsAbovePrice(ob, test.price, decimal.NewFromInt(test.min))

		if test.expParts == 0 {
			// Have to do this because
			assert.True(t, totalParts.IsZero())
		} else {
			assert.Equal(t, decimal.NewFromInt(test.expParts), totalParts)
		}
		assert.Equal(t, test.expVerify, ver)
	}
}
