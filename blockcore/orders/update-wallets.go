package orders

import (
	"github.com/pkg/errors"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
)

func updateWallets(transfer *assets.Transfer, walletStore *ledger.WalletStore) error {
	// Asset Transfer
	if transfer.Asset != nil {
		// Get Asset Wallets
		assetFrom, err := walletStore.Get(transfer.Asset.From)
		if err != nil {
			return errors.Wrap(err, "updateWallets transfer asset")
		}
		assetTo, err := walletStore.Get(transfer.Asset.To)
		if err != nil {
			return errors.Wrap(err, "updateWallets transfer asset")
		}
		// Asset Out
		if err = assetFrom.Portfolio.Sub(transfer.Asset.Asset); err != nil {
			return errors.Wrap(err, "updateWallets transfer asset")
		}
		// Asset In
		assetTo.Portfolio.Add(transfer.Asset.Asset)
	}

	// Payment Transfer
	if transfer.Payment != nil {
		// Get Payment Wallets
		paymentFrom, err := walletStore.Get(transfer.Payment.From)
		if err != nil {
			return errors.Wrap(err, "updateWallets transfer payment")
		}
		paymentTo, err := walletStore.Get(transfer.Payment.To)
		if err != nil {
			return errors.Wrap(err, "updateWallets transfer payment")
		}
		// Payment Out
		if err = paymentFrom.Cash.Sub(transfer.Payment.Money); err != nil {
			return errors.Wrap(err, "updateWallets transfer payment")
		}
		// Payment In
		paymentTo.Cash.Add(transfer.Payment.Money)
		paymentTo.CashAvailable.Add(transfer.Payment.Money)
	}

	// Fees
	if transfer.Fee != nil {
		// Get Fee Wallets
		feeFrom, err := walletStore.Get(transfer.Fee.From)
		if err != nil {
			return errors.Wrap(err, "updateWallets transfer fee")
		}
		feeTo, err := walletStore.Get(transfer.Fee.To)
		if err != nil {
			return errors.Wrap(err, "updateWallets transfer fee")
		}
		// Fee Out
		if err = feeFrom.Cash.Sub(transfer.Fee.Money); err != nil {
			return errors.Wrap(err, "updateWallets transfer fee")
		}
		// Fee In
		feeTo.Cash.Add(transfer.Fee.Money)
		feeTo.CashAvailable.Add(transfer.Fee.Money)
	}

	return nil
}
