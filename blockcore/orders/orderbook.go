package orders

import (
	"errors"
	"sort"

	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/records"
)

// orderBookRow is used with the orderbook.
// It holds details about the number of parts for sale at a given price and an array of those orders' ids.
type orderBookRow struct {
	OrderIDs []string        `json:"orderIDs,omitempty"`
	RowTotal decimal.Decimal `json:"rowTotal,omitempty"`
}

// orderBookRows is a map prices to orderbook rows.
type orderBookRows map[string]orderBookRow

// OrderBook holds a summary of current orders for a given product and the running totalParts.
// Its ledger index is the RecordID (same as the matching ProductID) and orderSide.
type OrderBook struct {
	*records.Default
	Side      OrderSide       `json:"side,omitempty"`
	Orders    orderBookRows   `json:"orders,omitempty"`
	BookTotal decimal.Decimal `json:"bookTotal,omitempty"`
}

// GetKeyAttrs overrides the method on records.Default to satisfy the records.Record interface.
func (ob *OrderBook) GetKeyAttrs() []string {
	return []string{ob.GetID(), ob.Side.String()}
}

// OrderBooks is a map of order sides to orderbooks.
type OrderBooks map[OrderSide]*OrderBook

// NewOrderBook is a constructor for OrderBook.
func NewOrderBook(side OrderSide, defaults *records.Default) *OrderBook {
	return &OrderBook{
		Default:   defaults,
		Side:      side,
		Orders:    make(orderBookRows),
		BookTotal: decimal.Zero,
	}
}

// add adds an price and orderid to an orderbook.
func (ob *OrderBook) add(price, parts decimal.Decimal, orderID string) {
	// In case order book orders is nil.
	if len(ob.Orders) == 0 {
		ob.Orders = make(orderBookRows)
	}

	priceStr := price.String()
	row := ob.Orders[priceStr]

	// Append the orderID and add the parts.
	row.OrderIDs = append(row.OrderIDs, orderID)
	row.RowTotal = row.RowTotal.Add(parts)
	ob.BookTotal = ob.BookTotal.Add(parts)
	// Set the updated row in the map.
	ob.Orders[priceStr] = row
}

// Delete removes part entries from the orderbook. If close is true, the orderID is also removed from the array.
// If a price array no longer has any entries, it is also deleted from the map.
func (ob *OrderBook) Delete(price, parts decimal.Decimal, orderID string, close bool) error {
	const errBase = "OrderBook Delete Failed: "
	priceStr := price.String()
	row, ok := ob.Orders[priceStr]
	if !ok {
		return errors.New(errBase + "no entry matching this price")
	}
	index := getIndex(row.OrderIDs, orderID)
	if index == -1 {
		return errors.New(errBase + "orderID does not exist in the price row")
	}
	if row.RowTotal.LessThan(parts) {
		return errors.New(errBase + "will result in negative row parts")
	}
	if ob.BookTotal.LessThan(parts) {
		return errors.New(errBase + "will result in negative total parts")
	}
	newRowTotal := row.RowTotal.Sub(parts)
	if close {
		rowLen := len(row.OrderIDs)
		zeroRowTotal := newRowTotal.IsZero()
		// If rowLen is 1, it means after closing this orderID, the orderIDs array would be empty - which can only happen if the RowTotal is also zero.
		if rowLen == 1 && !zeroRowTotal {
			return errors.New(errBase + "no orderIDs in row but non-zero row total")
		}
		// If the row length is greater than 1, it means the newRowTotal can not be zero, so return an error.
		if rowLen > 1 && zeroRowTotal {
			return errors.New(errBase + "have orderIDs in row but zero row total")
		}
		if rowLen == 1 && zeroRowTotal {
			// If the Orderbook Price Row has no entries and rowTotal is zero, delete the entire row.
			delete(ob.Orders, priceStr)
			// We don't need to do any more work on the row, so just update the bookTotal and return.
			ob.BookTotal = ob.BookTotal.Sub(parts)

			return nil
		}
		// If we're not completely deleting the row, remove the closed orderID from the index.
		row.OrderIDs = removeIndex(row.OrderIDs, index)
	}
	// If we're not closing the order, just update the row total and the orderBook total.
	row.RowTotal = newRowTotal
	ob.BookTotal = ob.BookTotal.Sub(parts)
	// Set the updated row in the map.
	ob.Orders[priceStr] = row

	return nil
}

// getFilteredSortedIndex returns an orderlist of price keys (by price) above or below limit which can be used to query the orderbook.
func getFilteredSortedIndex(orderBook *OrderBook, limit decimal.Decimal) []decimal.Decimal {
	priceKeys := make([]decimal.Decimal, 0, len(orderBook.Orders))

	// Create a slice of price rows that are compatible with the order's limit.
	for priceStr := range orderBook.Orders {
		price := decimal.RequireFromString(priceStr)
		switch orderBook.Side {
		case OrderSideSell:
			// User is buying.
			// Filter: Buyer's price limit is greater than or equal to the sell order's price.
			if limit.GreaterThanOrEqual(price) {
				priceKeys = append(priceKeys, price)
			}
			// Sort: sort the slice by the lowest price.
			sort.Slice(priceKeys, func(i, j int) bool {
				return priceKeys[i].LessThan(priceKeys[j])
			})
		case OrderSideBuy:
			// User is selling.
			// Filter: Seller's price limit is less than or equal to the buy order's price.
			if limit.LessThanOrEqual(price) {
				priceKeys = append(priceKeys, price)
			}
			// Sort: sort the slice by the highest price.
			sort.Slice(priceKeys, func(i, j int) bool {
				return priceKeys[i].GreaterThan(priceKeys[j])
			})
		}
	}

	return priceKeys
}

// GetPartsAbovePrice loops through price rows returning the total parts of orders who's price is above the supplied price.
func GetPartsAbovePrice(orderBook *OrderBook, price decimal.Decimal) decimal.Decimal {
	// Loop through matched price rows in the order index.
	sum := decimal.Zero
	for rowPrice, row := range orderBook.Orders {
		if decimal.RequireFromString(rowPrice).GreaterThanOrEqual(price) {
			sum = sum.Add(row.RowTotal)
		}
	}

	return sum
}

// verifyMinPartsAbovePrice calls GetPartsAbovePrice.
// Returns true if the result of GetPartsAbovePrice is greater than or equal to the supplied minimum.
func verifyMinPartsAbovePrice(orderBook *OrderBook, price, minimum decimal.Decimal) (bool, decimal.Decimal) {
	parts := GetPartsAbovePrice(orderBook, price)

	return parts.GreaterThanOrEqual(minimum), parts
}

func removeIndex(a []string, i int) []string {
	ret := make([]string, 0)
	ret = append(ret, a[:i]...)

	return append(ret, a[i+1:]...)
}

func getIndex(a []string, v string) int {
	for i, av := range a {
		if av == v {
			return i
		}
	}

	return -1
}
