package orders

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/ccerrors"
	"gitlab.com/blockacre/blockacre/blockcore/ledger"
	"gitlab.com/blockacre/blockacre/blockcore/records"
	"gitlab.com/blockacre/blockacre/blockcore/types"
)

// PreparedOrder is an object containing details to create an order.
type PreparedOrder struct {
	Order          *Order
	Orderbook      *OrderBook
	MatchOrderBook *OrderBook
	WalletStore    *ledger.WalletStore
}

// Prepare runs some validation and finally prepares an order ready for putting on the ledger.
func Prepare(stub shim.ChaincodeStubInterface, input *OrderInput, product *types.Product, issue bool) (*PreparedOrder, error) {
	// Calculate this order's minimum value (its value if executed at the limit price).
	minOrderValue := product.PartPrice.Mul(input.Parts)
	if !issue {
		// If not an issuance, check total order value exceeds the minimum.
		minRequired := OrderMinimums[input.Side]
		if minOrderValue.LessThan(minRequired) {
			return nil, ccerrors.InsufficientOrder(minOrderValue, minRequired)
		}
	}

	// Get the orderbooks for this product.
	OrderBooks, err := GetOrderBooks(stub, product.GetID())
	if err != nil {
		return nil, err
	}

	// Setup new order record.
	recordDefaults, err := records.New(stub, records.StatusActive, records.Order)
	if err != nil {
		return nil, errors.Wrap(err, "Prepare")
	}

	// Create values.
	limit := &assets.Money{
		Amount:   product.PartPrice,
		Currency: product.Currency,
	}

	// Create the order object on preparedOrder.
	// Do this first so we can use methods on the order.
	newOrder := &Order{
		Default:    recordDefaults,
		OrderInput: input,
		Vendor:     issue,
		Limit:      limit,
		Remaining:  input.Parts,
	}

	// Setup the wallet store and get the creator's wallet.
	walletStore := ledger.WalletNewStore(stub)
	creatorWallet, err := walletStore.Get(newOrder.GetWalletPrimaryKey())
	if err != nil {
		return nil, errors.Wrap(err, "Prepare")
	}

	// Setup the prepared order.
	preparedOrder := &PreparedOrder{
		Orderbook:   OrderBooks[input.Side],
		Order:       newOrder,
		WalletStore: walletStore,
	}

	switch input.Side {
	case OrderSideBuy:
		// If this is a buy order...
		preparedOrder.MatchOrderBook = OrderBooks[OrderSideSell]
		// Reduce creator's available cash balance by the maximum total order value (limit * parts).
		// We need to 'reserve' this balance to ensure the user can not withdraw it before the order completes.
		// Returns an error if user does not have a sufficient balance to cover the maximum price of the order.
		limitTotal := assets.Money{
			Amount:   minOrderValue,
			Currency: product.Currency,
		}
		if err = creatorWallet.CashAvailable.Sub(limitTotal); err != nil {
			return nil, errors.Wrap(err, "Prepare")
		}

	case OrderSideSell:
		// If this is a sell order...
		preparedOrder.MatchOrderBook = OrderBooks[OrderSideBuy]
		// Check if this is the product owner selling open product parts (an issuance).
		// If so, call the prepare issue function which will update the some details on the order and add a balance to the product's Portfolio.
		if issue {
			if err := prepareIssue(preparedOrder, product); err != nil {
				return nil, err
			}
		}
		// Check creator's wallet has enough saleable units.
		if err := creatorWallet.Portfolio.Has(input.Asset); err != nil {
			return nil, errors.Wrap(err, "Prepare")
		}
	}

	// Sanity check: does order.parts seem to have been set correctly.
	if preparedOrder.Order.Parts.IsZero() {
		return nil, ccerrors.Sanity("preparedOrder.Order.Parts.IsZero()", preparedOrder.Order.GetID())
	}
	// Sanity check: does order.remaining parts seem to have been set correctly.
	if !preparedOrder.Order.Parts.Equal(preparedOrder.Order.Remaining) {
		return nil, ccerrors.Sanity("!preparedOrder.Order.Parts.Equal(preparedOrder.Order.Remaining)", preparedOrder.Order.GetID())
	}

	return preparedOrder, nil
}
