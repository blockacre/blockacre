package main

import (
	"fmt"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/pkg/errors"

	"gitlab.com/blockacre/blockacre/blockcore/contracts"
)

func main() {
	var cc contracts.BlockCore
	if err := shim.Start(&cc); err != nil {
		err := errors.Wrap(err, "Error starting chaincode.")
		fmt.Println(err)
	}
}
