package testdata

import (
	"bytes"
	"encoding/json"
	"fmt"

	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

func compact(str string) string {
	jsonb := []byte(str)
	var buffer bytes.Buffer
	if err := json.Compact(&buffer, jsonb); err != nil {
		fmt.Println(err)
	}
	return buffer.String()
}

func RqDeposit(walletRef string) string {
	return compact(`
		{
			"amount": "` + CashDeposit.String() + `",
			"currency": "GBP",
			"walletRef": "` + walletRef + `"
		}`)
}

func RqDepositUSD(walletRef string) string {
	return compact(`
		{
			"amount": "` + CashDeposit.String() + `",
			"currency": "USD",
			"walletRef": "` + walletRef + `"
		}`)
}

func RqProductCreate() string {
	return compact(`
		{
			"name": "test",
			"type": "open",
			"currency": "GBP",
			"minimum": "` + unitMinimum.String() + `",
			"unitPrice": "` + priceUnit.String() + `",
			"partsPerUnit": "` + partsPerUnit.String() + `"
		}`)
}

func RqProductUpdate(id string) string {
	return compact(`
		{
			"id":"` + id + `",
			"unitPrice": "` + priceUnitUpdated.String() + `"
		}`)
}

func RqIssue(productID string) string {
	return compact(`
		{
			"productID":"` + productID + `"
		}`)
}

func RqGet(objectType records.DocType, keys []string) string {
	return compact(`
	{
		"objectType": "` + objectType.String() + `",
		"keys": ` + toJSONArray(keys) + `
		}`)
}

func RqOrder(productID string, side orders.OrderSide, parts decimal.Decimal) string {
	return compact(`
		{
			"parts": "` + parts.String() + `",
			"productID": "` + productID + `",
			"side": "` + side.String() + `"
		}`)
}

func RqQuery(dt string) string {
	return compact(`
		{
			"selector": {
				"docType": "` + dt + `",
				"id": 1
			},
			"limit" : 25
		}`)
}

func RqProductQuery(id string) string {
	return compact(`
		{
			"selector": {
				"docType": "product",
				"id": "` + id + `"
			},
			"limit" : 25
		}`)
}

func RqWithdraw(amount decimal.Decimal) string {
	return compact(`
		{
			"amount": "` + amount.String() + `",
			"currency": "GBP"
		}`)
}
