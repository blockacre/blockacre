package testdata

import (
	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/config"
	"gitlab.com/blockacre/blockacre/blockcore/orders"
)

// Values used in various tests.
var (
	// Base.
	CashDeposit      = decimal.NewFromFloat(6082.01)
	partsAdjuster    = decimal.NewFromFloat(1)
	partsPerUnit     = decimal.NewFromFloat(100)
	PriceAdjuster    = decimal.NewFromFloat(0.01)
	priceUnit        = decimal.NewFromFloat(6082)
	priceUnitUpdated = decimal.NewFromFloat(6083)
	PricePart        = priceUnit.DivRound(partsPerUnit, config.Precision)
	PricePartUpdated = priceUnitUpdated.DivRound(partsPerUnit, config.Precision)
	unitMinimum      = decimal.NewFromFloat(1)

	// Product Issue.
	PartsIssueOK           = partsPerUnit.Mul(unitMinimum)
	PartsIssueInsufficient = PartsIssueOK.Sub(partsAdjuster)
	CashIssueOKValue       = PartsIssueOK.Mul(PricePart)
	CashAfterIssueOrder    = CashDeposit.Sub(CashIssueOKValue)

	// For Buy Orders.
	buyOrderOK                     = orders.OrderMinimums[orders.OrderSideBuy].Div(PricePart) // Decimal number parts that can be bought to satisfy minimum buy
	PartsBuyOrderOK                = buyOrderOK.Ceil()                                        // Full number of parts to satisfy minimum buy
	buyOrderOKLimitTotal           = PricePart.Mul(PartsBuyOrderOK)                           // Decimal value for a successful buy order LimitTotal
	CashAfterBuyOrder              = CashDeposit.Sub(buyOrderOKLimitTotal)                    // Cash available balance after successful buy order
	PartsBuyOrderInsufficientCash  = CashDeposit.Div(PricePart).Ceil()                        // Full number of parts that are more than the user can afford
	PartsBuyOrderInsufficientTotal = buyOrderOK.Floor()                                       // Full number of parts that will not satisfy minimum buy

	// For Sell Orders.
	sellOrderOK                      = orders.OrderMinimums[orders.OrderSideSell].Div(PricePart) // Decimal number parts that can be bought to satisfy minimum sell
	PartsSellOrderOK                 = sellOrderOK.Ceil()                                        // Full number of parts to satisfy minimum sell
	CashAfterSellOrder               = CashDeposit.Sub(priceUnit)                                // Cash available balance after successful sell order
	PartsSellOrderInsufficientAssets = PartsIssueOK.Add(partsAdjuster)                           // Full number of parts that are more than the user owns - assuming the test buys enough for an issue
	PartsSellOrderInsufficientTotal  = sellOrderOK.Floor()                                       // Full number of parts that will not satisfy minimum sell
)
