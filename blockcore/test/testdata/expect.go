package testdata

import (
	"encoding/json"
	"strconv"
	"strings"
	"time"

	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/orders"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

type txDetailsType struct {
	ID        string         `json:"id,omitempty"`
	CreatedBy string         `json:"createdBy,omitempty"`
	Created   *time.Time     `json:"created,omitempty"`
	Modified  *time.Time     `json:"modified,omitempty"`
	Status    records.Status `json:"status,omitempty"`
}

func convertToTxDetails(rec records.Default) *txDetailsType {
	tx := &txDetailsType{
		ID:        rec.ID,
		CreatedBy: rec.CreatedBy,
		Created:   rec.Created,
		Modified:  rec.Modified,
		Status:    rec.Status,
	}
	return tx
}

func recDefaults(rec records.Default) string {
	txDetails := convertToTxDetails(rec)
	defs, err := json.Marshal(txDetails)
	if err != nil {
		panic(err)
	}
	s := string(defs)
	s = strings.TrimSuffix(s, "}")
	return s
}

func toJSONArray(arr []string) string {
	json, _ := json.Marshal(arr)
	return string(json)
}

func ExpStateProduct(rec records.Default) string {
	return recDefaults(rec) + `,
		"docType": "product",
		"currency": "GBP",
		"minimum": "` + unitMinimum.String() + `",
		"name": "test",
		"partPrice": "` + PricePart.String() + `",
		"partsPerUnit": "` + partsPerUnit.String() + `",
		"type": "open",
		"unitPrice": "` + priceUnit.String() + `",
		"issued": "0"
	}`
}

func ExpStateUpdateProduct(rec records.Default) string {
	return recDefaults(rec) + `,
		"docType": "product",
		"currency": "GBP",
		"minimum": "` + unitMinimum.String() + `",
		"name": "test",
		"partPrice": "` + PricePartUpdated.String() + `",
		"partsPerUnit":"` + partsPerUnit.String() + `",
		"type": "open",
		"unitPrice": "` + priceUnitUpdated.String() + `",
		"issued": "0"
	}`
}

func ExpStateEmptyOrderBook(rec records.Default, side orders.OrderSide) string {
	return recDefaults(rec) + `,
		"docType": "orderbook",
		"side": "` + side.String() + `",
		"bookTotal": "0"
	}`
}

func ExpStateOrderBook(rec records.Default, side orders.OrderSide, price, parts decimal.Decimal, orderIDs []string) string {
	return recDefaults(rec) + `,
		"docType": "orderbook",
		"side": "` + side.String() + `",
		"orders": {
			"` + price.String() + `": {
				"orderIDs": ["` + strings.Join(orderIDs, `","`) + `"],
				"rowTotal": "` + parts.String() + `"
			}
		},
		"bookTotal": "` + parts.String() + `"
	}`
}

func ExpStateOrder(rec records.Default, productID string, side orders.OrderSide, parts decimal.Decimal) string {
	return recDefaults(rec) + `,
		"docType":"order",
		"productID":"` + productID + `",
		"limit": {
			"amount":"` + PricePart.String() + `",
			"currency":"GBP"
		},
		"parts": "` + parts.String() + `",
		"remaining": "` + parts.String() + `",
		"side":"` + side.String() + `"
	}`
}

func ExpStateProductOrder(rec records.Default, productID string, side orders.OrderSide, parts decimal.Decimal) string {
	return recDefaults(rec) + `,
		"docType":"order",
		"vendor": true,
		"productID":"` + productID + `",
		"limit": {
			"amount":"` + PricePart.String() + `",
			"currency":"GBP"
		},
		"parts": "` + parts.String() + `",
		"remaining": "0",
		"side":"` + side.String() + `"
	}`
}

func ExpStateFulfilledSellOrder(rec records.Default, productID string) string {
	return recDefaults(rec) + `,
		"docType":"order",
		"productID":"` + productID + `",
		"limit": {
			"amount":"` + PricePartUpdated.String() + `",
			"currency":"GBP"
		},
		"parts":` + PartsSellOrderOK.String() + `,
		"remaining": 0,
		"side":"Sell"
	}`
}

func ExpStateTxIssue(rec records.Default, investorID, productID, buyOrderID string, sequence int, parts decimal.Decimal) string {
	amount := PricePart.Mul(parts)
	return recDefaults(rec) + `,
		"docType":"transfer",
		"vendor": true,
		"matchedOrder": "` + buyOrderID + `",
		"sequence": ` + strconv.Itoa(sequence) + `,
		"asset":{
			"parts": "` + parts.String() + `",
			"productID":"` + productID + `",
			"partPrice": "` + PricePart.String() + `",
			"from":"` + productID + `",
			"to":"` + investorID + `"
		},
		"payment":{
			"amount":"` + amount.String() + `",
			"currency":"GBP",
			"from":"` + investorID + `",
			"to":"` + productID + `"
		}
	}`
}

func ExpStateTxDeposit(rec records.Default, investorID, walletRef, curr string) string {
	return recDefaults(rec) + `,
		"docType": "transaction",
		"amount": "` + CashDeposit.String() + `",
		"currency": "` + curr + `",
		"from": "BankAccNumber",
		"to": "` + investorID + `",
		"type": "deposit",
		"walletRef": "` + walletRef + `"
	}`
}

func ExpStateTxWithdraw(rec records.Default, investorID, walletRef string, amount decimal.Decimal) string {
	return recDefaults(rec) + `,
		"docType": "transaction",
		"amount": "` + amount.String() + `",
		"currency": "GBP",
		"from": "` + investorID + `",
		"to": "BankAccNumber",
		"type": "withdrawal",
		"walletRef": "` + walletRef + `"
	}`
}

func ExpWalletNew(rec records.Default, ref string) string {
	return recDefaults(rec) + `,
		"docType": "wallet",
		"ref":"` + ref + `"
	}`
}

func ExpWalletNewProduct(rec records.Default, ref string) string {
	return recDefaults(rec) + `,
		"docType": "wallet",
		"product": true,
		"ref":"` + ref + `"
	}`
}

func ExpWalletDeposit(rec records.Default, ref string) string {
	return recDefaults(rec) + `,
		"docType": "wallet",
		"ref":"` + ref + `",
		"cash":[{"currency": "GBP", "amount":"` + CashDeposit.String() + `"}],
		"cashAvailable":[{"currency": "GBP", "amount":"` + CashDeposit.String() + `"}]
	}`
}

func ExpWalletDeposit2(rec records.Default, ref string) string {
	depAmount := CashDeposit.Mul(decimal.RequireFromString("2"))
	return recDefaults(rec) + `,
		"docType": "wallet",
		"ref":"` + ref + `",
		"cash":[{"currency": "GBP", "amount":"` + depAmount.String() + `"}],
		"cashAvailable":[{"currency": "GBP", "amount":"` + depAmount.String() + `"}]
	}`
}

func ExpWalletDeposit3(rec records.Default, ref string) string {
	depAmount := CashDeposit.Mul(decimal.RequireFromString("2"))
	return recDefaults(rec) + `,
		"docType": "wallet",
		"ref":"` + ref + `",
		"cash":[{"currency": "GBP", "amount":"` + depAmount.String() + `"}, {"currency": "USD", "amount":"` + CashDeposit.String() + `"}],
		"cashAvailable":[{"currency": "GBP", "amount":"` + depAmount.String() + `"}, {"currency": "USD", "amount":"` + CashDeposit.String() + `"}]
	}`
}

func ExpWallet(rec records.Default, ref, assetID string, cash, cashAvailable, parts decimal.Decimal) string {
	return recDefaults(rec) + `,
		"docType": "wallet",
		"ref":"` + ref + `",
		"cash":[{"currency": "GBP", "amount":"` + cash.String() + `"}],
		"cashAvailable":[{"currency": "GBP", "amount":"` + cashAvailable.String() + `"}],
		"portfolio": [{"productID": "` + assetID + `", "parts": "` + parts.String() + `"}]
	}`
}

func ExpWalletCashOnly(rec records.Default, ref string, cash, cashAvailable decimal.Decimal) string {
	return recDefaults(rec) + `,
		"docType": "wallet",
		"ref":"` + ref + `",
		"cash":[{"currency": "GBP", "amount":"` + cash.String() + `"}],
		"cashAvailable":[{"currency": "GBP", "amount":"` + cashAvailable.String() + `"}]
	}`
}

func ExpWalletProduct(rec records.Default, ref, assetID string, cash, cashAvailable, parts decimal.Decimal) string {
	return recDefaults(rec) + `,
		"docType": "wallet",
		"product": true,
		"ref":"` + ref + `",
		"cash":[{"currency": "GBP", "amount":"` + cash.String() + `"}],
		"cashAvailable":[{"currency": "GBP", "amount":"` + cashAvailable.String() + `"}],
		"portfolio": [{"productID": "` + assetID + `", "parts": "` + parts.String() + `"}]
	}`
}
