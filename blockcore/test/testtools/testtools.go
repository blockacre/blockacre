package testtools

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/golang/protobuf/proto"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-chaincode-go/shimtest"
	"github.com/hyperledger/fabric-protos-go/msp"
	"github.com/hyperledger/fabric-protos-go/peer"
	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"gitlab.com/blockacre/blockacre/blockcore/records"
)

func parseFunctionAndArgs(function string, args []string) [][]byte {
	argsAsBytes := [][]byte{
		[]byte(function),
	}
	for _, arg := range args {
		argsAsBytes = append(argsAsBytes, []byte(arg))
	}

	return argsAsBytes
}

func LogEntireState(t *testing.T, stub shim.ChaincodeStubInterface) {
	resultsIterator, rawErr := stub.GetStateByRange("", "")
	if rawErr != nil {
		t.Fatal(rawErr)
	}
	defer resultsIterator.Close()
	fmt.Println("*********** ENTIRE STATE ***********")
	for resultsIterator.HasNext() {
		kv, rawErr := resultsIterator.Next()
		if rawErr != nil {
			t.Fatal(rawErr)
		}
		key := kv.GetKey()
		val := kv.GetValue()
		objT, keys, _ := stub.SplitCompositeKey(key)
		fmt.Println(objT, keys)
		fmt.Println(string(val))
		fmt.Println("------")
	}
}

func SetCreator(t *testing.T, stub *shimtest.MockStub, cert string) {
	sid := &msp.SerializedIdentity{Mspid: "org1MSP", IdBytes: []byte(cert)}
	b, rawErr := proto.Marshal(sid)
	if rawErr != nil {
		t.Fatal(rawErr)
	}
	stub.Creator = b
}

// CheckState uses the supplied record to generate a key, using the record's GetObjectType and GetKeyAttrs methods.
// It fetches the record from the ledger and verifies it against the supplied expectation.
func CheckState(t *testing.T, stub shim.ChaincodeStubInterface, expected string, rec records.Record) {
	oType := rec.GetObjectType()
	oKey := rec.GetKeyAttrs()
	t.Logf("Check state of: '%v' with key '%v'", oType, oKey)
	key, _ := stub.CreateCompositeKey(oType.String(), oKey)
	bytes, rawErr := stub.GetState(key)
	assert.NoError(t, rawErr)
	assert.NotNil(t, bytes)
	assert.JSONEq(t, expected, string(bytes))
}

func CheckQuery(t *testing.T, stub *shimtest.MockStub, expected string, args ...string) {
	functionAndArgsAsBytes := parseFunctionAndArgs("query", args)
	resp := stub.MockInvoke(fake.CharactersN(16), functionAndArgsAsBytes)
	assert.Equal(t, int32(shim.OK), resp.Status)
	assert.NotNil(t, resp.Payload)
	actual, rawErr := json.Marshal(resp.Payload)
	assert.NoError(t, rawErr)
	assert.JSONEq(t, expected, string(actual))
}

func CheckInvoke(t *testing.T, stub *shimtest.MockStub, function string, args ...string) (resp peer.Response, rec records.Default) {
	functionAndArgsAsBytes := parseFunctionAndArgs(function, args)
	resp = stub.MockInvoke(fake.CharactersN(24), functionAndArgsAsBytes)
	if !assert.Equal(t, int32(shim.OK), resp.Status) {
		t.Fatal("CheckInvoke failed unexpectedly")
	}
	json.Unmarshal(resp.GetPayload(), &rec)

	return
}

func CheckBadQuery(t *testing.T, stub *shimtest.MockStub, expectedHTTPStatus int32, args ...string) {
	functionAndArgsAsBytes := parseFunctionAndArgs("query", args)
	act := stub.MockInvoke(fake.CharactersN(16), functionAndArgsAsBytes)
	assert.Equal(t, expectedHTTPStatus, act.Status)
}

func CheckBadInvoke(t *testing.T, stub *shimtest.MockStub, expectedHTTPStatus int32, function string, args ...string) {
	functionAndArgsAsBytes := parseFunctionAndArgs(function, args)
	act := stub.MockInvoke(fake.CharactersN(16), functionAndArgsAsBytes)
	assert.Equal(t, expectedHTTPStatus, act.Status)
}
