package testtools

import (
	"testing"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-chaincode-go/shimtest"
	"github.com/hyperledger/fabric-protos-go/peer"
)

type EmptyChaincode struct {
}

func (t *EmptyChaincode) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

func (t *EmptyChaincode) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

func InitEmptyStub(t *testing.T) (stub *shimtest.MockStub) {
	var cc EmptyChaincode
	stub = shimtest.NewMockStub("__TEST__", &cc)
	stub.MockInit("__TEST_INIT__", nil)

	return stub
}
