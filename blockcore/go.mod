module gitlab.com/blockacre/blockacre/blockcore

go 1.14

replace github.com/stickypixel/hyperledger/rbac => ./rbac

require (
	github.com/corpix/uarand v0.1.1 // indirect
	github.com/golang/protobuf v1.4.2
	github.com/hyperledger/fabric-chaincode-go v0.0.0-20200728190242-9b3ae92d8664
	github.com/hyperledger/fabric-protos-go v0.0.0-20200728190333-526bfc137380
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/pkg/errors v0.9.1
	github.com/shopspring/decimal v1.2.0
	github.com/speps/go-hashids v2.0.0+incompatible
	github.com/stickypixel/hyperledger/rbac v0.0.0-00010101000000-000000000000
	github.com/stretchr/testify v1.6.1
	go.uber.org/zap v1.15.0
)
