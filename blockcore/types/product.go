package types

import (
	"github.com/shopspring/decimal"

	"gitlab.com/blockacre/blockacre/blockcore/assets"
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

//go:generate enumer -type=ProductType -trimprefix=ProductType -transform=lower -json -text -output=product_generated.go

// ProductType represents the type of product.
type ProductType int

// Product types.
const (
	ProductTypeOpen ProductType = iota + 1 // Start from 1 or json marshalling treats value as a nil.
	ProductTypeClosed
)

// ProductCreateInput defines the inputs needed for creating a product.
type ProductCreateInput struct {
	Name         string          `json:"name,omitempty"`         // name of product.
	Type         ProductType     `json:"type,omitempty"`         // product type.
	Currency     assets.Currency `json:"currency,omitempty"`     // product currency.
	Minimum      decimal.Decimal `json:"minimum,omitempty"`      // minimum amount of units for issuance and redemption.
	UnitPrice    decimal.Decimal `json:"unitPrice,omitempty"`    // price of a full unit.
	PartsPerUnit decimal.Decimal `json:"partsPerUnit,omitempty"` // number of parts per unit.
}

// IsInput satisfies the InputInterface and allows to identiy this type as an input. Prevents passing non-pointers to the input validate function.
func (i *ProductCreateInput) IsInput() {}

// ProductUpdateInput defines the inputs needed for running the 'productupdate' contract.
type ProductUpdateInput struct {
	ID        string          `json:"id,omitempty"`
	UnitPrice decimal.Decimal `json:"unitPrice,omitempty"` // new price of a full unit.
}

// IsInput satisfies the InputInterface and allows to identiy this type as an input. Prevents passing non-pointers to the input validate function.
func (i *ProductUpdateInput) IsInput() {}

// Product defines a product record.
type Product struct {
	*records.Default
	*ProductCreateInput
	PartPrice decimal.Decimal `json:"partPrice,omitempty"` // the price of a unit part.
	Issued    decimal.Decimal `json:"issued,omitempty"`    // total amount of units in circulation on Blockacre.
}
