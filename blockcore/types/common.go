package types

import (
	"gitlab.com/blockacre/blockacre/blockcore/records"
)

// GetInput defines the inputs needed for running the 'get' contract.
type GetInput struct {
	ObjectType records.DocType `json:"objectType,omitempty"`
	Keys       []string        `json:"keys,omitempty"`
}

// IsInput satisfies the InputInterface and allows to identiy this type as an input. Prevents passing non-pointers to the input validate function.
func (i *GetInput) IsInput() {}
