# Base Repository Configuration

This project is generated and maintained using a Yeoman Generator.

## Pre-Requisites

1. Yeoman generator, `npm install -g yo`
1. Yarn is used for Node dependencies, `brew install yarn`
1. Node 10 is the required build environment, `nvm install 10`

## Installing and / or Updating to latest Yeoman templates

1. Clone `https://gitlab.com/sticky-pixel/tools/generator-base.git`
1. Run `yarn install` in the generator repository
1. Run `yarn link` in the generator repository. This will link the package locally so it can be used with local changes.
1. Run `yo base` in your project repository to install or update your project

# Deployment

1. `yarn release` to update version numbers

## Modifications

- Suggests VS Code plugins
- Runs commit lint when committing
- husky, see: `.huskyrc.js`. Runs `lint-staged`, `prettier` and `commitlint` on staged files when committing
- lint-staged, see: `lint-staged.config.js`. Ensures file extensions are in lower case, runs `eslint`, `imagemin`, `stylelint` and `svgo` on staged files when committing
- Releases, `yarn release` - suggests version number, updates `package.json`, commits, tags and pushes
- Has a basic Prettier config
- Has an opinionated SVG optimiser
- Does NOT opinionate JS or TS, configuration files should be added to the project
