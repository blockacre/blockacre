/* This is a generated file, do not edit it directly */
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  AWSDate: string;
  AWSDateTime: string;
  AWSEmail: string;
  AWSIPAddress: string;
  AWSJSON: string;
  AWSPhone: string;
  AWSTime: string;
  AWSTimestamp: number;
  AWSURL: string;
  BigInt: number;
  Double: number;
};

export enum Currency {
  GBP = "GBP",
  USD = "USD",
}

export type Asset = {
  __typename?: "Asset";
  productID: Scalars["ID"];
  parts: Scalars["Int"];
};

export type AssetTransfer = {
  __typename?: "AssetTransfer";
  productID: Scalars["ID"];
  parts: Scalars["Int"];
  partPrice: Scalars["Float"];
  from: Scalars["ID"];
  to: Scalars["ID"];
};

export type Money = {
  __typename?: "Money";
  amount: Scalars["Float"];
  currency: Currency;
};

export type MoneyTransfer = {
  __typename?: "MoneyTransfer";
  amount: Scalars["Float"];
  currency: Currency;
  from: Scalars["ID"];
  to: Scalars["ID"];
};

export type Transfer = Record & {
  __typename?: "Transfer";
  id: Scalars["ID"];
  createdBy: Scalars["ID"];
  created: Scalars["AWSDateTime"];
  docType: DocType;
  modified?: Maybe<Scalars["AWSDateTime"]>;
  status: Status;
  sequence: Scalars["Int"];
  vendor: Scalars["Boolean"];
  matchedOrder: Scalars["ID"];
  asset: AssetTransfer;
  payment: MoneyTransfer;
  fee: MoneyTransfer;
  totalValue: Money;
};

export enum TransactionType {
  deposit = "deposit",
  withdrawal = "withdrawal",
}

export type DepositInput = {
  amount: Scalars["Float"];
  currency: Currency;
  walletRef: Scalars["ID"];
};

export type WithdrawInput = {
  amount: Scalars["Float"];
  currency: Currency;
};

export type Transaction = Record & {
  __typename?: "Transaction";
  id: Scalars["ID"];
  createdBy: Scalars["ID"];
  created: Scalars["AWSDateTime"];
  docType: DocType;
  modified?: Maybe<Scalars["AWSDateTime"]>;
  status: Status;
  amount: Scalars["Float"];
  currency: Currency;
  from: Scalars["ID"];
  to: Scalars["ID"];
  type: TransactionType;
  walletRef: Scalars["ID"];
};

export type Record = {
  docType: DocType;
  id: Scalars["ID"];
  createdBy: Scalars["ID"];
  created: Scalars["AWSDateTime"];
  modified?: Maybe<Scalars["AWSDateTime"]>;
  status: Status;
};

export enum Status {
  active = "active",
  fulfilled = "fulfilled",
  failed = "failed",
}

export enum DocType {
  order = "order",
  orderBook = "orderBook",
  product = "product",
  transaction = "transaction",
  transfer = "transfer",
  wallet = "wallet",
  walletCounter = "walletCounter",
  walletIndex = "walletIndex",
}

export enum OrderFrequency {
  monthly = "monthly",
  single = "single",
}

export enum Group {
  admin = "admin",
  vendor = "vendor",
  investor = "investor",
}

export type Error = {
  __typename?: "Error";
  code?: Maybe<Scalars["String"]>;
  detail?: Maybe<Scalars["String"]>;
  source: Scalars["String"];
  status: Scalars["Int"];
  title: Scalars["String"];
};

export enum OrderSide {
  buy = "buy",
  sell = "sell",
}

export type OrderInput = {
  parts: Scalars["Int"];
  productID: Scalars["ID"];
  side: OrderSide;
};

export type OrderIssueInput = {
  productID: Scalars["ID"];
};

export type Order = Record & {
  __typename?: "Order";
  id: Scalars["ID"];
  createdBy: Scalars["ID"];
  created: Scalars["AWSDateTime"];
  docType: DocType;
  modified?: Maybe<Scalars["AWSDateTime"]>;
  status: Status;
  parts: Scalars["Int"];
  productID: Scalars["ID"];
  side: OrderSide;
  vendor: Scalars["Boolean"];
  limit: Money;
  remaining: Scalars["Int"];
};

export enum ProductType {
  open = "open",
  closed = "closed",
}

export type ProductCreateInput = {
  name: Scalars["String"];
  type: ProductType;
  currency: Currency;
  minimum: Scalars["Int"];
  unitPrice: Scalars["Float"];
  partsPerUnit: Scalars["Int"];
};

export type ProductUpdateInput = {
  id: Scalars["ID"];
  unitPrice: Scalars["Float"];
};

export type Product = Record & {
  __typename?: "Product";
  id: Scalars["ID"];
  createdBy: Scalars["ID"];
  created: Scalars["AWSDateTime"];
  docType: DocType;
  modified?: Maybe<Scalars["AWSDateTime"]>;
  status: Status;
  name: Scalars["String"];
  type: ProductType;
  currency: Currency;
  minimum: Scalars["Int"];
  unitPrice: Scalars["Float"];
  partsPerUnit: Scalars["Int"];
  partPrice: Scalars["Float"];
  issued: Scalars["Int"];
};

export type Response = {
  error?: Maybe<Error>;
};

export type ProductResponse = Response & {
  __typename?: "ProductResponse";
  data?: Maybe<Product>;
  error?: Maybe<Error>;
};

export type ProductsResponse = Response & {
  __typename?: "ProductsResponse";
  data?: Maybe<Array<Maybe<Product>>>;
  error?: Maybe<Error>;
};

export type OrderResponse = Response & {
  __typename?: "OrderResponse";
  data?: Maybe<Order>;
  error?: Maybe<Error>;
};

export type OrdersResponse = Response & {
  __typename?: "OrdersResponse";
  data?: Maybe<Array<Maybe<Order>>>;
  error?: Maybe<Error>;
};

export type TransactionResponse = Response & {
  __typename?: "TransactionResponse";
  data?: Maybe<Transaction>;
  error?: Maybe<Error>;
};

export type TransactionsResponse = Response & {
  __typename?: "TransactionsResponse";
  data?: Maybe<Array<Maybe<Transaction>>>;
  error?: Maybe<Error>;
};

export type TransferResponse = Response & {
  __typename?: "TransferResponse";
  data?: Maybe<Transfer>;
  error?: Maybe<Error>;
};

export type TransfersResponse = Response & {
  __typename?: "TransfersResponse";
  data?: Maybe<Array<Maybe<Transfer>>>;
  error?: Maybe<Error>;
};

export type WalletResponse = Response & {
  __typename?: "WalletResponse";
  data?: Maybe<Wallet>;
  error?: Maybe<Error>;
};

export type WalletsResponse = Response & {
  __typename?: "WalletsResponse";
  data?: Maybe<Array<Maybe<Wallet>>>;
  error?: Maybe<Error>;
};

export type GetInput = {
  keys: Array<Scalars["String"]>;
};

export type PartialInput = {
  keys?: Maybe<Array<Scalars["String"]>>;
};

export type Selector = {
  docType: DocType;
  id?: Maybe<Scalars["ID"]>;
};

export type Sort = {
  unknown: Scalars["String"];
};

export type QueryInput = {
  selector: Selector;
  limit?: Maybe<Scalars["Int"]>;
  skip?: Maybe<Scalars["Int"]>;
  sort?: Maybe<Sort>;
  fields?: Maybe<Array<Maybe<Scalars["String"]>>>;
};

export type Query = {
  __typename?: "Query";
  orders?: Maybe<OrdersResponse>;
  products?: Maybe<ProductsResponse>;
  transactions?: Maybe<TransactionsResponse>;
  transfers?: Maybe<TransfersResponse>;
  user?: Maybe<WalletResponse>;
  wallets?: Maybe<WalletsResponse>;
};

export type QueryOrdersArgs = {
  input: QueryInput;
};

export type QueryProductsArgs = {
  input: QueryInput;
};

export type QueryTransactionsArgs = {
  input: QueryInput;
};

export type QueryTransfersArgs = {
  input: QueryInput;
};

export type QueryWalletsArgs = {
  input: QueryInput;
};

export type Mutation = {
  __typename?: "Mutation";
  productcreate?: Maybe<ProductResponse>;
  productupdate?: Maybe<ProductResponse>;
  ordercreate?: Maybe<OrderResponse>;
  orderissuecreate?: Maybe<OrderResponse>;
  walletcreate?: Maybe<WalletResponse>;
  deposit?: Maybe<TransactionResponse>;
  withdraw?: Maybe<TransactionResponse>;
};

export type MutationProductcreateArgs = {
  input?: Maybe<ProductCreateInput>;
};

export type MutationProductupdateArgs = {
  input?: Maybe<ProductUpdateInput>;
};

export type MutationOrdercreateArgs = {
  input?: Maybe<OrderInput>;
};

export type MutationOrderissuecreateArgs = {
  input?: Maybe<OrderIssueInput>;
};

export type MutationDepositArgs = {
  input?: Maybe<DepositInput>;
};

export type MutationWithdrawArgs = {
  input?: Maybe<WithdrawInput>;
};

export type Wallet = Record & {
  __typename?: "Wallet";
  id: Scalars["ID"];
  createdBy: Scalars["ID"];
  created: Scalars["AWSDateTime"];
  docType: DocType;
  modified?: Maybe<Scalars["AWSDateTime"]>;
  status: Status;
  product?: Maybe<Scalars["Boolean"]>;
  ref: Scalars["ID"];
  cash?: Maybe<Array<Money>>;
  cashAvailable?: Maybe<Array<Money>>;
  portfolio?: Maybe<Array<Asset>>;
};
