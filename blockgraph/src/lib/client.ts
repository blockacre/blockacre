import http from "http";
import https from "https";
import axios, { AxiosRequestConfig } from "axios";

export const client = (basicAuth?: string) => {
  const base: AxiosRequestConfig = {
    baseURL: "http://localhost:8081",
    // keepAlive pools and reuses TCP connections, so it's faster
    httpAgent: new http.Agent({ keepAlive: true }),
    httpsAgent: new https.Agent({ keepAlive: true }),
    // cap the maximum content length we'll accept to 50MBs, just in case
    maxContentLength: 50 * 1000 * 1000,
    maxRedirects: 10,
    timeout: 60000,
  };
  if (basicAuth) {
    base.headers = {
      Authorization: basicAuth,
    };
  }
  return axios.create(base);
};
