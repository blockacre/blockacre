import { IncomingHttpHeaders } from "http";
import { Handler } from "aws-lambda";
import { AxiosResponse } from "axios";
import { Mutation, Query, QueryInput } from "../api.generated";

export type { AxiosRequestConfig } from "axios";

type MutationNames = keyof Omit<Mutation, "__typename">;
type QueryNames = keyof Omit<Query, "__typename">;
export type ContractNames = MutationNames | "query";

type CognitoJWTClaims = Readonly<{
  sub: string;
  "cognito:groups": [string];
  email_verified: boolean;
  algorithm: string;
  iss: string;
  phone_number_verified: boolean;
  "cognito:username": string;
  "cognito:roles": [string];
  aud: string;
  event_id: string;
  token_use: "id" | "access";
  phone_number: string;
  exp: number;
  email: string;
  auth_time: number;
  iat: number;
}>;

type CognitoIdentity = Readonly<{
  sub: string;
  issuer: string;
  "cognito:username": string;
  sourceIp: string;
  claims: CognitoJWTClaims;
  defaultAuthStrategy: string;
}>;

export enum TypeNames {
  mutation = "Mutation",
  query = "Query",
}

type GraphQLResolverQueryEvent = {
  typeName: TypeNames.query;
  fieldName: QueryNames;
  arguments: {
    input: QueryInput;
  };
  identity: CognitoIdentity;
  source: any;
  request: {
    headers: IncomingHttpHeaders;
  };
  prev: any;
};
type GraphQLResolverMutationEvent = {
  typeName: TypeNames.mutation;
  fieldName: MutationNames;
  arguments: {
    input: any;
  };
  identity: CognitoIdentity;
  source: unknown;
  request: {
    headers: IncomingHttpHeaders;
  };
  prev: unknown;
};

type GraphQLResolverEvent = GraphQLResolverQueryEvent | GraphQLResolverMutationEvent;

export type GraphQLResolverHandler = Handler<GraphQLResolverEvent, AxiosResponse>;
