/* eslint-disable no-param-reassign */
import middy from "@middy/core";
// import HttpStatus from "http-status-codes";
// import { ErrorResponse } from "../types";

const testIdentities = {
  guest: {
    claims: {
      "cognito:groups": ["guest"],
      sub: "guest",
    },
  },
  vendor: {
    claims: {
      "cognito:groups": ["vendor"],
      sub: "vendor1",
    },
  },
};

export const identity = () => ({
  before: (handler: middy.HandlerLambda, next: middy.NextFunction) => {
    if (!handler.event.identity) {
      // TODO: maybe reinstate this code depending on how IAM auth works. For now, use this with just the API Key auth
      // const errResponse: ErrorResponse = {
      //   detail: "Identity object not available on Lambda event",
      //   source: "blockgraph",
      //   status: HttpStatus.UNAUTHORIZED,
      //   title: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED),
      // };
      // handler.response = { error: errResponse };
      // throw new Error(errResponse.detail);
      handler.event.identity = testIdentities.vendor;
    }
    return next();
  },
});
