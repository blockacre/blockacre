import middy from "@middy/core";
import { HTTPMethod } from "http-method-enum";
import { AxiosRequestConfig, client, defaultMw, GraphQLResolverHandler, logger } from "../lib";

const corewalletcreate: GraphQLResolverHandler = async (event) => {
  const data = {
    userGroups: event.identity.claims["cognito:groups"],
    userID: event.identity.claims.sub,
  };
  const request: AxiosRequestConfig = {
    data,
    method: HTTPMethod.POST,
    url: `walletcreate`,
  };
  const response = await client().request(request);
  return response.data.data;
};

export const handler = logger.handler(middy(corewalletcreate).use(defaultMw));
