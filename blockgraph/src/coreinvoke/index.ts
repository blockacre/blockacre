import middy from "@middy/core";
import { HTTPMethod } from "http-method-enum";
import {
  AxiosRequestConfig,
  client,
  ContractNames,
  defaultMw,
  GraphQLResolverHandler,
  logger,
  TypeNames,
} from "../lib";

const coreinvoke: GraphQLResolverHandler = async (event) => {
  let contract: ContractNames;
  const data = {
    args: event.arguments.input || {},
    execute: false,
    userGroups: event.identity.claims["cognito:groups"],
    userID: event.identity.claims.sub,
  };

  switch (event.typeName) {
    case TypeNames.mutation:
      // Set the execute flag if this is a mutation
      data.execute = true;
      contract = event.fieldName;
      break;

    case TypeNames.query:
      contract = "query";
      data.execute = false;
      break;

    default:
      throw new Error("Unknown event type, must be 'mutation' or 'query'");
  }

  const request: AxiosRequestConfig = {
    data,
    method: HTTPMethod.POST,
    url: contract, // This url should match the contract name to be invoked
  };
  const response = await client().request(request);
  return response.data.data;
};

export const handler = logger.handler(middy(coreinvoke).use(defaultMw));
