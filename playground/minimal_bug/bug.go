package minimal

type assetInterface interface {
	getAmount() int
	setAmount(int)
}

type portfolio []assetInterface

type asset struct {
	Amount int
}

func (a *asset) getAmount() int {
	return a.Amount
}
func (a *asset) setAmount(newAmount int) {
	a.Amount = newAmount
}

type wallet struct {
	Cash portfolio
}

func (p *portfolio) Add(a assetInterface) {
	if len(*p) == 0 {
		*p = append(*p, a)
		return
	}
	addAmount := a.getAmount()
	balance := (*p)[0].getAmount()
	newBalance := balance + addAmount
	(*p)[0].setAmount(newBalance)
}
