package minimal

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func newAmount(na int) *asset {
	return &asset{na}
}
func TestAdd(t *testing.T) {
	var w wallet
	expWallet := wallet{
		Cash: portfolio{
			&asset{25},
		},
	}

	w.Cash.Add(newAmount(5))
	w.Cash.Add(newAmount(5))
	w.Cash.Add(newAmount(5))
	w.Cash.Add(newAmount(5))
	w.Cash.Add(newAmount(5))

	assert.Equal(t, expWallet, w)
	return
}
