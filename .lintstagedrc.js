const path = require("path");
const { ESLint } = require("eslint");
const filterAsync = require("node-filter-async").default;
const git = require("simple-git");

const eslintCli = new ESLint();

const extToLower = (files) => {
  files.forEach((oldFileName) => {
    const oldFile = path.parse(oldFileName);
    const newFileName = path.join(oldFile.dir, oldFile.name) + oldFile.ext.toLowerCase();
    if (newFileName !== oldFileName) {
      git().mv(oldFileName, newFileName);
    }
  });
};

const removeIgnoredFiles = async (files) => {
  const filteredFiles = await filterAsync(files, async (file) => {
    const isIgnored = await eslintCli.isPathIgnored(file);
    return !isIgnored;
  });
  return filteredFiles.join(" ");
};

module.exports = {
  // 'pretty-quick --staged' is run direct from the Husky file

  // Ensure all file extensions are in lower case so globbing works correctly
  "*": (files) => {
    extToLower(files);
    // Need to return an array here to prevent errors
    return [];
  },

  // Run Go linting and tests
  "**/*.go": () => ["make lint", "make test"],

  // Minify images
  "*.{png,jpeg,jpg,gif}": ["imagemin-lint-staged"],

  // Optimise svgs
  "*.svg": ["svgo --config=.svgo-config.yml"],

  // Lint files and run tests
  "**/*.{ts,tsx,js,jsx}": async (files) => {
    const filesToLint = await removeIgnoredFiles(files);
    return [`eslint --max-warnings=0 ${filesToLint}`, "yarn test"];
  },
};
