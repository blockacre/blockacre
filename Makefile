#!make
include .env
export

# ********* Go, JS & Minifab installation steps  *********

install: gitsm deps gen ven js mfab

gitsm:
	@echo "Downloading git submodules..."
	@git submodule init
	@git submodule update

deps:
	@echo "Installing Go build dependencies..."
	@GO111MODULE=off go get github.com/dmarkham/enumer

gen:
	@echo "Generating Go enums code..."
	@cd blockcore && go generate ./...

ven:
	@echo "Vendoring Go dependencies..."
	@cd blockgate && go mod vendor
	@cd blockcore && go mod vendor

js:
	@echo "Installing Node Modules"
	@yarn install

mfab:
	@curl -o /usr/local/bin/minifab -sL https://tinyurl.com/yxa2q6yr && chmod +x /usr/local/bin/minifab
	@docker pull hfrd/minifab:latest


# ********* BlockCore *********

start_core: down_core install
	@echo "Creating Minifab network, installing and instantiating BlockCore..."
	@mkdir -p vars/chaincode/${APP_CHAINCODE_ID}
	@cp -R blockcore vars/chaincode/${APP_CHAINCODE_ID}/go
	@minifab up -s couchdb -c ${APP_CHANNEL_NAME} -n ${APP_CHAINCODE_ID} -v 1.0

down_core: confirm
	@echo "Stopping and cleaning up BlockCore & BlockGate..."
	@rm -rf blockgate/vendor blockcore/vendor blockcore/*/*_generated.go
	@cd blockgate && docker-compose down --rmi local -v
	@minifab cleanup
	@rm -rf vars


# ********* BlockGate *********

start_gate:
	@echo "Starting BlockGate..."
	@cd blockgate && go mod vendor
	@cd blockgate && docker-compose up -d

start_gate_cli:
	@cd blockgate && docker-compose -p blockacre exec blockgate sh


# ********* BlockGraph *********

start_graph:
	@echo "Starting BlockGraph..."
	@osascript -e 'tell app "Terminal" to do script "cd ${PWD} && amplify mock api"'
	@osascript -e 'tell app "Terminal" to do script "cd ${PWD} && yarn start:api"'


# ********* BlockUI *********

start_ui:
	@echo "Starting BlockUI..."
	@osascript -e 'tell app "Terminal" to do script "cd ${PWD} && yarn start:ui"'


# ********* All  *********

start_all: start_gate start_graph start_ui


# ********* Go tests and linting  *********

test:
	@cd blockcore && ./scripts/test.sh
	@cd blockgate && ./scripts/test.sh

lint:
	@echo "Linting blockcore..."
	@cd blockcore && golangci-lint run
	@echo "Linting blockgate..."
	@cd blockgate && golangci-lint run


# ********* Utils  *********

confirm:
	@echo "This will tear down all the environments and destroy the existing ledger. Are you sure? [y/N] " && read ans && [ $${ans:-N} = y ]

