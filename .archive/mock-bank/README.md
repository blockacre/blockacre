# Mock Bank API

## Running locally

#### Requirements

- golang 1.12
- make

Run `make` from the `mock-bank` directory to build and run the API.

## Endpoints

### Create transaction `POST /transactions`

#### Example request

```json
{
  "amount": 10,
  "currency": "GBP",
  "timestamp": "2018-07-19T12:25:10Z",
  "reference": "abc"
}
```

#### Example response

```json
{
  "id": "dfa4e17f-37bf-4eef-8212-ec4706b0f073",
  "amount": 10,
  "currency": "GBP",
  "timestamp": "2018-07-19T12:25:10Z",
  "reference": "abc"
}
```

### Retrieve transaction `GET /transactions/{id}`

#### Example response

```json
{
  "id": "dfa4e17f-37bf-4eef-8212-ec4706b0f073",
  "amount": 10,
  "currency": "GBP",
  "timestamp": "2018-07-19T12:25:10Z",
  "reference": "abc"
}
```
