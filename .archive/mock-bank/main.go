package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/rogpeppe/fastuuid"
	"github.com/syndtr/goleveldb/leveldb"
)

var uuid = fastuuid.MustNewGenerator()

var db *leveldb.DB

type transaction struct {
	ID        string    `json:"id"`
	Amount    int64     `json:"amount"`
	Currency  string    `json:"currency"`
	Created   time.Time `json:"timestamp"`
	Reference string    `json:"reference"`
}

func createTransaction(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	decoder := json.NewDecoder(r.Body)
	var t transaction
	err := decoder.Decode(&t)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	t.ID = uuid.Hex128()
	body, err := json.Marshal(t)
	if err != nil {
		log.Fatal(err)
	}
	err = db.Put([]byte(t.ID), body, nil)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(body))
}

func getTransactionByID(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	data, err := db.Get([]byte(ps.ByName("id")), nil)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(data))
}

func main() {
	var err error
	db, err = leveldb.OpenFile(".db", nil)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	router := httprouter.New()
	router.POST("/transactions", createTransaction)
	router.GET("/transactions/:id", getTransactionByID)

	log.Println("API running on http://localhost:8080")

	log.Fatal(http.ListenAndServe(":8080", router))
}
