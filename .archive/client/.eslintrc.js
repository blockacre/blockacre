module.exports = {
  extends: ["airbnb-base", "plugin:prettier/recommended"],
  env: {
    node: true,
    jest: true,
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module",
  },
  rules: {
    // Allow dev dependencies
    "import/no-extraneous-dependencies": ["error", { devDependencies: true }],
  },
};
