// eslint-disable-next-line import/prefer-default-export
export default {
  name: "test",
  minimum: 1,
  price: {
    amount: 5000,
    currency: "GBP",
  },
  type: "Open",
};
