import Joi from "@hapi/joi";

const hexID = Joi.string().hex().length(64);

const base32 = Joi.string()
  .regex(/^[A-Z2-7]+$/, "base32")
  .custom((value, helpers) => {
    if (value.length % 2 !== 0) {
      return helpers.error("any.invalid");
    }
    return value;
  }, "length is multiple of 2");

const chainId = Joi.extend((joi) => {
  return {
    type: "id",
    base: joi.string(),
    messages: {
      "id.invalid": '"{{#label}}" must be {hex} or {hex}-{base32}',
    },
    validate(schema, value, helpers) {
      if (value.includes("-")) {
        // id has base32 suffix
        try {
          const [id, suffix] = value.split("-", 1);
          joi.assert(id, hexID.required());
          joi.assert(suffix, base32);
        } catch (error) {
          return helpers.error("id.invalid");
        }
      } else {
        // id is simple base64 with no suffix
        try {
          joi.assert(value, hexID.required());
        } catch (error) {
          return helpers.error("id.invalid");
        }
      }
      return undefined;
    },
  };
});

const amount = Joi.object().keys({
  amount: Joi.number().required(),
  currency: Joi.string().alphanum().length(3),
});

const status = Joi.string().valid("Created", "Succeeded", "Failed");

const wallet = Joi.object().keys({
  ref: Joi.string(),
  cash: Joi.object(),
  cashAvailable: Joi.object(),
  assets: Joi.object(),
});

export const fund = Joi.object().keys({
  id: chainId.required(),
  name: Joi.string().alphanum(),
  minimum: Joi.number().integer(),
  price: amount.required(),
  available: Joi.number().integer(),
  sold: Joi.number().integer(),
  wallet,
  owner: Joi.string().base64(),
  type: Joi.string().valid("Open", "Limited").required(),
  docType: Joi.valid("fund").required(),
});

export const order = Joi.object().keys({
  id: chainId.required(),
  owner: Joi.string().base64(),
  FundId: hexID.required(),
  amount: amount.required(),
  fullAmount: amount.required(),
  type: Joi.string().valid("Purchase", "Sale").required(),
  status: status.required(),
  created: Joi.string().isoDate().required(),
  updated: Joi.string().isoDate().required(),
  docType: Joi.valid("order").required(),
});

export const transaction = Joi.object().keys({
  id: chainId.required(),
  to: Joi.string().base64(),
  from: Joi.string().base64(),
  creditAmount: amount,
  debitAmount: amount,
  fees: amount,
  timestamp: Joi.string().isoDate().required(),
  product: Joi.object().keys({
    id: hexID.required(),
    units: Joi.number(),
  }),
  type: Joi.string().valid("Topup", "Withdrawal", "Sale", "Yield").required(),
  status: status.required(),
  docType: Joi.valid("transaction").required(),
});

export const user = Joi.object().keys({
  id: Joi.string().base64().min(64).required(),
  wallet: wallet.required(),
});

export const stats = Joi.object().keys({
  funds: Joi.number(),
  pendingordertotal: Joi.object().keys({
    purchase: Joi.number(),
    sale: Joi.number(),
  }),
  users: Joi.number(),
});
