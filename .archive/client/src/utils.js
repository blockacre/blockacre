export const flatten = (ob) => {
  const result = {};
  let flatObject;
  Object.keys(ob).forEach((i) => {
    if (typeof ob[i] === "object") {
      flatObject = flatten(ob[i]);
      Object.keys(flatObject).forEach((x) => {
        result[`${i}[${x}]`] = flatObject[x];
      });
    } else {
      result[i] = ob[i];
    }
  });

  return result;
};

export const toQueryString = (ob) => {
  return Object.entries(flatten(ob))
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join("&");
};

export const auth = (userID, password) =>
  `Basic ${Buffer.from(`${userID}:${password}`).toString("base64")}`;
