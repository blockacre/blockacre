import Joi from "@hapi/joi";
import { fund as fundSchema } from "../__fixtures__/schema";
import fundFixture from "../__fixtures__/fund";
import Client from "../client";

jest.setTimeout(10000);

const consumer1 = new Client({ userID: "consumer1", password: "password" });
const vendor = new Client({ userID: "vendor1", password: "password" });

describe("funds", () => {
  let fundID;
  describe("POST /funds", () => {
    it("returns a valid fund object", async () => {
      const result = await vendor.createFund(fundFixture);
      fundID = result.id;
      Joi.assert(result, fundSchema.required());
    });

    it("fails if not a vendor", async () => {
      expect.assertions(1);
      try {
        await consumer1.createFund(fundFixture);
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });

    it("fails if not signed in", async () => {
      expect.assertions(1);
      try {
        await new Client().createFund(fundFixture);
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });
  });

  describe("GET /funds", () => {
    it("returns valid fund objects", async () => {
      const schema = Joi.array().items(fundSchema).min(1);
      const result = await vendor.queryFunds();
      Joi.assert(result, schema);
    });

    it("fails if not signed in", async () => {
      expect.assertions(1);
      try {
        await new Client().queryFunds();
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });
  });

  describe("GET /funds/{id}", () => {
    it("returns a valid fund object", async () => {
      const result = await vendor.getFund(fundID);
      Joi.assert(result, fundSchema.required());
    });

    it("fails if not signed in", async () => {
      expect.assertions(1);
      try {
        await new Client().getFund(fundID);
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });
  });
});
