import Joi from "@hapi/joi";

import { stats } from "../__fixtures__/schema";
import Client from "../client";

jest.setTimeout(30000);

const consumer1 = new Client({ userID: "consumer1", password: "password" });

describe("stats", () => {
  describe("GET /stats", () => {
    it("gets stats", async () => {
      const res = await consumer1.stats();
      Joi.assert(res, stats.required());
    });

    it("fails if not signed in", async () => {
      expect.assertions(1);
      try {
        await new Client().stats();
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });
  });
});
