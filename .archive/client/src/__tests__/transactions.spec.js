import Joi from "@hapi/joi";
import { transaction } from "../__fixtures__/schema";
import Client from "../client";

jest.setTimeout(10000);

const consumer1 = new Client({ userID: "consumer1", password: "password" });
const admin = new Client({ userID: "admin1", password: "password" });

describe("transactions", () => {
  let topupTransactionID;
  // let withdrawTransactionID;

  describe("POST /transactions/topup", () => {
    const amount = 1e5;
    let res;
    let oldBalance;

    beforeAll(async () => {
      const profile = await consumer1.profile();
      oldBalance = profile.wallet.cash.GBP || 0;
      const { ref } = profile.wallet;
      res = await admin.topup(ref, amount, "GBP");
      topupTransactionID = res.id;
    });

    it("returns valid transaction object", () => {
      Joi.assert(res, transaction.required());
    });

    it("increases balance", async () => {
      const profile = await consumer1.profile();
      const newBalance = profile.wallet.cash.GBP;
      expect(newBalance).toBe(oldBalance + amount);
    });
  });

  describe("POST /transactions/withdraw", () => {
    const amount = 1e5;
    let res;
    let oldBalance;

    beforeAll(async () => {
      const profile = await consumer1.profile();
      oldBalance = profile.wallet.cash.GBP || 0;
      res = await consumer1.withdraw(amount, "GBP");
      // withdrawTransactionID = res.id;
    });

    it("returns a valid transaction object", () => {
      Joi.assert(res, transaction.required());
    });

    it("decreases user's balance", async () => {
      const profile = await consumer1.profile();
      const newBalance = profile.wallet.cash.GBP;
      expect(newBalance).toBe(oldBalance - amount);
    });
  });

  describe("GET /transactions", () => {
    it("returns an array of valid transaction objects", async () => {
      const res = await consumer1.queryTransactions();
      Joi.assert(res, Joi.array().items(transaction).min(1));
    });
  });

  describe("GET /transactions/{id}", () => {
    it("returns a valid transaction object", async () => {
      const result = await consumer1.getTransaction(topupTransactionID);
      Joi.assert(result, transaction.required());
    });
  });
});
