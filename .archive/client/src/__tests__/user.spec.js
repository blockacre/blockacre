import Joi from "@hapi/joi";
import crypto from "crypto";

import { user } from "../__fixtures__/schema";
import Client from "../client";

jest.setTimeout(30000);

const consumer1 = new Client({ userID: "consumer1", password: "password" });

describe("users", () => {
  describe("POST /usercreate", () => {
    const client = new Client();
    it("creates users", async () => {
      const userID = `consumer-${crypto.randomBytes(20).toString("hex")}`;
      const res = await client.usercreate(userID, "password");
      Joi.assert(res, user.required());
    });

    it("fails on existing userID", async () => {
      expect.assertions(1);
      try {
        await client.usercreate("consumer1", "password");
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });

    it("fails on empty userID", async () => {
      expect.assertions(1);
      try {
        await client.usercreate("", "password");
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });

    it("fails on empty password", async () => {
      expect.assertions(1);
      try {
        await client.usercreate("userID", "");
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });
  });

  describe("GET /user", () => {
    it("returns valid user object", async () => {
      const result = await consumer1.user();
      Joi.assert(result, user.required());
    });

    it("fails if not signed in", async () => {
      expect.assertions(1);
      try {
        await new Client().user();
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });
  });
});
