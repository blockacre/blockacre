import Joi from "@hapi/joi";
import { order } from "../__fixtures__/schema";
import fundFixture from "../__fixtures__/fund";
import Client from "../client";

jest.setTimeout(60000);

const consumer1 = new Client({ userID: "consumer1", password: "password" });
const consumer2 = new Client({ userID: "consumer2", password: "password" });
const vendor = new Client({ userID: "vendor1", password: "password" });
const admin = new Client({ userID: "admin1", password: "password" });
const fee = 0.01;

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

describe("orders", () => {
  let fundID;
  let purchaseOrderID;

  describe("POST /orders/purchase", () => {
    // enough to buy directly from fund (incl. fees)
    const amount = Number(
      ((fundFixture.price.amount * fundFixture.minimum) / (1 - fee)).toFixed(2)
    );
    let res;
    let cashAvailable;
    let assets;

    beforeAll(async () => {
      fundID = (await vendor.createFund(fundFixture)).id;
      await admin.topup((await consumer1.profile()).wallet.ref, amount, "GBP");
      const profile = await consumer1.profile();
      cashAvailable = profile.wallet.cashAvailable.GBP || 0 + amount;
      assets = Number(profile.wallet.assets[fundID] || 0);

      res = await consumer1.purchase(fundID, amount, "GBP");
      purchaseOrderID = res.id;
    });

    it("returns valid order object", () => {
      Joi.assert(res, order);
    });

    it("decreases available cash", async () => {
      const profile = await consumer1.profile();
      const newCashAvailable = profile.wallet.cashAvailable.GBP;
      expect(newCashAvailable).toBe(cashAvailable - amount);
    });

    it("purchases from fund", async () => {
      const query = { filter: { product: { id: fundID } } };
      while (!(await consumer1.queryTransactions(query)).length) {
        await sleep(2000);
      }
      const profile = await consumer1.profile();
      const newAssets = Number(profile.wallet.assets[fundID]);
      expect(newAssets).toBe(assets + fundFixture.minimum * 1e9);
    });

    it("fails on insufficient balance", async () => {
      expect.assertions(1);
      try {
        await consumer1.purchase(fundID, cashAvailable * 10, "GBP");
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });

    it("fails if not signed in", async () => {
      expect.assertions(1);
      try {
        await new Client().purchase(fundID, cashAvailable * 10, "GBP");
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });
  });

  describe("POST /orders/sell", () => {
    const units = 0.5;
    // const value = fundFixture.price.amount * units;
    const value = Number((fundFixture.price.amount * units).toFixed(2));
    const valueWithFee = Number((value * (1 + fee)).toFixed(2));
    let res;
    let buyerCash;
    let buyerAssets;
    let sellerCash;
    let sellerAssets;

    beforeAll(async () => {
      const seller = await consumer1.profile();
      sellerCash = seller.wallet.cash.GBP || 0;
      sellerAssets = Number(seller.wallet.assets[fundID] || 0);

      const buyer = await consumer2.profile();
      buyerCash = buyer.wallet.cash.GBP || 0;
      buyerAssets = Number(buyer.wallet.assets[fundID] || 0);

      await admin.topup(buyer.wallet.ref, valueWithFee, "GBP");

      res = await consumer1.sell(fundID, value, "GBP");
    });

    it("returns valid order object", () => {
      Joi.assert(res, order.required());
    });

    it("is matched with incoming purchase orders and assets are transferred", async () => {
      await consumer2.purchase(fundID, valueWithFee, "GBP");

      // wait for orders to be matched
      const query = { filter: { product: { id: fundID } } };
      let tx;
      while (!tx) {
        await sleep(2000);
        tx = (await consumer2.queryTransactions(query)).pop();
      }

      const seller = await consumer1.profile();
      const newSellerCash = Number(seller.wallet.cash.GBP);
      const newSellerAssets = Number(seller.wallet.assets[fundID]);

      expect(newSellerCash).toBe(sellerCash + tx.creditAmount.amount);
      expect(newSellerAssets).toBe(sellerAssets - tx.product.units);

      const buyer = await consumer2.profile();
      const newBuyerCash = Number(buyer.wallet.cash.GBP);
      const newBuyerAssets = Number(buyer.wallet.assets[fundID]);

      expect(newBuyerCash).toBe(buyerCash);
      expect(newBuyerAssets).toBe(buyerAssets + tx.product.units);
    });
  });

  describe("GET /orders", () => {
    it("returns an array of valid order objects", async () => {
      const res = await consumer1.queryOrders();
      Joi.assert(res, Joi.array().items(order).min(1));
    });

    it("fails if not signed in", async () => {
      expect.assertions(1);
      try {
        await new Client().queryOrders();
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });
  });

  describe("GET /orders/{id}", () => {
    it("returns a valid order object", async () => {
      const res = await consumer1.getOrder(purchaseOrderID);
      Joi.assert(res, order);
    });

    it("fails if not signed in", async () => {
      expect.assertions(1);
      try {
        await new Client().getOrder(purchaseOrderID);
      } catch (e) {
        expect(e).toMatchSnapshot();
      }
    });
  });
});
