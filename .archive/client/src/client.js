/* eslint-disable no-underscore-dangle, max-classes-per-file */
import fetch from "node-fetch";
import HTTPError from "./error";
import { toQueryString, auth } from "./utils";

export default class Client {
  constructor(options = {}) {
    this.baseUrl = options.baseUrl || "http://localhost:8080";
    this.options = {
      credentials: "include",
      mode: "cors",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",
      },
    };

    if (options.userID && options.password) {
      this.options.headers.Authorization = auth(options.userID, options.password);
    }
  }

  // request is a wrapper around fetch to throw errors on HTTP error codes and parse json
  async _request(method, uri, data) {
    const url = new URL(`/hlfab${uri}`, this.baseUrl);
    const options = { ...this.options, method };

    if (data) {
      if (method === "GET") {
        url.search = toQueryString(data);
      } else {
        options.body = JSON.stringify(data);
      }
    }

    const response = await fetch(url, options);

    if (response.status >= 200 && response.status < 300) {
      if (response.status === 204 || response.status === 205) {
        return null;
      }
      return (await response.json()).data;
    }

    throw new HTTPError(response.status, (await response.text()).trim());
  }

  stats() {
    return this._request("GET", "/stats", null, this.options);
  }

  createFund(fund) {
    return this._request("POST", "/funds", fund, this.options);
  }

  queryFunds(query) {
    return this._request("GET", "/funds", query, this.options);
  }

  getFund(id) {
    return this._request("GET", `/funds/${id}`, null, this.options);
  }

  distributeYield(id, amount, currency) {
    return this._request("GET", `/funds/${id}`, { amount, currency }, this.options);
  }

  purchase(id, amount, currency) {
    const body = { id, amount: { amount, currency } };
    return this._request("POST", `/orders/purchase`, body, this.options);
  }

  sell(id, amount, currency) {
    const body = { id, amount: { amount, currency } };
    return this._request("POST", `/orders/sell`, body, this.options);
  }

  queryOrders(query) {
    return this._request("GET", "/orders", query, this.options);
  }

  getOrder(id) {
    return this._request("GET", `/orders/${id}`, null, this.options);
  }

  topup(ref, amount, currency) {
    const body = { ref, amount: { amount, currency } };
    return this._request("POST", `/transactions/topup`, body, this.options);
  }

  withdraw(amount, currency) {
    return this._request("POST", `/transactions/withdraw`, { amount, currency }, this.options);
  }

  queryTransactions(query) {
    return this._request("GET", "/transactions", query, this.options);
  }

  getTransaction(id) {
    return this._request("GET", `/transactions/${id}`, null, this.options);
  }

  usercreate(userID, password) {
    return this._request("POST", "/usercreate", { userID, password }, this.options);
  }

  user() {
    return this._request("GET", `/user`, null, this.options);
  }
}

// (async () => {
//   const client = new Client({ userID: 'consumer1', password: 'password' });
//   await client.queryTransactions({ id: { a: 1 } });
// })();
