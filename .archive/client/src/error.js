const errors = {
  400: "BadRequest",
  401: "Unauthorized",
  402: "PaymentRequired",
  403: "Forbidden",
  404: "NotFound",
  409: "Conflict",
  500: "InternalServer",
};

class APIError extends Error {
  constructor(code, message) {
    super(message);
    this.code = code;
    this.name = `${errors[code]}Error`;
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = new Error().stack;
    }
  }
}

export default APIError;
