module.exports = {
  git: {
    commitMessage: "chore: release v${version}",
    tagAnnotation: "v${version}",
    tagName: "v${version}",
  },
  gitlab: {
    release: process.env.ci,
  },
  npm: {
    publish: false,
  },
  plugins: {
    "@release-it/conventional-changelog": {
      preset: "angular",
      infile: "CHANGELOG.md",
    },
  },
};
