| Item           | BlockCore                                |
| -------------- | ---------------------------------------- |
| Product Create | Done                                     |
| Product Update | Done                                     |
| Product Issue  | Done                                     |
| Product Redeem | Not started                              |
| Product Yield  | Incomplete                               |
| Deposit        | Done, except bank integration            |
| Withdraw       | Done, except bank integration            |
| User Create    | Done                                     |
| User Read      | Done                                     |
| User Update    | Done                                     |
| User Delete    |                                          |
| Buy            | Done                                     |
| Sell           | Done, except for double sale - see below |
| P2P Match      | Needs integration testing                |
| Order Read     |                                          |
| Order List     |                                          |

# TODO

- Input validation
- Fee Matrix & Calculations
- Deposit: Take bank's tx ID as arg and get tx details from bank's api (input.ref==userID)
  - Create tx as `Active` and mark as `Fulfilled` upon bank's hook
- Withdraw: banking integration
- Fine grained access control - define actions that users can and can't do
  - seems an Admin user can deposit money to themselves which is probably not right
  - Which users can withdraw and deposit
  - Which records users can query - for themselves and others
- Add some basic validation to the transfer function - e.g. if there is an asset but no payment info, the transfer should probably fail. Also check required fields
- Consider the order of how sell / buy orders should be processed when issuing / redeemeing units - should it be by time or by value then time
- Test using couchdb query for getting wallet by ref (include a getstate on the wallet ID to prevent phantom writes)
- Add create / update methods for ledger ops that check for presence or lack of existing record on ledger
- Ensure user can't put parts up for sale twice
- Update typings in lambda functions
- Update chaincode to return structured error
- Use CouchDB / External DB for stats and dashboarding types

# Tests to Add

- Ensure user can't transfer the same asset to themselves
- Orders at multiple prices are bought in correct order

# Not Doing

- Use Parts as a type and add some custom methods (isZero, add etc.)
  - Because would result in many type conversions
- Make txid more clear (use GetKeyAttrs method for any ID and rename ID to txid)
  - Because would make JS code more complicated - would have to duplicate methods?
- Consider marshalling ids to their respective objects without having to do a look up
  - May result in uncessary reads
