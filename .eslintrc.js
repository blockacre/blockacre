// Used for scripts etc in the root folder
module.exports = {
  env: {
    // Already included by `airbnb-typescript/base`
    // node: true,
  },
  extends: [
    // `airbnb` gives stylistic opinions
    "airbnb-base",
    // Recommended prettier / eslint config, see: https://prettier.io/docs/en/integrating-with-linters.html#recommended-configuration
    // Resolves conflicting prettier/@typescript-eslint formatting rules
    "prettier",
  ],
  plugins: ["sort-keys-fix"],
  root: true,
  rules: {
    // Require braces in arrow function body as needed
    "arrow-body-style": ["error", "as-needed"],

    // Allow dev dependencies
    "import/no-extraneous-dependencies": ["error", { devDependencies: true }],

    // Enforce import order and no spaces
    "import/order": ["error", { "newlines-between": "never" }],

    // Prefer arrow functions in callbacks
    "prefer-arrow-callback": "error",

    // Sort imports
    "sort-imports": [
      "error",
      {
        ignoreCase: true,
        ignoreDeclarationSort: true,
        ignoreMemberSort: false,
      },
    ],

    // Sort keys in objects etc
    "sort-keys-fix/sort-keys-fix": ["error", "asc", { caseSensitive: false, natural: true }],
  },
};
