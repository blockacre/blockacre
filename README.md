# Pre-Requisites

For Go files

- Go: `brew install go` or see: https://golang.org/doc/install
- golangci-lint CI: `brew install golangci/tap/golangci-lint && brew upgrade golangci/tap/golangci-lint` install as binaries only, or see: https://golangci-lint.run/usage/install/

- When running on Docker for Mac, you need to disable `Use gRPC FUSE for file sharing` in preferences > experimental features
