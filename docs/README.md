# Installation

## Prerequisites

- Make
- Go > 1.13
- Docker
- Docker Compose
- MiniFab: https://github.com/litong01/minifabric

# Usage

All commands are run from the project root

# BlockCore (Go)

The core Hyperledger fabric application. For development purposes, we use the Minifab network utility: https://github.com/litong01/minifabric for running the chaincode.

1. Run `make install` to install build dependencies, generate some Go code then download and vendor modules.
1. Run `make up` build the chaincode network and install and instantiate blockcore on the built network

# BlockGate (Go)

A micro service providing a REST API to BlockCore. Built with Go Kit: https://github.com/go-kit/kit. Acts as a very light interface in to the chaincode, almost like a proxy, handling the network connection profile and interactions with the Fabric CA server. Contains minimal business logic.

1. Run `make serve` to build and run the BlockGate API
1. Run `make serve_cli` to login in to the BlockGate docker container. From there, to see API code changes quit the API with `CTRL+C` (this will not tear down
   the environment) and then run `go run -mod=vendor chain/cmd/main.go` from the Docker container.

# Other

Run `make down` to tear down the current environment, delete the docker containers, volumes & images and remove all temporary files.

## URLs

| App                   | URL                             |
| --------------------- | ------------------------------- |
| Blockacre API         | <http://localhost:8081>         |
| CouchDB Web Interface | <http://localhost:5984/_utils/> |
| Blockchain Explorer   | n/a                             |

# Testing

## Integration Tests

Run the application using the command `make` from the project root and wait for it to be fully up and running.

In a separate console, run `yarn install`, then `yarn test` from the `./tests` directory.

## Manual Testing

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/134a03f4f08040823093)

# Minifab

## Invoke Chaincode Examples

minifab invoke -n blockcore -p '"get", "{\"objectType\": \"walletIndex\", \"id\": \"BA1A27R\"}"'
minifab invoke -n blockcore -p '"partial", "{\"objectType\": \"wallet\"}"'
