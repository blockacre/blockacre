/* eslint-disable */

const workspaces = require("./package.json").workspaces.packages;

const removeWorkspaceFiles = (files) =>
  files.filter((file) => {
    const pathFolders = path.dirname(file).split(path.sep);
    return !pathFolders.some((r) => workspaces.includes(r));
  });

const removeIgnoredFiles = (files) => files.filter((file) => !cli.isPathIgnored(file)).join(" ");

const allTsJsGlob = "**/*.{ts,tsx,js,jsx}";
const workspaceTsJsGlob = `{${workspaces.join(",")}}/${allTsJsGlob}`;

// Run workspace tasks, if workspace files have changed
// [workspaceTsJsGlob]: (files) => [
//   `yarn commit:eslint`, // Run the workspace linting on all files,
//   `yarn commit:test ${files}`, // Then run the related workspace tests on staged files
// ],
