// eslint-disable-next-line camelcase
const { npm_package_version } = process.env;

module.exports = {
  appVersion: npm_package_version,
};
