/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type ProductCreateInput = {
  name: string;
  type: ProductType;
  currency: Currency;
  minimum: number;
  unitPrice: number;
  partsPerUnit: number;
};

export enum ProductType {
  open = "open",
  closed = "closed",
}

export enum Currency {
  GBP = "GBP",
  USD = "USD",
}

export enum docType {
  order = "order",
  orderBook = "orderBook",
  product = "product",
  transaction = "transaction",
  transfer = "transfer",
  wallet = "wallet",
  walletCounter = "walletCounter",
  walletIndex = "walletIndex",
}

export enum Status {
  active = "active",
  fulfilled = "fulfilled",
  failed = "failed",
}

export type ProductUpdateInput = {
  id: string;
  unitPrice: number;
};

export type OrderInput = {
  parts: number;
  productID: string;
  side: OrderSide;
};

export enum OrderSide {
  buy = "buy",
  sell = "sell",
}

export type OrderIssueInput = {
  productID: string;
};

export type DepositInput = {
  amount: number;
  currency: Currency;
  walletRef: string;
};

export enum TransactionType {
  deposit = "deposit",
  withdrawal = "withdrawal",
}

export type WithdrawInput = {
  amount: number;
  currency: Currency;
};

export type QueryInput = {
  selector: Selector;
  limit?: number | null;
  skip?: number | null;
  sort?: Sort | null;
  fields?: Array<string | null> | null;
};

export type Selector = {
  docType: docType;
  id?: string | null;
};

export type Sort = {
  unknown: string;
};

export type ProductcreateMutationVariables = {
  input?: ProductCreateInput | null;
};

export type ProductcreateMutation = {
  productcreate: {
    __typename: "ProductResponse";
    data: {
      __typename: "Product";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      name: string;
      type: ProductType;
      currency: Currency;
      minimum: number;
      unitPrice: number;
      partsPerUnit: number;
      partPrice: number;
      issued: number;
    } | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type ProductupdateMutationVariables = {
  input?: ProductUpdateInput | null;
};

export type ProductupdateMutation = {
  productupdate: {
    __typename: "ProductResponse";
    data: {
      __typename: "Product";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      name: string;
      type: ProductType;
      currency: Currency;
      minimum: number;
      unitPrice: number;
      partsPerUnit: number;
      partPrice: number;
      issued: number;
    } | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type OrdercreateMutationVariables = {
  input?: OrderInput | null;
};

export type OrdercreateMutation = {
  ordercreate: {
    __typename: "OrderResponse";
    data: {
      __typename: "Order";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      parts: number;
      productID: string;
      side: OrderSide;
      vendor: boolean;
      limit: {
        __typename: "Money";
        amount: number;
        currency: Currency;
      };
      remaining: number;
    } | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type OrderissuecreateMutationVariables = {
  input?: OrderIssueInput | null;
};

export type OrderissuecreateMutation = {
  orderissuecreate: {
    __typename: "OrderResponse";
    data: {
      __typename: "Order";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      parts: number;
      productID: string;
      side: OrderSide;
      vendor: boolean;
      limit: {
        __typename: "Money";
        amount: number;
        currency: Currency;
      };
      remaining: number;
    } | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type WalletcreateMutation = {
  walletcreate: {
    __typename: "WalletResponse";
    data: {
      __typename: "Wallet";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      product: boolean | null;
      ref: string;
      cash: Array<{
        __typename: "Money";
        amount: number;
        currency: Currency;
      }> | null;
      cashAvailable: Array<{
        __typename: "Money";
        amount: number;
        currency: Currency;
      }> | null;
      portfolio: Array<{
        __typename: "Asset";
        productID: string;
        parts: number;
      }> | null;
    } | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type DepositMutationVariables = {
  input?: DepositInput | null;
};

export type DepositMutation = {
  deposit: {
    __typename: "TransactionResponse";
    data: {
      __typename: "Transaction";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      amount: number;
      currency: Currency;
      from: string;
      to: string;
      type: TransactionType;
      walletRef: string;
    } | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type WithdrawMutationVariables = {
  input?: WithdrawInput | null;
};

export type WithdrawMutation = {
  withdraw: {
    __typename: "TransactionResponse";
    data: {
      __typename: "Transaction";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      amount: number;
      currency: Currency;
      from: string;
      to: string;
      type: TransactionType;
      walletRef: string;
    } | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type OrdersQueryVariables = {
  input: QueryInput;
};

export type OrdersQuery = {
  orders: {
    __typename: "OrdersResponse";
    data: Array<{
      __typename: "Order";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      parts: number;
      productID: string;
      side: OrderSide;
      vendor: boolean;
      limit: {
        __typename: "Money";
        amount: number;
        currency: Currency;
      };
      remaining: number;
    } | null> | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type ProductsQueryVariables = {
  input: QueryInput;
};

export type ProductsQuery = {
  products: {
    __typename: "ProductsResponse";
    data: Array<{
      __typename: "Product";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      name: string;
      type: ProductType;
      currency: Currency;
      minimum: number;
      unitPrice: number;
      partsPerUnit: number;
      partPrice: number;
      issued: number;
    } | null> | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type TransactionsQueryVariables = {
  input: QueryInput;
};

export type TransactionsQuery = {
  transactions: {
    __typename: "TransactionsResponse";
    data: Array<{
      __typename: "Transaction";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      amount: number;
      currency: Currency;
      from: string;
      to: string;
      type: TransactionType;
      walletRef: string;
    } | null> | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type TransfersQueryVariables = {
  input: QueryInput;
};

export type TransfersQuery = {
  transfers: {
    __typename: "TransfersResponse";
    data: Array<{
      __typename: "Transfer";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      sequence: number;
      vendor: boolean;
      matchedOrder: string;
      asset: {
        __typename: "AssetTransfer";
        productID: string;
        parts: number;
        partPrice: number;
        from: string;
        to: string;
      };
      payment: {
        __typename: "MoneyTransfer";
        amount: number;
        currency: Currency;
        from: string;
        to: string;
      };
      fee: {
        __typename: "MoneyTransfer";
        amount: number;
        currency: Currency;
        from: string;
        to: string;
      };
      totalValue: {
        __typename: "Money";
        amount: number;
        currency: Currency;
      };
    } | null> | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type UserQuery = {
  user: {
    __typename: "WalletResponse";
    data: {
      __typename: "Wallet";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      product: boolean | null;
      ref: string;
      cash: Array<{
        __typename: "Money";
        amount: number;
        currency: Currency;
      }> | null;
      cashAvailable: Array<{
        __typename: "Money";
        amount: number;
        currency: Currency;
      }> | null;
      portfolio: Array<{
        __typename: "Asset";
        productID: string;
        parts: number;
      }> | null;
    } | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};

export type WalletsQueryVariables = {
  input: QueryInput;
};

export type WalletsQuery = {
  wallets: {
    __typename: "WalletsResponse";
    data: Array<{
      __typename: "Wallet";
      id: string;
      createdBy: string;
      created: string;
      docType: docType;
      modified: string | null;
      status: Status;
      product: boolean | null;
      ref: string;
      cash: Array<{
        __typename: "Money";
        amount: number;
        currency: Currency;
      }> | null;
      cashAvailable: Array<{
        __typename: "Money";
        amount: number;
        currency: Currency;
      }> | null;
      portfolio: Array<{
        __typename: "Asset";
        productID: string;
        parts: number;
      }> | null;
    } | null> | null;
    error: {
      __typename: "Error";
      code: string | null;
      detail: string | null;
      source: string;
      status: number;
      title: string;
    } | null;
  } | null;
};
