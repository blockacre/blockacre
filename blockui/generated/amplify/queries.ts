/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const orders = /* GraphQL */ `
  query Orders($input: QueryInput!) {
    orders(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        parts
        productID
        side
        vendor
        limit {
          amount
          currency
        }
        remaining
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const products = /* GraphQL */ `
  query Products($input: QueryInput!) {
    products(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        name
        type
        currency
        minimum
        unitPrice
        partsPerUnit
        partPrice
        issued
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const transactions = /* GraphQL */ `
  query Transactions($input: QueryInput!) {
    transactions(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        amount
        currency
        from
        to
        type
        walletRef
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const transfers = /* GraphQL */ `
  query Transfers($input: QueryInput!) {
    transfers(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        sequence
        vendor
        matchedOrder
        asset {
          productID
          parts
          partPrice
          from
          to
        }
        payment {
          amount
          currency
          from
          to
        }
        fee {
          amount
          currency
          from
          to
        }
        totalValue {
          amount
          currency
        }
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const user = /* GraphQL */ `
  query User {
    user {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        product
        ref
        cash {
          amount
          currency
        }
        cashAvailable {
          amount
          currency
        }
        portfolio {
          productID
          parts
        }
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const wallets = /* GraphQL */ `
  query Wallets($input: QueryInput!) {
    wallets(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        product
        ref
        cash {
          amount
          currency
        }
        cashAvailable {
          amount
          currency
        }
        portfolio {
          productID
          parts
        }
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
