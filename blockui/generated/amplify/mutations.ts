/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const productcreate = /* GraphQL */ `
  mutation Productcreate($input: ProductCreateInput) {
    productcreate(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        name
        type
        currency
        minimum
        unitPrice
        partsPerUnit
        partPrice
        issued
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const productupdate = /* GraphQL */ `
  mutation Productupdate($input: ProductUpdateInput) {
    productupdate(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        name
        type
        currency
        minimum
        unitPrice
        partsPerUnit
        partPrice
        issued
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const ordercreate = /* GraphQL */ `
  mutation Ordercreate($input: OrderInput) {
    ordercreate(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        parts
        productID
        side
        vendor
        limit {
          amount
          currency
        }
        remaining
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const orderissuecreate = /* GraphQL */ `
  mutation Orderissuecreate($input: OrderIssueInput) {
    orderissuecreate(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        parts
        productID
        side
        vendor
        limit {
          amount
          currency
        }
        remaining
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const walletcreate = /* GraphQL */ `
  mutation Walletcreate {
    walletcreate {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        product
        ref
        cash {
          amount
          currency
        }
        cashAvailable {
          amount
          currency
        }
        portfolio {
          productID
          parts
        }
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const deposit = /* GraphQL */ `
  mutation Deposit($input: DepositInput) {
    deposit(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        amount
        currency
        from
        to
        type
        walletRef
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
export const withdraw = /* GraphQL */ `
  mutation Withdraw($input: WithdrawInput) {
    withdraw(input: $input) {
      data {
        id
        createdBy
        created
        docType
        modified
        status
        amount
        currency
        from
        to
        type
        walletRef
      }
      error {
        code
        detail
        source
        status
        title
      }
    }
  }
`;
