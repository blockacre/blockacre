/**
 * Type Naming:
 *
 * General Type: prefix with T, e.g. TCONFIG
 * Params Type: prefix with PT, e.g. PTPushDialog
 * Props: prefix with 'P', e.g. PErrorPage
 * Return Type: prefix with 'RT' e.g. RTFunctionName
 * Router Params (useParams): prefix with UP e.g. UPProductDetail
 * State: prefix with S e.g. SDialog
 * Context Type: prefix with CT, e.g. CTDialogPush
 * Location State: prefix with LS, e.g. LSOrderCreate
 */

type TCONFIG = {
  appVersion: string;
};

declare const CONFIG: TCONFIG;

// eslint-disable-next-line @typescript-eslint/ban-types
type PWithC<P = {}> = P & {
  children: React.ReactNode;
};
