/* eslint-disable @typescript-eslint/consistent-type-definitions */
import { addMethod, number, setLocale } from "yup";

declare module "yup" {
  interface NumberSchema {
    skipType(): NumberSchema;
  }
}

export function initValidation(): void {
  addMethod(number, "skipType", function skipType() {
    return this.transform((cv: unknown, ov: unknown) =>
      typeof ov === "string" && ov.trim() === "" ? undefined : cv
    );
  });
  setLocale({
    mixed: {
      notType: ({ path, type }): string => `${path} must be a ${type}.`,
    },
  });
}
