import { OrderFrequency, OrderInput, ProductCreateInput } from "./api.generated";

type FormField<T> = {
  [P in keyof T]: T[P] | "";
};

export type ProductCreateFormFields = FormField<ProductCreateInput>;
export type ProductCreateFormFieldNames = keyof ProductCreateFormFields;

// These are the actaul form fields
export type OrderCreateFormFields = Omit<OrderInput, "productID" | "side"> & {
  orderFrequency: OrderFrequency;
};
export type OrderCreateFormFieldNames = keyof OrderCreateFormFields;
// These are the values that the flow submits
export type OrderCreateSubmitValues = OrderCreateFormFields & Pick<OrderInput, "side">;

export type LSOrderCreate = OrderCreateFormFields & {
  confirmed?: boolean;
  totalAsNumber: number;
  totalAsString: string;
};

export type AllFormFields = ProductCreateFormFields & OrderCreateFormFields;
export type AnyFormFieldName = ProductCreateFormFieldNames | OrderCreateFormFieldNames;

export type CardVariants = "list" | "summary" | "detail";
