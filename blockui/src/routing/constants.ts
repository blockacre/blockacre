import { OrderSide } from "../api.generated";

export const routePrefix = {
  admin: "/admin",
  investor: "/investor",
  orderSidePath: `:orderSide(${OrderSide.buy}|${OrderSide.sell})`,
  products: "/products",
  vendor: "/vendor",
};
