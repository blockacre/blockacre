import React from "react";
import { ApolloProvider } from "@apollo/client";
import { Amplify } from "@aws-amplify/core";
import { Global } from "@emotion/react";
import { CssBaseline, ThemeProvider } from "@material-ui/core";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { apolloClient } from "./apolloClient";
import { App } from "./App";
import awsconfig from "./aws-exports";
import * as serviceWorker from "./serviceWorker";
import { coreTheme } from "./theme/coreTheme";

Amplify.configure(awsconfig);

ReactDOM.render(
  <ApolloProvider client={apolloClient}>
    <ThemeProvider theme={coreTheme}>
      <BrowserRouter>
        <React.StrictMode>
          <CssBaseline />
          <Global styles={coreTheme.cssMixins.global} />
          <App />
        </React.StrictMode>
      </BrowserRouter>
    </ThemeProvider>
  </ApolloProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
