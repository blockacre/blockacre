import React from "react";
import { Helmet } from "react-helmet";
import { AuthRoute } from "./auth";
import { AppContainer } from "./components/layout";
import { NavBar, Switch404 } from "./components/navigation";
import { OrdersRouter } from "./modules/orders";
import { ProductRouter } from "./modules/products";
import { TransactionsRouter } from "./modules/transactions";
import { TransfersRouter } from "./modules/transfers";
import { routePrefix } from "./routing";

export function App(): React.ReactElement {
  return (
    <AppContainer>
      <Helmet defaultTitle="Welcome" titleTemplate="%s | Blockacre" />
      <NavBar />
      <Switch404>
        <AuthRoute exact path="/">
          Home
        </AuthRoute>
        <AuthRoute path={`${routePrefix.products}`}>
          <ProductRouter />
        </AuthRoute>
        <AuthRoute path={`${routePrefix.investor}/portfolio`}>Portfolio</AuthRoute>
        <AuthRoute path={`${routePrefix.investor}/orders`}>
          <OrdersRouter />
        </AuthRoute>
        <AuthRoute path={`${routePrefix.investor}/transfers`}>
          <TransfersRouter />
        </AuthRoute>
        <AuthRoute path={`${routePrefix.investor}/transactions`}>
          <TransactionsRouter />
        </AuthRoute>
        <AuthRoute path={`${routePrefix.investor}/settings`}>Settings</AuthRoute>
      </Switch404>
    </AppContainer>
  );
}
