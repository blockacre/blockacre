import React, { useState } from "react";
import { css, useTheme } from "@emotion/react";
import { FormControl, FormLabel, Slider, Typography } from "@material-ui/core";
import { Controller, UseFormMethods } from "react-hook-form";
import { AnyFormFieldName } from "../../types";

type PAmountSlider = {
  control: UseFormMethods["control"];
  label: string;
  name: AnyFormFieldName;
  total: string;
};

export function AmountSlider({ control, label, name, total }: PAmountSlider): React.ReactElement {
  const [focused, setFocused] = useState(false);
  const min = 1;
  const max = 20;

  const theme = useTheme();
  const styles = {
    price: css`
      margin-left: auto;
    `,
  };

  return (
    <Controller
      control={control}
      name={name}
      render={({ onBlur, onChange, value }): React.ReactElement => {
        function handleFocus(): void {
          setFocused(true);
        }
        function handleBlur(): void {
          onBlur();
          setFocused(false);
        }

        return (
          <FormControl fullWidth>
            <FormLabel css={theme.cssMixins.spaceSimpleV(2)} focused={focused} id="amount-slider">
              {label}
            </FormLabel>
            <Slider
              marks
              aria-labelledby="amount-slider"
              css={theme.cssMixins.spaceSimpleV(2)}
              max={max}
              min={min}
              value={value as number}
              valueLabelDisplay="auto"
              onBlur={handleBlur}
              onChange={(_, newValue): void => {
                onChange(newValue ? Number(newValue) : min);
              }}
              onFocus={handleFocus}
            />
            <Typography css={[theme.cssMixins.spaceSimpleV(2), styles.price]}>
              Total: <strong>{total}</strong>
            </Typography>
          </FormControl>
        );
      }}
    />
  );
}
