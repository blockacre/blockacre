import React from "react";
import { TextField, TextFieldProps } from "@material-ui/core";
import { FieldErrors, UseFormMethods } from "react-hook-form";
import { AllFormFields, AnyFormFieldName } from "../../types";

type FilteredTextFieldProps = Omit<
  TextFieldProps,
  "error" | "fullWidth" | "helperText" | "id" | "inputRef"
>;

type PInputField = FilteredTextFieldProps & {
  errors: FieldErrors<AllFormFields>;
  name: AnyFormFieldName;
  register: UseFormMethods["register"];
};

export function InputField({ errors, name, register, ...rest }: PInputField): React.ReactElement {
  const errorMsg = errors[name]?.message;
  return (
    <TextField
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
      error={!!errorMsg}
      helperText={errorMsg || " "}
      id={name}
      inputRef={register}
      name={name}
    />
  );
}
