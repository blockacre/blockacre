import React from "react";
import { css, useTheme } from "@emotion/react";
import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from "@material-ui/core";
import { UseFormMethods } from "react-hook-form";
import { OrderFrequency } from "../../api.generated";
import { AnyFormFieldName } from "../../types";

type POrderFrequencyRadio = {
  control: UseFormMethods["control"];
  defaultValue: OrderFrequency;
  label: string;
  name: AnyFormFieldName;
};

export function OrderFrequencyRadio({
  control,
  defaultValue,
  label,
  name,
}: POrderFrequencyRadio): React.ReactElement {
  const theme = useTheme();
  const styles = {
    legend: css`
      margin-bottom: ${theme.spacing(2)};
    `,
  };
  return (
    <FormControl fullWidth component="fieldset">
      <FormLabel component="legend" css={styles.legend}>
        {label}
      </FormLabel>
      <RadioGroup aria-label={label} defaultValue={defaultValue} name={name}>
        <FormControlLabel
          disabled
          control={<Radio color="primary" />}
          inputRef={control.register}
          label="Monthly Recurring (coming soon)"
          value={OrderFrequency.monthly}
        />
        <FormControlLabel
          control={<Radio color="primary" />}
          inputRef={control.register}
          label="Single"
          value={OrderFrequency.single}
        />
      </RadioGroup>
    </FormControl>
  );
}
