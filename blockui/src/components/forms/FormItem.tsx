import React from "react";
import { Grid } from "@material-ui/core";

type PFormItem = {
  fullWidth?: boolean;
};

export function FormItem({ children, fullWidth }: PWithC<PFormItem>): React.ReactElement {
  return (
    <Grid item sm={fullWidth ? undefined : 6} xs={12}>
      {children}
    </Grid>
  );
}
