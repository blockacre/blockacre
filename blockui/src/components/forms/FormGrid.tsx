import React from "react";
import { Grid } from "@material-ui/core";

export function FormGrid({ children }: PWithC): React.ReactElement {
  return (
    <Grid container spacing={2}>
      {children}
    </Grid>
  );
}
