export type TDLItem = {
  detail: React.ReactNode;
  title: string;
};
