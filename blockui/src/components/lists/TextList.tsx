import React from "react";
import { css, useTheme } from "@emotion/react";
import { List, ListProps } from "@material-ui/core";

type PTextList = ListProps & {
  paragraph: boolean;
};

export function TextList({ paragraph, ...props }: PTextList): React.ReactElement {
  const theme = useTheme();
  const styles = {
    list: css`
      /* Use a little more margin on the right to balance it properly */
      margin-right: ${theme.spacing(6)};
      ${paragraph && { marginBottom: theme.spacing(2) }};
      margin-left: ${theme.spacing(4)};
    `,
  };

  // eslint-disable-next-line react/jsx-props-no-spreading
  return <List css={styles.list} {...props} />;
}
