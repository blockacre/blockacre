import React from "react";
import { css, useTheme } from "@emotion/react";
import { Typography } from "@material-ui/core";
import { TDLItem } from "./types";

export function TextListItem({ detail, title }: TDLItem): React.ReactElement | null {
  const theme = useTheme();
  const styles = {
    li: css`
      ${theme.cssMixins.spaceSimpleV(1)};
      background-color: ${theme.palette.background.default};
      padding: ${theme.spacing(1, 2)};
    `,
  };
  return (
    <li css={styles.li}>
      <Typography color="primary" variant="overline">
        {title}
      </Typography>
      <Typography>{detail}</Typography>
    </li>
  );
}
