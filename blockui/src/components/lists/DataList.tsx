import React from "react";
import { css } from "@emotion/react";
import { Chip, Grid, Typography } from "@material-ui/core";
import { isEmpty } from "../../lib";
import { TDLItem } from "./types";

type PDataList = {
  dataList: TDLItem[];
};

export function DataList({ dataList }: PDataList): React.ReactElement | null {
  if (isEmpty(dataList)) {
    return null;
  }
  const styles = {
    both: css`
      text-transform: capitalize;
    `,
  };
  return (
    <Grid container component="dl" spacing={1}>
      {dataList.map((item) => (
        <React.Fragment key={item.title}>
          <Grid item component="dt" css={styles.both} md={2} sm={3} xs={6}>
            <Typography variant="body2">{item.title}:</Typography>
          </Grid>
          <Grid item component="dd" md={2} sm={3} xs={6}>
            <Chip css={styles.both} label={item.detail} size="small" />
          </Grid>
        </React.Fragment>
      ))}
    </Grid>
  );
}
