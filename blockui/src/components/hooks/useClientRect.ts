import { useCallback, useState } from "react";

type SUseClientRect = null | DOMRect;
type RTUseClientRect = [rect: SUseClientRect, ref: (node: HTMLElement) => void];

export function useClientRect(): RTUseClientRect {
  const [rect, setRect] = useState<SUseClientRect>(null);
  const ref = useCallback((node: HTMLElement) => {
    if (node !== null) {
      setRect(node.getBoundingClientRect());
    }
  }, []);
  return [rect, ref];
}
