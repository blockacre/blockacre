import React, { useEffect } from "react";
import { Dialog, DialogProps } from "@material-ui/core";
import { useHistory } from "react-router";

type SDialog = {
  Content: React.ReactNode;
  closeAll?: boolean;
  maxWidth?: DialogProps["maxWidth"];
  nodeId: string;
  open: boolean;
  persistRouting?: boolean;
};

type PTPushDialog = Omit<SDialog, "open">;

/**
 * In this Push context, we just provide the pushDialog function with pushes a dialog on to the array.
 * This helps prevent unnecessary re-renders
 */
export type CTDialogPush = (params: PTPushDialog) => void;
export const DialogPushContext = React.createContext<undefined | CTDialogPush>(undefined);

/**
 * Note that we always need to create a new array when updating the state, otherwise no re-renders!
 */
export function DialogProvider({ children }: PWithC): React.ReactElement {
  const [dialogs, setDialogs] = React.useState<SDialog[]>([]);

  /**
   * Add new dialog to the dialogs array and open immediately
   */
  function pushDialog({
    closeAll = false,
    Content,
    maxWidth = "md",
    nodeId,
    persistRouting = false,
  }: PTPushDialog): void {
    setDialogs(
      dialogs.concat([
        {
          closeAll,
          Content,
          maxWidth,
          nodeId,
          open: true,
          persistRouting,
        },
      ])
    );
  }

  /**
   * Create a new dialogs array with open set to false on the target dialog.
   * Don't remove the dialog from the array here b/c we want the tranistions to complete.
   */
  function closeDialog(targetIndex: number): void {
    const targetDialog = dialogs[targetIndex];
    const newDialogs = dialogs.map((dialog, index) => {
      /**
       * If the target dialog has requested to closeAll, set open to false in all the dialogs
       */
      if (targetDialog.closeAll) {
        return { ...dialog, open: false };
      }
      /**
       * Otherwise, return the dialog unchanged, except the target
       */
      if (index !== targetIndex) {
        return dialog;
      }
      return { ...targetDialog, open: false };
    });
    setDialogs(newDialogs);
  }

  /**
   * Called when the transitions have finished, we can safely remove from the array
   */
  function removeDialog(targetIndex: number): void {
    /**
     * If dialog has request to closeAll, remove all the dialogs
     */
    const targetDialog = dialogs[targetIndex];
    if (targetDialog.closeAll) {
      setDialogs([]);
    }
    setDialogs(dialogs.filter((_, index) => index !== targetIndex));
  }

  /**
   * Close all Dialogs on route changes, unless they have requested to be persisted
   */
  const history = useHistory();
  useEffect(() => {
    const unlisten = history.listen(() => {
      setDialogs((ds) => ds.map((d) => ({ ...d, open: d.persistRouting ? d.open : false })));
    });

    return (): void => unlisten();
  }, [history]);

  return (
    <DialogPushContext.Provider value={pushDialog}>
      {children}
      {dialogs.map((dialog, index) => (
        <Dialog
          key={dialog.nodeId}
          fullWidth
          maxWidth={dialog.maxWidth}
          open={dialog.open}
          TransitionProps={{
            onExited: (): void => removeDialog(index),
          }}
          onClose={(): void => closeDialog(index)}
        >
          {dialog.Content}
        </Dialog>
      ))}
    </DialogPushContext.Provider>
  );
}
