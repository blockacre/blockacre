import React from "react";
import { CTDialogPush, DialogPushContext } from "./DialogProvider";

/**
 * useDialog is really just a shortcut to the DialogContext
 * and provides a limit set of functions to suit most use cases.
 */
export function useDialog(): CTDialogPush {
  const context = React.useContext(DialogPushContext);
  if (context === undefined) {
    throw new Error("useDialog must be used within a DialogProvider");
  }
  return context;
}
