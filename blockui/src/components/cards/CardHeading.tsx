import React from "react";
import { Typography } from "@material-ui/core";

export function CardHeading({ children }: PWithC): React.ReactElement {
  return (
    <Typography component="h2" variant="h3">
      {children}
    </Typography>
  );
}
