import React, { AriaAttributes } from "react";

type PTabPanel = {
  ariaLabelledby: AriaAttributes["aria-labelledby"];
  id: string;
  index: string;
  value: string;
};

export function TabPanel(props: PWithC<PTabPanel>): React.ReactElement {
  const { ariaLabelledby, children, id, index, value } = props;

  return (
    <div aria-labelledby={ariaLabelledby} hidden={value !== index} id={id} role="tabpanel">
      {value === index && children}
    </div>
  );
}
