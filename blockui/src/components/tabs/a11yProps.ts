import React from "react";

type RTA11yProps = {
  tab: {
    "aria-controls": React.AriaAttributes["aria-controls"];
    id: string;
  };
  tabPanel: {
    ariaLabelledby: React.AriaAttributes["aria-labelledby"];
    id: string;
  };
};

export function a11yProps(name: string, index: string): RTA11yProps {
  return {
    tab: {
      "aria-controls": `${name}-tabpanel-${index}`,
      id: `${name}-tab-${index}`,
    },
    tabPanel: {
      ariaLabelledby: `${name}-tab-${index}`,
      id: `${name}-tabpanel-${index}`,
    },
  };
}
