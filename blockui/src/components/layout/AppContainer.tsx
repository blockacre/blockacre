import React from "react";
import { css, useTheme } from "@emotion/react";
import { AppHeader } from "./AppHeader";

export function AppContainer({ children }: PWithC): React.ReactElement {
  const theme = useTheme();
  const styles = {
    inner: css`
      ${theme.cssMixins.containerFullHeightInner};
      padding-top: ${theme.cssMixins.appHeaderHeight};
      background-color: ${theme.palette.background.paper};
    `,
    outer: theme.cssMixins.containerFullHeightOuter,
  };

  return (
    <div css={styles.outer}>
      <div css={styles.inner}>
        <AppHeader />
        {children}
      </div>
    </div>
  );
}
