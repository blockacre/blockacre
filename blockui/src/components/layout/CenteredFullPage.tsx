import React from "react";
import { useTheme } from "@emotion/react";
import { Container } from "@material-ui/core";

export function CenteredFullPage({ children }: PWithC): React.ReactElement {
  const theme = useTheme();
  return (
    <Container
      css={[theme.cssMixins.colCentered, theme.cssMixins.containerFullHeightOuter]}
      maxWidth="md"
    >
      {children}
    </Container>
  );
}
