import React from "react";
import { css, useTheme } from "@emotion/react";
import { Container } from "@material-ui/core";

export function SceneFooter({ children }: PWithC): React.ReactElement {
  const theme = useTheme();
  const styles = {
    container: css`
      padding-top: ${theme.spacing(2)};
      padding-bottom: ${theme.spacing(2)};
    `,
    footer: css`
      flex-shrink: 0;
      border-top: 1px solid ${theme.palette.divider};
    `,
  };

  return (
    <footer css={styles.footer}>
      <Container css={styles.container} maxWidth="xs">
        {children}
      </Container>
    </footer>
  );
}

/**
 * The following code would allow to use a fixed footer in case of issues in layouts later
 */
// export function SceneFooter({ children }: PWithC): React.ReactElement {
//   const theme = useTheme();
//   const [footerRect, footerRef] = useClientRect();
//   const styles = {
//     footer: css`
//       ${theme.cssMixins.stretchBottom};
//       ${theme.cssMixins.sceneSpacingH};
//       position: fixed;
//     `,
//     placeholder: css`
//       height: ${theme.spacing(4.5)};
//       height: ${footerRect?.height}px;
//       flex-shrink: 0;
//     `,
//   };
//   return (
//     <React.Fragment>
//       <footer ref={footerRef} css={styles.footer}>
//         {children}
//       </footer>
//       <div aria-hidden="true" css={styles.placeholder} />
//     </React.Fragment>
//   );
// }
