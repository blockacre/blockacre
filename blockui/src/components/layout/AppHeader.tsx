import React from "react";
import { css, useTheme } from "@emotion/react";
import { Container, IconButton, SvgIcon } from "@material-ui/core";
import { Link } from "react-router-dom";
import { ReactComponent as BrandLogo } from "../../images/logo.svg";

export function AppHeader(): React.ReactElement {
  const theme = useTheme();
  const styles = {
    header: css`
      ${theme.cssMixins.rowVCentered};
      ${theme.cssMixins.stretchTop};
      position: fixed;
      z-index: ${theme.zIndex.appBar};
      justify-content: space-between;
    `,
  };

  return (
    <header css={styles.header}>
      <Container maxWidth="xl">
        <IconButton color="primary" component={Link} edge="start" to="/">
          <SvgIcon component={BrandLogo} viewBox="0 0 554 785" />
        </IconButton>
      </Container>
    </header>
  );
}
