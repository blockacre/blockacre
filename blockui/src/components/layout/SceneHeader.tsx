import React from "react";
import { css, useTheme } from "@emotion/react";
import { Container } from "@material-ui/core";

export function SceneHeader({ children }: PWithC): React.ReactElement {
  const theme = useTheme();
  const styles = {
    container: css`
      padding-top: ${theme.spacing(3)};
      padding-bottom: ${theme.spacing(3)};
    `,
    header: css`
      border-bottom: 1px solid ${theme.palette.divider};
      background-color: ${theme.palette.background.paper};
      position: relative;
      flex-shrink: 0;
    `,
  };
  return (
    <header css={styles.header}>
      <Container css={styles.container}>{children}</Container>
    </header>
  );
}
