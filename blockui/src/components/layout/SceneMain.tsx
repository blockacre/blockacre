import React from "react";
import { css, useTheme } from "@emotion/react";
import { Container, ContainerProps } from "@material-ui/core";

type PSceneMain = Pick<ContainerProps, "maxWidth">;

export function SceneMain({ children, maxWidth }: PWithC<PSceneMain>): React.ReactElement {
  const theme = useTheme();
  const styles = {
    container: css`
      padding-top: ${theme.spacing(4)};
      padding-bottom: ${theme.spacing(4)};
    `,
    main: css`
      ${theme.cssMixins.containerFullHeightInner};
      background-color: ${theme.palette.background.default};
      flex-grow: 1;
    `,
  };
  return (
    <main css={styles.main}>
      <Container css={styles.container} maxWidth={maxWidth}>
        {children}
      </Container>
    </main>
  );
}
