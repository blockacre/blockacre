import React from "react";
import { css, useTheme } from "@emotion/react";

export function SceneWrap({ children }: PWithC): React.ReactElement {
  const theme = useTheme();
  const styles = css`
    ${theme.cssMixins.containerFullHeightOuter};
    ${theme.cssMixins.colStretch};
  `;
  return <section css={styles}>{children}</section>;
}
