import * as React from "react";
import { css } from "@emotion/react";
import { BottomNavigation, BottomNavigationAction } from "@material-ui/core";
import AccountBalanceWalletRoundedIcon from "@material-ui/icons/AccountBalanceWalletRounded";
import LibraryAddRoundedIcon from "@material-ui/icons/LibraryAddRounded";
import MenuRoundedIcon from "@material-ui/icons/MenuRounded";
import SwapHorizRoundedIcon from "@material-ui/icons/SwapHorizRounded";
import { NavLink, Route, Switch } from "react-router-dom";
import { routePrefix } from "../../routing";

export function NavBar(): React.ReactElement {
  const [value, setValue] = React.useState(0);

  const styles = {
    navBar: css`
      position: fixed;
      bottom: 0;
      left: 0;
      right: 0;
    `,
  };
  return (
    <Switch>
      <Route exact={false} path={routePrefix.vendor}>
        <BottomNavigation
          showLabels
          component="nav"
          css={styles.navBar}
          value={value}
          onChange={(_, newValue: number): void => {
            setValue(newValue);
          }}
        >
          <BottomNavigationAction
            component={NavLink}
            icon={<AccountBalanceWalletRoundedIcon />}
            label="Products"
            to={`${routePrefix.vendor}/portfolio`}
          />
          <BottomNavigationAction
            component={NavLink}
            icon={<LibraryAddRoundedIcon />}
            label="New Product"
            to={`${routePrefix.vendor}/invest`}
          />
          <BottomNavigationAction
            component={NavLink}
            icon={<MenuRoundedIcon />}
            label="More"
            to={`${routePrefix.vendor}/settings`}
          />
        </BottomNavigation>
      </Route>
      <Route exact={false} path={routePrefix.investor}>
        <BottomNavigation
          showLabels
          component="nav"
          css={styles.navBar}
          value={value}
          onChange={(_, newValue: number): void => {
            setValue(newValue);
          }}
        >
          <BottomNavigationAction
            component={NavLink}
            icon={<AccountBalanceWalletRoundedIcon />}
            label="Porfolio"
            to={`${routePrefix.investor}/portfolio`}
          />
          <BottomNavigationAction
            component={NavLink}
            icon={<LibraryAddRoundedIcon />}
            label="Invest"
            to={`${routePrefix.investor}/invest`}
          />
          <BottomNavigationAction
            component={NavLink}
            icon={<SwapHorizRoundedIcon />}
            label="Transfer"
            to={`${routePrefix.investor}/transfer`}
          />
          <BottomNavigationAction
            component={NavLink}
            icon={<MenuRoundedIcon />}
            label="More"
            to={`${routePrefix.investor}/settings`}
          />
        </BottomNavigation>
      </Route>
    </Switch>
  );
}
