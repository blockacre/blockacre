import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { NotFoundPage } from "../../modules";

export function Switch404({
  children,
  redirect = false,
}: PWithC<{ redirect?: boolean }>): React.ReactElement {
  return (
    <Switch>
      {children}
      {redirect && <Redirect to="/404" />}
      <Route path="*">
        <NotFoundPage />
      </Route>
    </Switch>
  );
}
