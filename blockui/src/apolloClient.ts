import {
  ApolloClient,
  createHttpLink,
  FieldFunctionOptions,
  FieldReadFunction,
  InMemoryCache,
  TypePolicies,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { ProductsResponse, QueryInput } from "./api.generated";
import generatedFragments from "./apiFragments.generated";
import awsconfig from "./aws-exports";
import { capitalize } from "./lib";

// For GraphQL Auth, see: https://www.apollographql.com/docs/react/networking/authentication/
const httpLink = createHttpLink({
  uri: awsconfig.aws_appsync_graphqlEndpoint,
});

type TPrevContext = {
  [key: string]: unknown;
  headers: Headers;
};

const authLink = setContext((_, previousContext: TPrevContext) => ({
  headers: {
    ...previousContext.headers,
    "x-api-key": awsconfig.aws_appsync_apiKey,
  },
}));

/**
 * This function queries cached query results by id to avoid network round trips
 */
function queryByIdPolicy(
  existingData: ProductsResponse,
  { args, canRead, toReference }: FieldFunctionOptions
): ReturnType<FieldReadFunction> {
  if (existingData) {
    return existingData;
  }

  const input = args?.input as QueryInput;
  const { docType, id } = input.selector;
  if (!id) {
    return undefined;
  }

  const reference = toReference({ __typename: capitalize(docType), id });
  if (!canRead(reference)) {
    return undefined;
  }

  const cachedObject = {
    data: [reference],
    error: null,
  };
  return cachedObject;
}

const typePolicies: TypePolicies = {
  Query: {
    fields: {
      products: queryByIdPolicy,
    },
  },
};

export const apolloClient = new ApolloClient({
  cache: new InMemoryCache({
    possibleTypes: generatedFragments.possibleTypes,
    typePolicies,
  }),
  link: authLink.concat(httpLink),
});
