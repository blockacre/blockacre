/* This is a generated file, do not edit it directly */
import { gql } from "@apollo/client";
import * as Types from "../api.generated";

export type RecordFieldsTransferFragment = {
  __typename?: "Transfer";
  created: string;
  createdBy: string;
  docType: Types.DocType;
  id: string;
  modified?: Types.Maybe<string>;
  status: Types.Status;
};

export type RecordFieldsTransactionFragment = {
  __typename?: "Transaction";
  created: string;
  createdBy: string;
  docType: Types.DocType;
  id: string;
  modified?: Types.Maybe<string>;
  status: Types.Status;
};

export type RecordFieldsOrderFragment = {
  __typename?: "Order";
  created: string;
  createdBy: string;
  docType: Types.DocType;
  id: string;
  modified?: Types.Maybe<string>;
  status: Types.Status;
};

export type RecordFieldsProductFragment = {
  __typename?: "Product";
  created: string;
  createdBy: string;
  docType: Types.DocType;
  id: string;
  modified?: Types.Maybe<string>;
  status: Types.Status;
};

export type RecordFieldsWalletFragment = {
  __typename?: "Wallet";
  created: string;
  createdBy: string;
  docType: Types.DocType;
  id: string;
  modified?: Types.Maybe<string>;
  status: Types.Status;
};

export type RecordFieldsFragment =
  | RecordFieldsTransferFragment
  | RecordFieldsTransactionFragment
  | RecordFieldsOrderFragment
  | RecordFieldsProductFragment
  | RecordFieldsWalletFragment;

export type ErrorFieldsFragment = {
  __typename?: "Error";
  code?: Types.Maybe<string>;
  detail?: Types.Maybe<string>;
  source: string;
  status: number;
  title: string;
};

export const RecordFieldsFragmentDoc = /* #__PURE__ */ gql`
  fragment recordFields on Record {
    id
    createdBy
    created
    docType
    modified
    status
  }
`;
export const ErrorFieldsFragmentDoc = /* #__PURE__ */ gql`
  fragment errorFields on Error {
    code
    detail
    source
    status
    title
  }
`;
