import { DocType, QueryInput } from "../api.generated";

export function createSimpleQuery(docType: DocType, id?: string): { input: QueryInput } {
  if (id) {
    return {
      input: {
        selector: { docType, id },
      },
    };
  }
  return {
    input: {
      selector: { docType },
    },
  };
}
