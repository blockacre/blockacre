export type PossibleTypesResultData = {
  possibleTypes: {
    [key: string]: string[];
  };
};
const result: PossibleTypesResultData = {
  possibleTypes: {
    Record: ["Transfer", "Transaction", "Order", "Product", "Wallet"],
    Response: [
      "ProductResponse",
      "ProductsResponse",
      "OrderResponse",
      "OrdersResponse",
      "TransactionResponse",
      "TransactionsResponse",
      "TransferResponse",
      "TransfersResponse",
      "WalletResponse",
      "WalletsResponse",
    ],
  },
};
export default result;
