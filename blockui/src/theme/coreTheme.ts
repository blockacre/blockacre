import { createMuiTheme } from "@material-ui/core";
import { components } from "./components";
import { coreThemeConstants } from "./coreThemeConstants";
import { cssMixins } from "./cssMixins";
import { palette } from "./palette";
import { typography } from "./typography";

export const coreTheme = createMuiTheme({
  components,
  cssMixins,
  palette,
  shape: {
    borderRadius: coreThemeConstants.borderRadius.md,
  },
  typography,
});
