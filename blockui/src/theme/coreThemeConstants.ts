export const coreThemeConstants = {
  borderRadius: {
    lg: "1rem",
    md: "0.5rem",
  },
  fontFamily: [
    "-apple-system",
    "BlinkMacSystemFont",
    '"Segoe UI"',
    "Roboto",
    '"Helvetica Neue"',
    "Arial",
    "sans-serif",
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
  ].join(","),
  letterSpacing: {
    lg: "0.05em",
    md: "0",
  },
  lineHeight: 1.5,
};
