import { ThemeOptions } from "@material-ui/core";
import { coreThemeConstants } from "./coreThemeConstants";
import { defaultMuiTheme } from "./defaultMuiTheme";

const cardSpacing = defaultMuiTheme.spacing(3, 3);

const cardStyles = {
  headers: {
    borderBottom: `1px solid ${defaultMuiTheme.palette.divider}`,
    padding: cardSpacing,
  },
};

export const components: ThemeOptions["components"] = {
  MuiAlert: {
    styleOverrides: {
      message: {
        fontWeight: 300,
      },
    },
  },
  MuiButton: {
    defaultProps: {
      disableElevation: true,
      size: "large",
    },
    styleOverrides: {
      contained: {
        fontWeight: 700,
      },
      sizeLarge: {
        fontSize: "1rem",
      },
    },
  },
  MuiButtonBase: {
    defaultProps: {
      type: "button",
    },
  },
  MuiCard: {
    defaultProps: {
      elevation: 0,
      variant: "outlined",
    },
    styleOverrides: {
      root: {
        position: "relative",
      },
    },
  },
  MuiCardActions: {
    styleOverrides: {
      root: {
        justifyContent: "flex-end",
        padding: cardSpacing,
      },
    },
  },
  MuiCardContent: {
    styleOverrides: {
      root: {
        ".MuiCardHeader-root + &": {
          padding: cardSpacing,
        },
        padding: cardSpacing,
      },
    },
  },
  MuiCardHeader: {
    defaultProps: {
      disableTypography: true,
    },
    styleOverrides: {
      root: {
        ...cardStyles.headers,
      },
    },
  },
  MuiDialogActions: {
    styleOverrides: {
      root: {
        justifyContent: "flex-end",
        padding: cardSpacing,
      },
    },
  },
  MuiDialogContent: {
    styleOverrides: {
      root: {
        padding: cardSpacing,
      },
    },
  },
  MuiDialogTitle: {
    defaultProps: {
      disableTypography: true,
    },
    styleOverrides: {
      root: {
        ...cardStyles.headers,
      },
    },
  },
  MuiPaper: {
    styleOverrides: {
      rounded: {
        borderRadius: coreThemeConstants.borderRadius.lg,
      },
    },
  },
  MuiTextField: {
    defaultProps: {
      fullWidth: true,
    },
  },
};
