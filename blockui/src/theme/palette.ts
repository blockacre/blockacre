import { ThemeOptions } from "@material-ui/core";

// Palette Colors
export const colors = {
  brandBlue: "#23314B",
  brandFade: "#EFF1F4",
  brandGreen: "#0CAB68",
  brandOrange: "#EB922F",
  error: "#E0447C",
  white: "#FFF",
};

export const palette: ThemeOptions["palette"] = {
  background: {
    default: colors.brandFade,
    paper: colors.white,
  },
  error: {
    main: colors.error,
  },
  primary: {
    contrastText: colors.white,
    main: colors.brandGreen,
  },
  secondary: {
    main: colors.brandBlue,
  },
};
