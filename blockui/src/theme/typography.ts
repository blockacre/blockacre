import { ThemeOptions } from "@material-ui/core";
import { coreThemeConstants } from "./coreThemeConstants";

export const baseBody = {
  fontWeight: 400,
  lineHeight: coreThemeConstants.lineHeight,
};

export const typography: ThemeOptions["typography"] = {
  body1: {
    ...baseBody,
  },
  body2: {
    ...baseBody,
  },
  // buttons also handled in components file
  button: {
    textTransform: "capitalize",
  },
  fontFamily: coreThemeConstants.fontFamily,
  fontWeightBold: 700,
  fontWeightLight: 300,
  fontWeightMedium: 400,
  fontWeightRegular: 400,
  h1: {
    fontSize: "2rem",
    fontWeight: 700,
  },
  h2: {
    fontSize: "2rem",
    fontWeight: 300,
  },
  h3: {
    fontSize: "1.25rem",
    fontWeight: 700,
  },
  h4: {
    fontSize: "1.25rem",
    fontWeight: 300,
  },
  h5: {
    fontSize: "1rem",
    fontWeight: 700,
  },
  h6: {
    fontSize: "1rem",
    fontWeight: 300,
  },
  overline: {
    lineHeight: coreThemeConstants.lineHeight,
  },
};
