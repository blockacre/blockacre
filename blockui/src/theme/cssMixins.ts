import { css, SerializedStyles } from "@emotion/react";
import { defaultMuiTheme } from "./defaultMuiTheme";

export const cssMixins = {
  appHeaderHeight: "48px",
  colCentered: css`
    flex-direction: column;
    align-items: center;
    display: flex;
    justify-content: center;
  `,
  colHCentered: css`
    flex-direction: column;
    display: flex;
    justify-content: center;
  `,
  colStretch: css`
    display: flex;
    flex-direction: column;
  `,
  colVCentered: css`
    flex-direction: column;
    align-items: center;
    display: flex;
  `,
  containerFullHeightInner: css`
    height: 100%;
    overflow-y: auto;
  `,
  containerFullHeightOuter: css`
    height: 100%;
  `,
  global: css`
    html,
    body,
    .root {
      height: 100%;
    }
  `,
  rowCentered: css`
    align-items: center;
    display: flex;
    justify-content: center;
  `,
  rowHCentered: css`
    display: flex;
    justify-content: center;
  `,
  rowStretch: css`
    display: flex;
  `,
  rowVCentered: css`
    align-items: center;
    display: flex;
  `,
  sceneSpacingV: css`
    padding-top: ${defaultMuiTheme.spacing(3)};
    padding-bottom: ${defaultMuiTheme.spacing(3)};
  `,
  spaceSimpleV: (spacing: number): SerializedStyles => css`
    margin-bottom: ${defaultMuiTheme.spacing(spacing)};

    &:last-child {
      margin-bottom: 0;
    }

    + & {
      margin-top: ${defaultMuiTheme.spacing(spacing)};
    }
  `,
  stretchAll: css`
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
  `,
  stretchBottom: css`
    right: 0;
    bottom: 0;
    left: 0;
  `,
  stretchTop: css`
    top: 0;
    right: 0;
    left: 0;
  `,
};
