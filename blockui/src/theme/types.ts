/* eslint-disable @typescript-eslint/consistent-type-definitions, @typescript-eslint/no-empty-interface */
import { Theme as MUITheme } from "@material-ui/core/styles";
import { cssMixins } from "./cssMixins";
import {} from "@emotion/react/types/css-prop";

declare module "@material-ui/core/styles/createMuiTheme" {
  interface Theme {
    cssMixins: typeof cssMixins;
  }
  // allow configuration using `createMuiTheme`
  interface ThemeOptions {
    cssMixins: typeof cssMixins;
  }
}

declare module "@emotion/react" {
  export interface Theme extends MUITheme {}
}
