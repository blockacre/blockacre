import React from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { ErrorPage, LoadingPage } from "../modules";
import { useAuth } from "./useAuth";

export function AuthRoute({ children, ...rest }: PWithC<RouteProps>): React.ReactElement {
  const { error, groups, isLoading } = useAuth();
  /**
   * Todo - add real auth in to this file
   */
  const validRoute = !!groups;

  if (isLoading) {
    return <LoadingPage />;
  }

  if (error) {
    return <ErrorPage error={error} />;
  }

  return (
    <Route
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
      render={({ location }): React.ReactNode =>
        validRoute ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}
