import { useEffect, useState } from "react";
import { Auth, CognitoUser } from "@aws-amplify/auth";
import { Group } from "../api.generated";

type AuthUser = {
  email: string;
  groups: Group[];
};

type AuthPayload = {
  "cognito:groups": Group[];
};

function getEmailAndGroups(cognitoUser: CognitoUser): AuthUser {
  const userSession = cognitoUser.getSignInUserSession();
  const auth: AuthUser = {
    email: "",
    groups: [],
  };
  if (userSession) {
    // Grab the user's groups
    const payload = userSession.getIdToken().payload as AuthPayload;
    auth.groups = payload["cognito:groups"];
    // And email
    auth.email = cognitoUser.getUsername();
  }
  return auth;
}

type RTUseAuth = {
  email: string;
  error: string;
  groups: Group[];
  isLoading: boolean;
};
export function useAuth(): RTUseAuth {
  const [email, setEmail] = useState("");
  const [groups, setGroups] = useState<Group[]>([]);
  // TODO: remove eslint disable
  // eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    async function runQuery(): Promise<void> {
      try {
        const cognitoUser = (await Auth.currentAuthenticatedUser()) as CognitoUser;
        const auth = getEmailAndGroups(cognitoUser);
        setEmail(auth.email);
        setGroups(auth.groups);
      } catch (err) {
        if (typeof err === "string") {
          // TODO: don't return errors yet
          // setError(err);
          // eslint-disable-next-line no-console
          console.log(err);
        } else {
          // TODO: check error types
          // eslint-disable-next-line no-console
          console.log(err);
        }
      } finally {
        setIsLoading(false);
      }
    }
    void runQuery();
  }, []);
  return { email, error, groups, isLoading };
}
