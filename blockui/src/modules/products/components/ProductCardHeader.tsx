import React from "react";
import { Chip, Grid } from "@material-ui/core";
import { Product } from "../../../api.generated";
import { CardHeading } from "../../../components/cards";
import { amountToString } from "../../../lib";

type PProductCardHeader = {
  product: Pick<Product, "name" | "partPrice" | "currency">;
};

export function ProductCardHeader({ product }: PProductCardHeader): React.ReactElement {
  return (
    <Grid container alignItems="center" justifyContent="space-between">
      <Grid item>
        <CardHeading>{product.name}</CardHeading>
      </Grid>
      <Grid item>
        <Chip
          color="secondary"
          label={
            <React.Fragment>
              from: <strong>{amountToString(product.partPrice, product.currency)}</strong>
            </React.Fragment>
          }
        />
      </Grid>
    </Grid>
  );
}
