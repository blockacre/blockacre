import React from "react";
import { Button, Card, CardActions, CardContent, CardHeader, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import { DataList } from "../../../components/lists";
import { routePrefix } from "../../../routing";
import { CardVariants } from "../../../types";
import { useProduct } from "../hooks";
import { productData } from "../lib";
import { ProductCardHeader } from "./ProductCardHeader";

type PProductCard = {
  variant?: CardVariants;
};

export function ProductCard({ variant = "summary" }: PProductCard): React.ReactElement | null {
  const { product } = useProduct();

  const displayActions = ["list"].includes(variant);
  const displayData = ["detail", "list"].includes(variant);

  if (!product) {
    return null;
  }
  return (
    <Card>
      <CardHeader title={<ProductCardHeader product={product} />} />
      <CardContent>
        <Typography paragraph>
          Description placeholder: The aim of the product is to provide an income stream that rises
          annually in line with LPI (RPI between 0% and 5%).
        </Typography>
        {displayData && <DataList dataList={productData(product)} />}
      </CardContent>
      {displayActions && (
        <CardActions>
          <Button
            component={Link}
            size="medium"
            to={`${routePrefix.products}/${product.id}`}
            variant="outlined"
          >
            See Details
          </Button>
          <Button
            component={Link}
            size="medium"
            to={`${routePrefix.products}/${product.id}`}
            variant="contained"
          >
            Invest
          </Button>
        </CardActions>
      )}
    </Card>
  );
}
