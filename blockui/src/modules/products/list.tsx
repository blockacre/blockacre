import React from "react";
import { Grid, Typography } from "@material-ui/core";
import { Helmet } from "react-helmet";
import { DocType } from "../../api.generated";
import { SceneHeader, SceneMain, SceneWrap } from "../../components/layout";
import { createSimpleQuery } from "../../graphql/queries";
import { LoadingPage } from "../utility";
import { ProductCard } from "./components";
import { useProductsQuery } from "./lib";

export function Products(): React.ReactElement {
  const { data, loading } = useProductsQuery({
    variables: createSimpleQuery(DocType.product),
  });
  return (
    <SceneWrap>
      <Helmet>
        <title>Investment Products</title>
      </Helmet>
      <SceneHeader>
        <Typography variant="h1">Available Investment Products</Typography>
        <Typography>
          Here is a list of all of our currently available investment products
        </Typography>
      </SceneHeader>
      {loading && <LoadingPage />}
      <SceneMain maxWidth="md">
        <Grid container spacing={3}>
          {data?.products?.data?.map((product) =>
            !product ? null : (
              <Grid key={product?.id} item xs={12}>
                <ProductCard variant="list" />
              </Grid>
            )
          )}
        </Grid>
      </SceneMain>
    </SceneWrap>
  );
}
