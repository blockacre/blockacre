import React from "react";
import { generatePath, useParams } from "react-router-dom";
import { OrderSide } from "../../api.generated";
import { AuthRoute } from "../../auth";
import { Switch404 } from "../../components/navigation";
import { routePrefix } from "../../routing";
import { OrderConfirmDialog } from "../orders/components/OrderConfirmDialog";
import { DepositDialog } from "../transactions/components";
import { ProductCreate } from "./create";
import { Product } from "./detail";
import { Products } from "./list";

type UPOrderConfirm = {
  orderSide: OrderSide;
  productId: string;
};

const investFlowStaticPaths = {
  step1: `${routePrefix.products}/:productId/`,
  step2: `${routePrefix.products}/:productId/${routePrefix.orderSidePath}/confirm`,
  step3: `${routePrefix.products}/:productId/deposit`,
};

export function ProductRouter(): React.ReactElement {
  const { orderSide = "buy", productId = "xxx" } = useParams<UPOrderConfirm>();

  const investFlowDynamicPaths = {
    step1: generatePath(`${investFlowStaticPaths.step1}`, { productId }),
    step2: generatePath(`${investFlowStaticPaths.step2}`, { orderSide, productId }),
    step3: generatePath(`${investFlowStaticPaths.step3}`, { orderSide, productId }),
  };

  return (
    <Switch404>
      <AuthRoute exact path={`${routePrefix.products}/create`}>
        <ProductCreate />
      </AuthRoute>
      <AuthRoute path={investFlowStaticPaths.step1}>
        <Product />
        <AuthRoute exact path={investFlowStaticPaths.step2}>
          <OrderConfirmDialog
            nextPath={investFlowDynamicPaths.step3}
            prevPath={investFlowDynamicPaths.step1}
          />
        </AuthRoute>
        <AuthRoute exact path={investFlowStaticPaths.step3}>
          <DepositDialog prevPath={investFlowDynamicPaths.step2} />
        </AuthRoute>
      </AuthRoute>
      <AuthRoute exact path={`${routePrefix.products}`}>
        <Products />
      </AuthRoute>
    </Switch404>
  );
}
