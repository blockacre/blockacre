import React from "react";
import { Grid, Typography } from "@material-ui/core";
import { Helmet } from "react-helmet";
import { SceneHeader, SceneMain, SceneWrap } from "../../components/layout";
import { preventShift } from "../../lib";
import { OrderCreateCard } from "../orders/components";
import { LoadingPage, NotFoundPage } from "../utility";
import { ProductCard } from "./components";
import { useProduct } from "./hooks";

export function Product(): React.ReactElement {
  const { loading, product } = useProduct();
  if (!loading && !product) {
    return <NotFoundPage />;
  }

  const title = preventShift(product?.name, { loading });
  return (
    <SceneWrap>
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <SceneHeader>
        <Typography variant="h1">{title}</Typography>
      </SceneHeader>
      {loading && <LoadingPage />}
      {product && (
        <SceneMain maxWidth="lg">
          <Grid container spacing={3}>
            <React.Fragment>
              <Grid container item md={6} xs={12}>
                <Grid item xs={12}>
                  <ProductCard variant="detail" />
                </Grid>
              </Grid>
              <Grid container item md={6} xs={12}>
                <Grid item xs={12}>
                  <OrderCreateCard />
                </Grid>
              </Grid>
            </React.Fragment>
          </Grid>
        </SceneMain>
      )}
    </SceneWrap>
  );
}
