import React from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import { Typography } from "@material-ui/core";
import { LoadingButton } from "@material-ui/lab";
import { Helmet } from "react-helmet";
import { useForm } from "react-hook-form";
import { ProductCreateInput, ProductType } from "../../api.generated";
import { FormGrid, FormItem, InputField } from "../../components/forms";
import { SceneFooter, SceneHeader, SceneMain } from "../../components/layout";
import { SceneWrap } from "../../components/layout/SceneWrap";
import { ProductCreateFormFields } from "../../types";
import { formDefaults, formLabels, formSchema, useProductCreateMutation } from "./lib";

export function ProductCreate(): React.ReactElement {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { errors, handleSubmit, register } = useForm<ProductCreateFormFields>({
    defaultValues: formDefaults,
    resolver: yupResolver(formSchema),
  });

  const [productCreateMutation, { loading }] = useProductCreateMutation();

  async function onFormSubmit(values: ProductCreateInput): Promise<void> {
    await productCreateMutation({ variables: { input: values } });
  }

  return (
    <SceneWrap>
      <Helmet>
        <title>Create New Investment Product</title>
      </Helmet>
      <SceneHeader>
        <Typography variant="h1">New Investment Product</Typography>
        <Typography>Please use the form below to create and publish a new product</Typography>
      </SceneHeader>
      <SceneMain maxWidth="md">
        <form id="ProductCreate" onSubmit={handleSubmit(onFormSubmit)}>
          <FormGrid>
            <FormItem>
              <InputField errors={errors} label={formLabels.name} name="name" register={register} />
            </FormItem>
            <FormItem>
              <InputField
                select
                errors={errors}
                label={formLabels.type}
                name="type"
                register={register}
                SelectProps={{
                  native: true,
                }}
              >
                {Object.values(ProductType).map((option) => (
                  <option key={option} value={option}>
                    {option}
                  </option>
                ))}
              </InputField>
            </FormItem>
            <FormItem>
              <InputField
                errors={errors}
                label={formLabels.unitPrice}
                name="unitPrice"
                register={register}
              />
            </FormItem>
            <FormItem>
              <InputField
                errors={errors}
                label={formLabels.partsPerUnit}
                name="partsPerUnit"
                register={register}
              />
            </FormItem>
            <FormItem>
              <InputField
                errors={errors}
                label={formLabels.minimum}
                name="minimum"
                register={register}
              />
            </FormItem>
            <input ref={register} name="currency" type="hidden" />
          </FormGrid>
        </form>
      </SceneMain>
      <SceneFooter>
        <LoadingButton
          fullWidth
          color="secondary"
          form="ProductCreate"
          pending={loading}
          type="submit"
          variant="contained"
        >
          Create
        </LoadingButton>
      </SceneFooter>
    </SceneWrap>
  );
}
