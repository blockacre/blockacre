/* This is a generated file, do not edit it directly */
import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
import * as Types from "../../../api.generated";
import {
  ErrorFieldsFragment,
  ErrorFieldsFragmentDoc,
  RecordFieldsFragmentDoc,
  RecordFieldsOrderFragment,
  RecordFieldsProductFragment,
  RecordFieldsTransactionFragment,
  RecordFieldsTransferFragment,
  RecordFieldsWalletFragment,
} from "../../../graphql/fragments.generated";

export type ProductCreateMutationVariables = Types.Exact<{
  input?: Types.Maybe<Types.ProductCreateInput>;
}>;

export type ProductCreateMutation = {
  __typename?: "Mutation";
  productcreate?: Types.Maybe<{
    __typename?: "ProductResponse";
    data?: Types.Maybe<
      {
        __typename?: "Product";
        currency: Types.Currency;
        issued: number;
        minimum: number;
        name: string;
        partPrice: number;
        partsPerUnit: number;
        type: Types.ProductType;
        unitPrice: number;
      } & RecordFieldsProductFragment
    >;
    error?: Types.Maybe<{ __typename?: "Error" } & ErrorFieldsFragment>;
  }>;
};

export type ProductsQueryVariables = Types.Exact<{
  input: Types.QueryInput;
}>;

export type ProductsQuery = {
  __typename?: "Query";
  products?: Types.Maybe<{
    __typename?: "ProductsResponse";
    data?: Types.Maybe<
      Array<
        Types.Maybe<
          {
            __typename?: "Product";
            currency: Types.Currency;
            issued: number;
            minimum: number;
            name: string;
            partPrice: number;
            partsPerUnit: number;
            type: Types.ProductType;
            unitPrice: number;
          } & RecordFieldsProductFragment
        >
      >
    >;
    error?: Types.Maybe<{ __typename?: "Error" } & ErrorFieldsFragment>;
  }>;
};

export const ProductCreateDocument = /* #__PURE__ */ gql`
  mutation productCreate($input: ProductCreateInput) {
    productcreate(input: $input) {
      data {
        ...recordFields
        name
        type
        currency
        minimum
        unitPrice
        partsPerUnit
        partPrice
        issued
      }
      error {
        ...errorFields
      }
    }
  }
  ${RecordFieldsFragmentDoc}
  ${ErrorFieldsFragmentDoc}
`;

/**
 * __useProductCreateMutation__
 *
 * To run a mutation, you first call `useProductCreateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useProductCreateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [productCreateMutation, { data, loading, error }] = useProductCreateMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useProductCreateMutation(
  baseOptions?: Apollo.MutationHookOptions<ProductCreateMutation, ProductCreateMutationVariables>
) {
  return Apollo.useMutation<ProductCreateMutation, ProductCreateMutationVariables>(
    ProductCreateDocument,
    baseOptions
  );
}
export type ProductCreateMutationHookResult = ReturnType<typeof useProductCreateMutation>;
export type ProductCreateMutationResult = Apollo.MutationResult<ProductCreateMutation>;
export const ProductsDocument = /* #__PURE__ */ gql`
  query products($input: QueryInput!) {
    products(input: $input) {
      data {
        ...recordFields
        name
        type
        currency
        minimum
        unitPrice
        partsPerUnit
        partPrice
        issued
      }
      error {
        ...errorFields
      }
    }
  }
  ${RecordFieldsFragmentDoc}
  ${ErrorFieldsFragmentDoc}
`;

/**
 * __useProductsQuery__
 *
 * To run a query within a React component, call `useProductsQuery` and pass it any options that fit your needs.
 * When your component renders, `useProductsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProductsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useProductsQuery(
  baseOptions: Apollo.QueryHookOptions<ProductsQuery, ProductsQueryVariables>
) {
  return Apollo.useQuery<ProductsQuery, ProductsQueryVariables>(ProductsDocument, baseOptions);
}
export function useProductsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<ProductsQuery, ProductsQueryVariables>
) {
  return Apollo.useLazyQuery<ProductsQuery, ProductsQueryVariables>(ProductsDocument, baseOptions);
}
export type ProductsQueryHookResult = ReturnType<typeof useProductsQuery>;
export type ProductsLazyQueryHookResult = ReturnType<typeof useProductsLazyQuery>;
export type ProductsQueryResult = Apollo.QueryResult<ProductsQuery, ProductsQueryVariables>;
