import { Product } from "../../../api.generated";
import { TDLItem } from "../../../components/lists";
import { amountToString } from "../../../lib";

type ProductData = Pick<
  Product,
  "type" | "currency" | "issued" | "minimum" | "partPrice" | "partsPerUnit"
>;

export function productData(product: ProductData): TDLItem[] {
  return [
    { detail: product.type, title: "Type" },
    { detail: product.currency, title: "Currency" },
    { detail: product.issued, title: "Issued" },
    { detail: product.minimum, title: "Minimum Units" },
    { detail: amountToString(product.partPrice, product.currency), title: "Part Price" },
    { detail: product.partsPerUnit, title: "Parts Per Unit" },
  ];
}
