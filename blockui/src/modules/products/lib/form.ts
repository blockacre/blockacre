import { number, object, ObjectSchema, string } from "yup";
import { Currency, ProductType } from "../../../api.generated";
import { initValidation } from "../../../initValidation";
import { ProductCreateFormFieldNames, ProductCreateFormFields } from "../../../types";

export const formDefaults: ProductCreateFormFields = {
  currency: Currency.GBP,
  minimum: "",
  name: "",
  partsPerUnit: "",
  type: "",
  unitPrice: "",
};
export const formLabels: Record<ProductCreateFormFieldNames, string> = {
  currency: "Currency",
  minimum: "Minimum Units for Issuance & Redemption",
  name: "Product Name",
  partsPerUnit: "Parts Per Unit",
  type: "Product Type",
  unitPrice: "Unit Price",
};

const currencyValues = Object.values(Currency);
const productTypeValues = Object.values(ProductType);

initValidation();

export const formSchema: ObjectSchema<ProductCreateFormFields> = object()
  .shape({
    currency: string().label(formLabels.currency).required().oneOf(currencyValues),
    minimum: number().skipType().label(formLabels.minimum).required().positive().integer(),
    name: string().label(formLabels.name).required(),
    partsPerUnit: number()
      .skipType()
      .label(formLabels.partsPerUnit)
      .required()
      .positive()
      .integer(),
    type: string().label(formLabels.type).required().oneOf(productTypeValues),
    unitPrice: number().skipType().label(formLabels.unitPrice).required().positive(),
  })
  .required();
