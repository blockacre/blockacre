import { useParams } from "react-router";
import { DocType, Product } from "../../../api.generated";
import { createSimpleQuery } from "../../../graphql/queries";
import { ProductsQueryResult, useProductsQuery } from "../lib";

type UPProductDetail = {
  productId: string;
};

type RTUseProducts = Omit<ProductsQueryResult, "data"> & {
  product: null | Product;
  productId: string;
};

/**
 * Helper hook to get a single product from the current productId in the url
 */
export function useProduct(): RTUseProducts {
  const { productId } = useParams<UPProductDetail>();
  const { data, ...rest } = useProductsQuery({
    variables: createSimpleQuery(DocType.product, productId),
  });
  const products = data?.products?.data || [null];
  const product = products[0];
  return { product, productId, ...rest };
}
