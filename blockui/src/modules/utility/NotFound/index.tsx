import React from "react";
import { Link as MuiLink, Typography } from "@material-ui/core";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { CenteredFullPage } from "../../../components/layout";

export function NotFoundPage(): React.ReactElement {
  return (
    <React.Fragment>
      <Helmet>
        <title>Page Not Found</title>
      </Helmet>
      <CenteredFullPage>
        <Typography paragraph component="h1" variant="h2">
          404, sorry we couldn’t find this page.
        </Typography>
        <Typography>
          Please visit the application{" "}
          <MuiLink component={Link} to="/">
            homepage
          </MuiLink>{" "}
          and try again.
        </Typography>
      </CenteredFullPage>
    </React.Fragment>
  );
}
