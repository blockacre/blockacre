import React from "react";
import { Alert, Typography } from "@material-ui/core";
import { Helmet } from "react-helmet";
import { CenteredFullPage } from "../../../components/layout";

type PErrorPage = {
  error: string;
};
export function ErrorPage({ error }: PErrorPage): React.ReactElement {
  return (
    <React.Fragment>
      <Helmet>
        <title>Sorry something went wrong</title>
      </Helmet>
      <CenteredFullPage>
        <Typography paragraph component="h1" variant="h2">
          Uh oh, sorry there was an error loading this page.
        </Typography>
        <Alert severity="error" variant="outlined">
          {error}
        </Alert>
      </CenteredFullPage>
    </React.Fragment>
  );
}
