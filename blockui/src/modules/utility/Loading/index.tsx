import React from "react";
import { CircularProgress } from "@material-ui/core";
import { Helmet } from "react-helmet";
import { CenteredFullPage } from "../../../components/layout";

export function LoadingPage(): React.ReactElement {
  return (
    <React.Fragment>
      <Helmet>
        <title>Loading...</title>
      </Helmet>
      <CenteredFullPage>
        <CircularProgress color="secondary" />
      </CenteredFullPage>
    </React.Fragment>
  );
}
