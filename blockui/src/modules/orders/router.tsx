import React from "react";
import { AuthRoute } from "../../auth";
import { Switch404 } from "../../components/navigation";
import { routePrefix } from "../../routing";
import { Order } from "./detail";
import { Orders } from "./list";

export function OrdersRouter(): React.ReactElement {
  return (
    <Switch404>
      <AuthRoute exact path={`${routePrefix.investor}/orders/:orderId`}>
        <Order />
      </AuthRoute>
      <AuthRoute exact path={`${routePrefix.investor}/orders`}>
        <Orders />
      </AuthRoute>
    </Switch404>
  );
}
