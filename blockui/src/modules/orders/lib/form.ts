import { number, object, ObjectSchema, string } from "yup";
import { OrderFrequency } from "../../../api.generated";
import { initValidation } from "../../../initValidation";
import { OrderCreateFormFieldNames, OrderCreateFormFields } from "../../../types";

export const formDefaults: OrderCreateFormFields = {
  orderFrequency: OrderFrequency.single,
  parts: 5,
};
export const formLabels: Record<OrderCreateFormFieldNames, string> = {
  orderFrequency: "Investment Frequency",
  parts: "Number of Parts",
};

initValidation();

const orderFrequencyValues = Object.values(OrderFrequency);

export const formSchema: ObjectSchema<OrderCreateFormFields> = object()
  .shape({
    orderFrequency: string()
      .label(formLabels.orderFrequency)
      .required()
      .oneOf(orderFrequencyValues),
    parts: number().skipType().label(formLabels.parts).required().positive().integer(),
  })
  .required();
