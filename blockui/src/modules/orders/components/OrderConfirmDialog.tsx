import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@material-ui/core";
import { LoadingButton } from "@material-ui/lab";
import { generatePath, LinkProps, useHistory, useLocation, useParams } from "react-router-dom";
import { OrderSide } from "../../../api.generated";
import { CardHeading } from "../../../components/cards";
import { TextList, TextListItem } from "../../../components/lists";
import { amountToString } from "../../../lib";
import { LSOrderCreate } from "../../../types";
import { useProduct } from "../../products/hooks";

type UPOrderConfirm = {
  orderSide: OrderSide;
  productId: string;
};

type POrderConfirmDialog = {
  nextPath: string;
};

export function OrderConfirmDialog({ nextPath }: POrderConfirmDialog): React.ReactElement | null {
  const { orderSide, productId } = useParams<UPOrderConfirm>();
  const history = useHistory();
  const { state } = useLocation<LSOrderCreate>();
  const { product } = useProduct();

  if (!product) {
    return null;
  }

  const actionText = orderSide === OrderSide.buy ? "invest" : "sell parts";

  function handleConfirm(): void {
    const depositPath: LinkProps<LSOrderCreate>["to"] = {
      pathname: generatePath(nextPath, {
        orderSide,
        productId,
      }),
      state: {
        ...state,
        confirmed: true,
      },
    };
    history.push(depositPath);
  }

  return (
    <Dialog fullWidth open aria-labelledby="confirm-order-dialog" maxWidth="sm">
      <DialogTitle id="confirm-order-dialog">
        <CardHeading>Confirm Your Order</CardHeading>
      </DialogTitle>
      <DialogContent>
        <Typography paragraph>
          You are about to create an order to {actionText} in the <strong>{product.name}</strong>.
          Please review your order details below.
        </Typography>
        <TextList paragraph>
          <TextListItem detail={product.name} title="Product Name" />
          <TextListItem
            detail={amountToString(product.partPrice, product.currency)}
            title="Part Price"
          />
          <TextListItem detail={state.parts} title="Number of Parts" />
          <TextListItem detail={state.totalAsString} title="Total Investment" />
        </TextList>
        <Typography>
          Once you click confirm, your order will be placed in a pending state until we receive your
          payment. You will be shown details of how to make payment in the next step.
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={(): void => history.goBack()}>
          Cancel
        </Button>
        <LoadingButton variant="contained" onClick={handleConfirm}>
          Confirm
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
}
