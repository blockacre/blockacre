/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Card, CardActions, CardContent, Tab, Tabs } from "@material-ui/core";
import queryString from "query-string";
import { useForm } from "react-hook-form";
import { useHistory, useLocation, useParams } from "react-router-dom";
import { OrderSide } from "../../../api.generated";
import { AmountSlider, FormGrid, FormItem, OrderFrequencyRadio } from "../../../components/forms";
import { a11yProps, TabPanel } from "../../../components/tabs";
import { amountToString, multiply } from "../../../lib";
import { OrderCreateFormFields, OrderCreateSubmitValues } from "../../../types";
import { useProduct } from "../../products/hooks";
import { formDefaults, formLabels, formSchema } from "../lib";

type POrderCreateCard = {
  onSubmit: (values: OrderCreateSubmitValues) => void;
};

type UPOrderCreateCard = {
  orderSide: OrderSide;
};

export function OrderCreateCard({ onSubmit }: POrderCreateCard): React.ReactElement | null {
  const { product } = useProduct();
  const { orderSide } = useParams<UPOrderCreateCard>();
  const { search } = useLocation();
  const history = useHistory();
  const urlQ = queryString.parse(search);
  if (!orderSide) {
    history.replace(`/${OrderSide.buy}`);
  }

  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { control, handleSubmit, watch } = useForm<OrderCreateFormFields>({
    defaultValues: { ...formDefaults, ...urlQ },
    resolver: yupResolver(formSchema),
  });

  if (!product) {
    return null;
  }

  function onFormSubmit(formValues: OrderCreateFormFields): void {
    const values: OrderCreateSubmitValues = {
      ...formValues,
      side: orderSide,
    };
    onSubmit(values);
  }

  const parts = watch("parts");
  const totalAsNumber = multiply(parts, product.partPrice);
  const totalAsString = amountToString(totalAsNumber, product.currency);

  const a11yPropsBuy = a11yProps("orderside", OrderSide.buy);
  const a11yPropsSell = a11yProps("orderside", OrderSide.sell);

  return (
    <Card>
      <Tabs
        centered
        aria-label="Invest or Sell Order tabs"
        value={orderSide}
        variant="fullWidth"
        onChange={(_, newValue: OrderSide): void => history.replace(`./${newValue}`)}
      >
        <Tab label="Invest" value={OrderSide.buy} {...a11yPropsBuy.tab} />
        <Tab label="Sell" value={OrderSide.sell} {...a11yPropsSell.tab} />
      </Tabs>
      <TabPanel index={orderSide} value={OrderSide.buy} {...a11yPropsBuy.tabPanel}>
        <form id="OrderCreateCard" onSubmit={handleSubmit(onFormSubmit)}>
          <CardContent>
            <FormGrid>
              <FormItem fullWidth>
                <AmountSlider
                  control={control}
                  label={formLabels.parts}
                  name="parts"
                  total={totalAsString}
                />
              </FormItem>
              <FormItem fullWidth>
                <OrderFrequencyRadio
                  control={control}
                  defaultValue={formDefaults.orderFrequency}
                  label={formLabels.orderFrequency}
                  name="orderFrequency"
                />
              </FormItem>
            </FormGrid>
          </CardContent>
          <CardActions>
            <Button fullWidth color="primary" type="submit" variant="contained">
              Invest
            </Button>
          </CardActions>
        </form>
      </TabPanel>
    </Card>
  );
}
