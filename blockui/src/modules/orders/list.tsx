import React from "react";
import { Helmet } from "react-helmet";

export function Orders(): React.ReactElement {
  return (
    <React.Fragment>
      <Helmet>
        <title>Orders List</title>
      </Helmet>
      Actions: View Detail, Create New Order (buy or sell more)
    </React.Fragment>
  );
}
