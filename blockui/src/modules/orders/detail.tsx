import React from "react";
import { Helmet } from "react-helmet";
import { useParams } from "react-router-dom";

type UPOrderDetail = {
  orderId: string;
};

export function Order(): React.ReactElement {
  const { orderId } = useParams<UPOrderDetail>();
  return (
    <React.Fragment>
      <Helmet>
        <title>Order Detail</title>
      </Helmet>
      orderId: {orderId}. Actions: Stop
    </React.Fragment>
  );
}
