import React from "react";
import { Helmet } from "react-helmet";

export function Transactions(): React.ReactElement {
  return (
    <React.Fragment>
      <Helmet>
        <title>Transactions List</title>
      </Helmet>
      Outgoing and Incoming Transactions List (Fiat). Actions: View Detail, Deposit, Withdraw
    </React.Fragment>
  );
}
