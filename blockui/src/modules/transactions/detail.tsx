import React from "react";
import { Helmet } from "react-helmet";
import { useParams } from "react-router-dom";

type UPTransactionDetail = {
  transactionId: string;
};

export function Transaction(): React.ReactElement {
  const { transactionId } = useParams<UPTransactionDetail>();
  return (
    <React.Fragment>
      <Helmet>
        <title>Transaction Detail</title>
      </Helmet>
      transactionId: {transactionId}. Actions: Stop
    </React.Fragment>
  );
}
