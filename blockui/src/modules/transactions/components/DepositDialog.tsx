import React from "react";
import {
  Alert,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@material-ui/core";
import { useLocation } from "react-router-dom";
import { CardHeading } from "../../../components/cards";
import { TextList, TextListItem } from "../../../components/lists";
import { LSOrderCreate } from "../../../types";
import { useWallet } from "../../user/hooks";

export function DepositDialog(): React.ReactElement | null {
  const { ref } = useWallet();
  const { state } = useLocation<LSOrderCreate>();
  return (
    <Dialog fullWidth open aria-labelledby="confirm-order-dialog" maxWidth="sm">
      <DialogTitle id="confirm-order-dialog">
        <CardHeading>Your Order is Ready</CardHeading>
      </DialogTitle>
      <DialogContent>
        <Typography paragraph>
          You’re all set! Please now deposit your payment to our bank details below.
        </Typography>
        <TextList paragraph>
          <TextListItem detail={ref} title="Your Reference" />
          <TextListItem detail={state.totalAsString} title="Total" />
          <TextListItem detail="Blockacre Ltd." title="Account Name" />
          <TextListItem detail="01234567" title="Account Number" />
          <TextListItem detail="01-01-01" title="Sort Code" />
        </TextList>
        <Alert severity="warning">
          To ensure your deposit is accepted, you must include your reference, the deposited amount
          must match the total above and the payment must come from one of your registered bank
          accounts.
        </Alert>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined">Finish</Button>
      </DialogActions>
    </Dialog>
  );
}
