import React from "react";
import { Helmet } from "react-helmet";

export function WithdrawCreate(): React.ReactElement {
  return (
    <React.Fragment>
      <Helmet>
        <title>Withdraw</title>
      </Helmet>
      Withdraw Balance Form
    </React.Fragment>
  );
}
