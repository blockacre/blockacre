import React from "react";
import { AuthRoute } from "../../auth";
import { Switch404 } from "../../components/navigation";
import { routePrefix } from "../../routing";
import { DepositCreate } from "./deposit/create";
import { Transaction } from "./detail";
import { Transactions } from "./list";
import { WithdrawCreate } from "./withdraw/create";

export function TransactionsRouter(): React.ReactElement {
  return (
    <Switch404>
      <AuthRoute exact path={`${routePrefix.investor}/transactions/withdraw/create`}>
        <WithdrawCreate />
      </AuthRoute>
      <AuthRoute exact path={`${routePrefix.investor}/transactions/deposit/create`}>
        <DepositCreate />
      </AuthRoute>
      <AuthRoute exact path={`${routePrefix.investor}/transactions/:transactionId`}>
        <Transaction />
      </AuthRoute>
      <AuthRoute exact path={`${routePrefix.investor}/transactions`}>
        <Transactions />
      </AuthRoute>
    </Switch404>
  );
}
