import React from "react";
import { Helmet } from "react-helmet";

export function DepositCreate(): React.ReactElement {
  return (
    <React.Fragment>
      <Helmet>
        <title>Deposit</title>
      </Helmet>
      Deposit Balance (including recurring) Form
    </React.Fragment>
  );
}
