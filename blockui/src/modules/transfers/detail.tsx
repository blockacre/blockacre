import React from "react";
import { Helmet } from "react-helmet";
import { useParams } from "react-router-dom";

type UPTransferDetail = {
  transferId: string;
};

export function Transfer(): React.ReactElement {
  const { transferId } = useParams<UPTransferDetail>();
  return (
    <React.Fragment>
      <Helmet>
        <title>Transfer Detail</title>
      </Helmet>
      transferId: {transferId}. Transfer Detail. Actions: Query
    </React.Fragment>
  );
}
