import React from "react";
import { AuthRoute } from "../../auth";
import { Switch404 } from "../../components/navigation";
import { routePrefix } from "../../routing";
import { Transfer } from "./detail";
import { Transfers } from "./list";

export function TransfersRouter(): React.ReactElement {
  return (
    <Switch404>
      <AuthRoute exact path={`${routePrefix.investor}/transfers/:transferId`}>
        <Transfer />
      </AuthRoute>
      <AuthRoute exact path={`${routePrefix.investor}/transfers`}>
        <Transfers />
      </AuthRoute>
    </Switch404>
  );
}
