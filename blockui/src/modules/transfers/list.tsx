import React from "react";
import { Helmet } from "react-helmet";

export function Transfers(): React.ReactElement {
  return (
    <React.Fragment>
      <Helmet>
        <title>Transfers List</title>
      </Helmet>
      Outgoing and Incoming Transfers List. Actions: View Detail Transfers can not be created
      manually. They are only created to fulfil an order
    </React.Fragment>
  );
}
