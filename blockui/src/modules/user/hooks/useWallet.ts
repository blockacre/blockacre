import { DocType, Status, Wallet } from "../../../api.generated";

export function useWallet(): Wallet {
  return {
    created: "",
    createdBy: "",
    docType: DocType.wallet,
    id: "",
    ref: "fakewalletref",
    status: Status.active,
  };
}
