export function multiply(...amounts: number[]): number {
  return amounts.reduce((prev, next) => {
    const nextAsNumber = next ? Number(next) : 1;
    return prev * nextAsNumber;
  });
}
