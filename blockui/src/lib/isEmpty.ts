import { Maybe } from "../api.generated";

export function isEmpty(arr: Maybe<Array<unknown>>): boolean {
  if (!Array.isArray(arr)) return true;
  return arr.length === 0;
}
