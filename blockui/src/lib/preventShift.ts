type TPreventShift = {
  fallback?: string;
  loading?: boolean;
  loadingFallback?: string;
};

export function preventShift(
  str = "",
  { fallback = "\u00A0", loading = false, loadingFallback = "..." }: TPreventShift
): string {
  if (loading) {
    return loadingFallback;
  }
  return str || fallback;
}
