import { Currency } from "../api.generated";

const currencySymbols = Object.freeze({
  EUR: "€",
  GBP: "£",
  USD: "$",
});

function formatDecimal(amount: number): string {
  if (typeof amount !== "number") {
    return "";
  }

  return Number(amount).toLocaleString(undefined, {
    maximumFractionDigits: 2,
    minimumFractionDigits: 2,
  });
}

function currencyCodeToSymbol(code: Currency): string {
  return currencySymbols[code] ?? "";
}

export function amountToString(amount: number, code: Currency): string {
  return `${currencyCodeToSymbol(code)}${formatDecimal(amount)}`;
}
