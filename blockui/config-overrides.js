/* eslint-disable react-hooks/rules-of-hooks */
const {
  addBabelPlugin,
  addBabelPreset,
  addBundleVisualizer,
  addWebpackPlugin,
  override,
} = require("customize-cra");
const overrides = require("./webpack");

const hasSentry = !!process.env.SENTRY_AUTH_TOKEN;

module.exports = override(
  addBabelPlugin("@emotion/babel-plugin"),
  addBabelPreset(["@babel/preset-react", { importSource: "@emotion/react", runtime: "automatic" }]),
  addBundleVisualizer({}, true),
  addWebpackPlugin(overrides.configPlugin),
  hasSentry && addWebpackPlugin(overrides.sentryCliPlugin)
);
