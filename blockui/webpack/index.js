const SentryCliPlugin = require("@sentry/webpack-plugin");
const CONFIG = JSON.stringify(require("config"));
const paths = require("react-scripts/config/paths");
// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require("webpack");

const configPlugin = new webpack.DefinePlugin({
  CONFIG,
});

const sentryCliPlugin = new SentryCliPlugin({
  debug: true,
  include: paths.appBuild,
});
module.exports = {
  configPlugin,
  sentryCliPlugin,
};
