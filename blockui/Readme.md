# Acceptable PeerDependency Errors

When in a normal repo, these packages don't need to be in devDependencies because they are included by react-scripts.

```json
"@typescript-eslint/parser": "~3.5.0",
"babel-eslint": "~10.1.0",
"eslint": "~6.6.0",
"eslint-config-react-app": "~5.2.1",
"eslint-plugin-import": "~2.22.0",
"eslint-plugin-jsx-a11y": "~6.3.1",
"eslint-plugin-react": "~7.20.2",
"eslint-plugin-react-hooks": "~4.0.4",
```

These ones are also okay

```json
"@babel/core",
"webpack",
```

# TODO:

- Auth
  - Get user profile on auth
  - Handle logout (reset all state / reload)
  - Handle auth errors (reset most state except login form)
  - Move 'user' out of auth reducer
