const extendsCommonStart = [
  // Order is important here
  "eslint:recommended",
  // `react-app` is not included in airbnb-typescript
  "react-app",
  // Re-Enable all react rules (some get overridden by Airbnb)
  "plugin:react/all",
  // Use airbnb with react support - we also add stricter checking later with plugin:react/all
  "airbnb",
];
const extendsCommonEnd = [
  "airbnb/hooks",
  "plugin:jsx-a11y/recommended",
  // Add browser compatibility linting
  "plugin:compat/recommended",
  // Resolve conflicting prettier formatting rules see: https://github.com/prettier/eslint-config-prettier
  "prettier",
  "prettier/babel",
  "prettier/react",
];

const commonRuleOverrides = {
  // Require braces in arrow function body as needed
  "arrow-body-style": ["error", "as-needed"],

  // Enforce function types
  "func-style": ["error", "declaration"],

  // Disallow default exports
  "import/no-default-export": "error",

  // Disallow node imports (this is a browser application)
  "import/no-nodejs-modules": "error",

  // Disallow /index in imports
  "import/no-useless-path-segments": [
    "error",
    {
      noUselessIndex: true,
    },
  ],

  // Enforce import order and no spaces between imports
  "import/order": [
    "warn",
    {
      alphabetize: {
        caseInsensitive: true,
        order: "asc",
      },
      "newlines-between": "never",
      pathGroups: [
        {
          group: "external",
          pattern: "react",
          position: "before",
        },
      ],
      pathGroupsExcludedImportTypes: ["react"],
    },
  ],

  // Allow named exports from files with a single export
  "import/prefer-default-export": "off",

  // Adjust no-void so we can use void when calling promises in useEffect
  "no-void": ["error", { allowAsStatement: true }],

  // Prefer arrow functions in callbacks
  "prefer-arrow-callback": "error",

  // Enforce React.Fragment vs <>
  "react/jsx-fragments": ["error", "element"],

  // Turn off nested jsx rule
  "react/jsx-max-depth": "off",

  // Allow text in jsx components
  "react/jsx-no-literals": "off",

  // No unnecessary fragments
  "react/jsx-no-useless-fragment": "error",

  // TODO: investigate re-enabling
  // "react/jsx-props-no-spreading": "off",

  // Sort react props
  "react/jsx-sort-props": [
    // Just show warnings for sort order
    "warn",
    {
      callbacksLast: true,
      ignoreCase: true,
      noSortAlphabetically: false,
      reservedFirst: true,
      shorthandFirst: true,
    },
  ],

  // Turn off required default props - seems unnecessary with TS but could re-enable later
  "react/require-default-props": "off",

  // Sort destructure keys, just show warnings for sort order
  "sort-destructure-keys/sort-destructure-keys": ["warn", { caseSensitive: false }],

  // Sort imports
  "sort-imports": [
    // Just show warnings for sort order
    "warn",
    {
      ignoreCase: true,
      ignoreDeclarationSort: true,
      ignoreMemberSort: false,
    },
  ],

  // Sort keys in objects etc, just show warnings for sort order
  "sort-keys-fix/sort-keys-fix": ["warn", "asc", { caseSensitive: false, natural: true }],
};

module.exports = {
  env: {
    browser: true,
  },
  overrides: [
    // Configuration for all javascript files
    {
      extends: [...extendsCommonStart, ...extendsCommonEnd],
      files: ["**/*.js", "**/*.jsx"],
      plugins: ["jsx-a11y", "sort-keys-fix", "sort-destructure-keys"],
      rules: {
        ...commonRuleOverrides,
      },
    },
    // Configuration for all typescript files
    {
      extends: [
        ...extendsCommonStart,
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "airbnb-typescript",
        "plugin:typescript-sort-keys/recommended",
        "prettier/@typescript-eslint",
        ...extendsCommonEnd,
      ],
      files: ["**/*.ts", "**/*.tsx"],
      parser: "@typescript-eslint/parser",
      parserOptions: {
        project: ["./tsconfig.json"],
        tsconfigRootDir: __dirname,
      },
      plugins: ["@typescript-eslint", "jsx-a11y", "sort-keys-fix", "sort-destructure-keys"],
      rules: {
        ...commonRuleOverrides,

        // Enforce 'type' for type definitions
        "@typescript-eslint/consistent-type-definitions": ["error", "type"],

        // Enforce return type on all functions, not just exported
        "@typescript-eslint/explicit-function-return-type": "error",

        // Fixes "no-shadow" false positives
        "@typescript-eslint/no-shadow": "error",

        // False positives, fixed by @typescript-eslint/no-unused-vars-experimental rule
        // Just show warnings so that it doesn't prevent dev builds
        "@typescript-eslint/no-unused-vars": "off",
        "@typescript-eslint/no-unused-vars-experimental": "warn",

        // False positives, fixed by @typescript-eslint/no-shadow rule
        "no-shadow": "off",

        // Just show warnings for sort order
        "typescript-sort-keys/interface": "warn",
        "typescript-sort-keys/string-enum": "warn",
      },
    },
    // All root and Webpack Javascript files
    {
      files: ["./*.js", "./webpack/**/*.js"],
      rules: {
        // Allow 'require' in root js files
        "@typescript-eslint/no-var-requires": "off",
        "import/no-extraneous-dependencies": ["error", { devDependencies: true }],
      },
    },
    {
      // Allow these rules in generated files
      files: ["**/generated/**", "*.generated.*"],
      rules: {
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/no-unused-vars-experimental": "off",
        "import/no-default-export": "off",
      },
    },
  ],
  root: true,
  settings: {
    polyfills: ["Promise"],
  },
};
