import { Money } from "../generated/types";

export const formatDecimal = (num: number) =>
  Number(num).toLocaleString(undefined, {
    maximumFractionDigits: 2,
    minimumFractionDigits: 2,
  });

const currencySymbols = Object.freeze({
  EUR: "€", // Euro
  GBP: "£", // Pound Sterling
  USD: "$", // US Dollar
});

type CurrencyCode = keyof typeof currencySymbols;

export const currencies = Object.keys(currencySymbols) as CurrencyCode[];

export const currencyCodeToSymbol = (code: string) => currencySymbols[code as CurrencyCode];

export const amountToString = (amount: Money) =>
  currencyCodeToSymbol(amount.currency) + formatDecimal(amount.amount);
