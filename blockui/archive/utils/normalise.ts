export const integer = (val: string) => {
  const s = val.replace(/[^\d]/g, "");
  return s ? Number(s) : "";
};

export const decimal = (val: string) =>
  val.replace(/[^\d.]/g, "").replace(/^.*?(\d+(\.\d{0,2})?).*?$/g, "$1");
