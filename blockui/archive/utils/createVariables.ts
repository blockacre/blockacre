export const createVariables = <I>(input: I) => ({
  variables: {
    input,
  },
});
