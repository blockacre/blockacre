import { get, set } from "lodash";

type StringMap = {
  [key: string]: string;
}

type ValidatorFunction = (value: string, data?: { [s: string]: string }) => string | null;

const isEmpty = (value: any) =>
  value === undefined || value === null || value === "" || Number.isNaN(value);

const join = (rules: Array<Function>) => (value: any, data: any, params: any) =>
  rules.map((rule: Function) => rule(value, data, params)).find((error) => !!error);

export const email: ValidatorFunction = (value: string) => {
  if (!isEmpty(value) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    return "Invalid email address";
  }
  return null;
};

export const required: ValidatorFunction = (value: string) => {
  if (isEmpty(value)) {
    return "Required";
  }
  return null;
};

export const minLength = (num: number): ValidatorFunction => function(value: string) {
  if (!isEmpty(value) && value.length < num) {
    return `Must be at least ${num} characters`;
  }
  return null;
};

export const maxLength = (num: number): ValidatorFunction => function(value: string) {
  if (!isEmpty(value) && value.length > num) {
    return `Must be no more than ${num} characters`;
  }
  return null;
};

export const min = (num: number): ValidatorFunction => function(value: string) {
  if (!isEmpty(value) && Number(value) < num) {
    return `Must be at least ${num}`;
  }
  return null;
};

export const max = (num: number): ValidatorFunction => function(value: string) {
  if (!isEmpty(value) && Number(value) > num) {
    return `Must be no more than ${num}`;
  }
  return null;
};

export const integer: ValidatorFunction = (value: string) => {
  if (!isEmpty(value) && !Number.isInteger(Number(value))) {
    return "Must be an integer";
  }
  return null;
};

export const number: ValidatorFunction = (value: string) => {
  if (!isEmpty(value) && Number.isNaN(Number(value))) {
    return "Must be an number";
  }
  return null;
};

export const oneOf = (enumeration: Array<string>): ValidatorFunction => function(value: string) {
  // eslint-disable-next-line no-bitwise
  if (!isEmpty(value) && !~enumeration.indexOf(value)) {
    return `Must be one of: ${enumeration.join(", ")}`;
  }
  return null;
};

export const match = (field: string): ValidatorFunction => function(value: string, data?: StringMap) {
  if (!isEmpty(value) && data) {
    if (value !== data[field]) {
      return `Doesn't match field: ${field}`;
    }
  }
  return null;
};

export const createValidator = (
  rules: { [s: string]: ValidatorFunction | ValidatorFunction[] },
  params?: any
) => (data: StringMap = {}) => {
  const errors: StringMap = {};
  Object.keys(rules).forEach((key) => {
    const rule = join(([] as ValidatorFunction[]).concat(rules[key])); // concat enables both functions and arrays of functions
    const error = rule(get(data, key), data, { key, ...params });
    if (error) {
      set(errors, key, error);
    }
  });
  return errors;
};
