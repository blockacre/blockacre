import React, { ComponentType, lazy, Suspense } from "react";

export const loadable = (
  importFunc: () => Promise<{ default: ComponentType<any> }>,
  { fallback = null }: { fallback: JSX.Element | null } = { fallback: null }
) => {
  const LazyComponent = lazy(importFunc);

  const Loadable = (props: any) => (
    <Suspense fallback={fallback}>
      <LazyComponent {...props} />
    </Suspense>
  );

  return Loadable;
};
