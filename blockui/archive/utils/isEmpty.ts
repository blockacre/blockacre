import { Asset, Money } from "../../src/generated/types";

export const isEmptyBalance = (arr: Array<Money> | undefined | null): boolean => {
  // return arr[0].amount === 0;
  if (!Array.isArray(arr)) return true;
  return arr[0].amount === 0;
};

export const isEmptyPortfolio = (arr: Array<Asset> | undefined | null): boolean => {
  // return arr[0].amount === 0;
  if (!Array.isArray(arr)) return true;
  return arr[0].parts === 0;
};
