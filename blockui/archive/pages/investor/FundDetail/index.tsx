import React from "react";
import { Helmet } from "react-helmet";
import { ErrorPage } from "../../components/ErrorPage";
import { Loader } from "../../components/Loader";
import { PageHeader } from "../../components/PageHeader";
import { amountToString } from "../../utils/units";
import { Alert, Card, CardBody, CardHeader, Col, Row, Table } from "../Dashboard/node_modules/reactstrap";
import { BuySellWidget } from "./BuySellWidget";

export const ConsumerFundDetailsPageComponent: React.FC = () => {
  if (error) {
    return <ErrorPage error={error} />;
  }

  if (loading || !fund) {
    return <Loader />;
  }

  return (
    <article>
      <Helmet>
        <title>{`Fund: ${fund.name}`}</title>
      </Helmet>
      <PageHeader heading={`Fund: ${fund.name}`} />
      {error && <Alert>{error}</Alert>}
      <Row>
        <Col lg={6}>
          <Card className="mb-3">
            <CardHeader tag="h5">Fund details</CardHeader>
            <CardBody>
              <Table>
                <tbody>
                  <tr>
                    <th scope="row">Unit Price</th>
                    <td>{amountToString(fund.price)}</td>
                  </tr>
                  <tr>
                    <th scope="row">Type</th>
                    <td>{fund.type}</td>
                  </tr>
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
        <Col lg={6}>
          <BuySellWidget fund={fund} />
        </Col>
      </Row>
    </article>
  );
};
