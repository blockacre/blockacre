import React, { useState } from "react";
import { AmountForm } from "../../components/AmountForm";
import { Fund } from "../../generated/types";
import { Card, CardBody, CardHeader, Nav, NavItem, NavLink, TabContent, TabPane } from "../Dashboard/node_modules/reactstrap";

export const BuySellWidget = ({ fund: { price, id } }: { fund: Fund }) => {
  const [activeTab, setActiveTab] = useState(0);

  const toggle = (tab: number) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  return (
    <Card className="mb-3">
      <CardHeader tag="h5">
        <Nav tabs>
          <NavItem>
            <NavLink className={activeTab === 0 ? "active" : ""} onClick={() => toggle(0)}>
              Invest
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={activeTab === 1 ? "active" : ""} onClick={() => toggle(1)}>
              Sell
            </NavLink>
          </NavItem>
        </Nav>
      </CardHeader>
      <CardBody>
        <TabContent activeTab={activeTab}>
          <TabPane tabId={0}>
            <AmountForm
              currency={price.currency}
              endpoint="/orders/purchase"
              form="invest"
              initialValues={{ id }}
              successMessage="Buy order submitted successfully"
            />
          </TabPane>
          <TabPane tabId={1}>
            <AmountForm
              currency={price.currency}
              endpoint="/orders/sell"
              form="sell"
              initialValues={{ id }}
              successMessage="Sell order submitted successfully"
            />
          </TabPane>
        </TabContent>
      </CardBody>
    </Card>
  );
};
