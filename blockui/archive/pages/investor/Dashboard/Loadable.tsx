import React from "react";
import { loadable } from "../../../utils/loadable";
import { LoadingPage } from "../../utility/LoadingPage";

export const ConsumerDashboardPage = loadable(() => import("./index"), {
  fallback: <LoadingPage />,
});
