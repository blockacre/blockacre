import React from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { PageHeader } from "../../../../archive/components/layout/PageHeader";
import { Loader } from "../../../../archive/components/Loader";
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
} from "../../../../archive/layouts/investor/node_modules/reactstrap";
import { Order } from "../../../generated/types";
import { amountToString } from "../../../utils/units";

export const ConsumerDashboardPageComponent: React.FC = () => {
  const cash = Object.entries<number>(user.wallet.cashAvailable);
  const assets = Object.entries<number>(user.wallet.assets);
  return (
    <article>
      <Helmet>
        <title>Dashboard</title>
      </Helmet>
      <PageHeader heading="Dashboard" />
      <Row>
        <Col lg={6}>
          <Card className="mb-3">
            <CardHeader tag="h5">Available Balance</CardHeader>
            <CardBody>
              {!cash.length ? (
                <Alert className="d-flex justify-content-between" color="warning">
                  Your balance is 0
                  <Button size="sm" tag={Link} to="/transfers">
                    Top up
                  </Button>
                </Alert>
              ) : (
                <Row tag="dl">
                  {cash.map(([key, value]) => (
                    <React.Fragment key={key}>
                      <Col className="text-truncate" sm="6" tag="dt">
                        {key}
                      </Col>
                      <Col sm="6" tag="dd">
                        {value}
                      </Col>
                    </React.Fragment>
                  ))}
                </Row>
              )}
            </CardBody>
          </Card>
          <Card>
            <CardHeader tag="h5">Assets</CardHeader>
            <CardBody>
              {!assets.length ? (
                <Alert className="d-flex justify-content-between" color="warning">
                  You don&apos;t own any assets
                  <Button size="sm" tag={Link} to="/funds">
                    Invest
                  </Button>
                </Alert>
              ) : (
                <Row tag="dl">
                  {assets.map(([key, value]) => (
                    <React.Fragment key={key}>
                      <Col className="text-truncate" sm="6" tag="dt">
                        <Link to={`/funds/${key}`}>{key}</Link>
                      </Col>
                      <Col sm="6" tag="dd">
                        {value}
                      </Col>
                    </React.Fragment>
                  ))}
                </Row>
              )}
            </CardBody>
          </Card>
        </Col>
        <Col lg={6}>
          {orders.loading && <Loader />}
          {orders.data && !!orders.data.length && (
            <Card className="mb-3">
              <CardHeader tag="h5">Active Orders</CardHeader>
              <CardBody>
                <Table hover striped style={{ tableLayout: "fixed" }}>
                  <thead>
                    <tr>
                      <th>Type</th>
                      <th>Fund</th>
                      <th>Amount</th>
                      <th>Remaining</th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.data.map((order: Order) => (
                      <tr key={order.id}>
                        <td>{order.type}</td>
                        <td className="text-truncate">
                          <Link to={`/funds/${order.productId}`}>{order.productId}</Link>
                        </td>
                        <td>{amountToString(order.fullAmount)}</td>
                        <td>{amountToString(order.amount)}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          )}
        </Col>
      </Row>
    </article>
  );
};
