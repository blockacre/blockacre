import React from "react";
import { LoadingPage } from "../../components/LoadingPage";
import { loadable } from "../../utils/loadable";

export const FundInvestorsPage = loadable(() => import("./index"), { fallback: <LoadingPage /> });
