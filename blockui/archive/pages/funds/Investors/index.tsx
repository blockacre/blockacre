import React from "react";
import { Helmet } from "react-helmet";
import { Loader } from "../../components/Loader";
import { PageHeader } from "../../components/PageHeader";
import { Alert, Card, CardBody, Col, Row } from "../Edit/node_modules/reactstrap";
import { QueryForm } from "./QueryForm";

export const FundInvestorsPageComponent: React.FC = () => (
  <article>
    <Helmet>
      <title>Investors</title>
    </Helmet>
    <PageHeader heading="Investors" />
    <Row>
      <Col sm={4}>
        <Card>
          <CardBody>
            <QueryForm />
            <small>Example implementation of a filter form</small>
          </CardBody>
        </Card>
      </Col>
      <Col>
        {error && <Alert color="danger">{error}</Alert>}
        {loading && <Loader />}
        {investors && (
          <div className="list-group">
            {investors.map((investor) => (
              <div key={investor.ref} className="list-group-item">
                <h5>Reference: {investor.ref}</h5>
                Units: {investor.assets[fund.id]}
                <br />
                <small>Need user DB to show more details</small>
              </div>
            ))}
          </div>
        )}
      </Col>
    </Row>
  </article>
);
