import React from "react";
import { Input } from "../../components/Input";
import { Form } from "../Edit/node_modules/reactstrap";
import { Field } from "../Edit/node_modules/redux-form";

export const QueryFormComponent = () => (
  <Form onSubmit={handleSubmit}>
    <fieldset disabled={submitting}>
      <Field
        autocomplete="off"
        component={Input}
        format={(value: string) => (value ? value.substr(4) : "")}
        label="Reference"
        name="filter.ref.$regex"
        parse={(value: string) => `(?i)${value}`}
        type="text"
      />
    </fieldset>
  </Form>
);
