import React from "react";
import { Field } from "../../../../archive/components/AmountForm/node_modules/redux-form";
import { Button, Form } from "../../../../archive/layouts/investor/node_modules/reactstrap";
import { Input } from "../../components/Input";

export const QueryFormComponent = () => (
  <Form onSubmit={handleSubmit}>
    <fieldset disabled={submitting}>
      <Field
        autocomplete="off"
        component={Input}
        format={(value: string) => (value ? value.substr(4) : "")}
        label="Name"
        name="filter.name.$regex"
        parse={(value: string) => `(?i)${value}`}
        type="text"
      />
      <Field autocomplete="off" component={Input} label="Type" name="filter.type" type="select">
        <option value="">Any</option>
        <option value="Open">Open</option>
        <option value="Limited">Limited</option>
      </Field>
      <Button block color="primary" disabled={anyTouched && !valid} size="lg" type="submit">
        Submit
      </Button>
    </fieldset>
  </Form>
);
