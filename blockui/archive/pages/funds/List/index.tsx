import React from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import {
  Alert,
  Card,
  CardBody,
  Col,
  Row,
} from "../../../../archive/layouts/investor/node_modules/reactstrap";
import { Loader } from "../../components/Loader";
import { PageHeader } from "../../components/PageHeader";
import { amountToString } from "../../utils/units";
import { QueryForm } from "./QueryForm";

export const FundListPageComponent: React.FC = () => (
  <article>
    <Helmet>
      <title>Funds</title>
    </Helmet>
    <PageHeader heading="Funds" />
    <Row>
      <Col sm={4}>
        <Card>
          <CardBody>
            <QueryForm />
            <small>Example implementation of a filter form</small>
          </CardBody>
        </Card>
      </Col>
      <Col>
        {error && <Alert color="danger">{error}</Alert>}
        {loading && <Loader />}
        {funds && (
          <div className="list-group">
            {funds.map((fund) => (
              <Link key={fund.id} className="list-group-item list-group-item-action" to={fund.id}>
                <h5>{fund.name}</h5>
                <dl className="row mb-0">
                  <dt className="col-sm-3">Unit Price</dt>
                  <dd className="col-sm-9">{amountToString(fund.price)}</dd>
                  <dt className="col-sm-3">Type</dt>
                  <dd className="col-sm-9">{fund.type}</dd>
                </dl>
              </Link>
            ))}
          </div>
        )}
      </Col>
    </Row>
  </article>
);
