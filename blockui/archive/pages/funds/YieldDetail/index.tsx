import React from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { faChevronLeft } from "../../../../archive/components/DefaultLayout/node_modules/@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "../../../../archive/components/DefaultLayout/node_modules/@fortawesome/react-fontawesome";
import {
  Alert,
  Button,
  Card,
  CardBody,
  Table,
} from "../../../../archive/layouts/investor/node_modules/reactstrap";
import { Loader } from "../../components/Loader";
import { PageHeader } from "../../components/PageHeader";
import { amountToString } from "../../utils/units";

export const FundYieldDetailsPageComponent = () => {
  const date = new Date(timestamp).toLocaleString();
  return (
    <article>
      <Helmet>
        <title>Yields &gt; Transactions</title>
      </Helmet>
      <PageHeader heading={`Yields: ${date}`}>
        <Button tag={Link} to="./">
          <FontAwesomeIcon icon={faChevronLeft} /> Back
        </Button>
      </PageHeader>
      {error && <Alert color="danger">{error}</Alert>}
      {loading && <Loader />}
      {transactions && (
        <Card>
          <CardBody>
            <Table striped style={{ tableLayout: "fixed" }}>
              <tbody>
                <tr>
                  <th style={{ width: "70%" }}>Recipient</th>
                  <th className="text-right">Amount</th>
                </tr>
                {transactions.map((item) => (
                  <tr key={item.id}>
                    <td className="text-truncate">{item.to}</td>
                    <td className="text-right">{amountToString(item.creditAmount)}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      )}
    </article>
  );
};
