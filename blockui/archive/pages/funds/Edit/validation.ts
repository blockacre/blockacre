import memoize from "lru-memoize";
import { currencies } from "../../../utils/units";
import {
  createValidator,
  integer,
  min,
  minLength,
  number,
  oneOf,
  required,
} from "../../../utils/validation";

const fundValidation = createValidator({
  minimum: [required, integer, min(1)],
  name: [required, minLength(3)],
  "price.amount": [required, number],
  "price.currency": [required, oneOf(currencies)],
  type: [required, oneOf(["Open", "Limited"])],
});

export const validation = memoize(10)(fundValidation);
