import React from "react";
import { loadable } from "../../../utils/loadable";
import { LoadingPage } from "../../utility/LoadingPage";

export const EditFundPage = loadable(() => import("./index"), { fallback: <LoadingPage /> });

export const EditFundPageComponent = loadable(
  () => import("./index").then((pkg) => ({ default: pkg.EditFundPageComponent })),
  { fallback: <LoadingPage /> }
);
