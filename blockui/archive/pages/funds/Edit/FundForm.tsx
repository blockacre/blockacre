import React from "react";
import { Field } from "../../../../archive/components/AmountForm/node_modules/redux-form";
import { Input } from "../../../../archive/components/Input";
import {
  Alert,
  Button,
  Form,
  UncontrolledAlert,
} from "../../../../archive/layouts/investor/node_modules/reactstrap";
import { Money } from "../../../generated/types";
import { decimal, integer } from "../../../utils/normalise";

export type FundFormData = {
  id: string;
  minimum: number;
  name: string;
  price: Money;
  type: string;
};

export const FundFormComponent: React.FC = () => (
  <Form onSubmit={handleSubmit}>
    {!submitting && submitSucceeded && (
      <UncontrolledAlert color="success">Saved successfully</UncontrolledAlert>
    )}
    {error && <Alert color="danger">{error}</Alert>}
    <fieldset disabled={submitting}>
      <Field component="input" name="id" type="hidden" />
      <Field component={Input} label="Name" name="name" type="text" />
      <Field
        component={Input}
        label="Minimum"
        name="minimum"
        normalize={integer}
        pattern="\d+"
        type="text" // show numerical keyboard (number input causes issues with redux-form)
      />
      <Field
        component={Input}
        label="Price"
        name="price.amount"
        normalize={decimal}
        pattern="[\d.]+"
        type="text" // show numerical keyboard (number input causes issues with redux-form)
      />
      <Field component={Input} label="Currency" name="price.currency" type="select">
        <option value="">{}</option>
        <option value="GBP">GBP</option>
        <option value="USD">USD</option>
      </Field>
      <Field component={Input} label="Type" name="type" type="select">
        <option value="">{}</option>
        <option value="Open">Open</option>
        <option value="Limited">Limited</option>
      </Field>
      <Button block color="primary" disabled={pristine || submitting} size="lg" type="submit">
        {submitting ? "Loading..." : "Submit"}
      </Button>
    </fieldset>
  </Form>
);
