import React from "react";
import { Helmet } from "react-helmet";
import { PageHeader } from "../../../../archive/components/layout/PageHeader";
import { Container } from "../../../../archive/layouts/investor/node_modules/reactstrap";
import { ErrorPage } from "../../utility/ErrorPage";
import { LoadingPage } from "../../utility/LoadingPage";
import { FundForm } from "./FundForm";

export const EditFundPageComponent: React.FC = () => {
  if (error) {
    return <ErrorPage error={error} />;
  }
  if (loading) {
    return <LoadingPage />;
  }
  const title = fund ? "Edit Fund" : "Add Fund";
  return (
    <article className="mt-5">
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <Container>
        <PageHeader heading={title} />
        <article>
          <FundForm endpoint={`/funds/${fund ? fund.id : ""}`} initialValues={fund || {}} />
        </article>
      </Container>
    </article>
  );
};
