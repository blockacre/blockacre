import React, { useState } from "react";
import { Helmet } from "react-helmet";
import { useHistory } from "react-router-dom";
import { AmountForm } from "../../components/AmountForm";
import { Loader } from "../../components/Loader";
import { PageHeader } from "../../components/PageHeader";
import { amountToString } from "../../utils/units";
import { faBars, faFileExport, faPoundSign } from "../YieldDetail/node_modules/@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "../YieldDetail/node_modules/@fortawesome/react-fontawesome";
import {
  Alert,
  Button,
  Card,
  CardBody,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Modal,
  ModalBody,
  ModalHeader,
  Table,
  UncontrolledButtonDropdown,
} from "../YieldDetail/node_modules/reactstrap";

export const FundYieldsPageComponent = () => {
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  const history = useHistory();

  return (
    <article>
      <Helmet>
        <title>Yields</title>
      </Helmet>
      <PageHeader heading="Yields">
        <Button color="success" onClick={toggle}>
          <FontAwesomeIcon icon={faPoundSign} /> Pay Yields
        </Button>{" "}
        <UncontrolledButtonDropdown>
          <DropdownToggle caret>
            <FontAwesomeIcon icon={faBars} />
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <FontAwesomeIcon icon={faFileExport} /> Export CSV
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledButtonDropdown>
      </PageHeader>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Pay Yields</ModalHeader>
        <ModalBody>
          <AmountForm
            currency={fund.price.currency}
            endpoint={`/funds/${fund.id}/yield`}
            form="yield"
            successMessage="Yield paid successfully"
          />
        </ModalBody>
      </Modal>
      {error && <Alert color="danger">{error}</Alert>}
      {loading && <Loader />}
      {yields && (
        <Card>
          <CardBody>
            <Table hover striped>
              <tbody>
                <tr>
                  <th>Date/time</th>
                  <th>Investors paid</th>
                  <th>Amount per unit</th>
                  <th>Total amount</th>
                </tr>
                {yields.map((item) => (
                  <tr
                    key={item.id}
                    style={{ cursor: "pointer" }}
                    onClick={() => history.push(item.created)}
                  >
                    <td>{new Date(item.created).toLocaleString()}</td>
                    <td>{item.investorCount}</td>
                    <td>{amountToString(item.amountPerUnit)}</td>
                    <td>{amountToString(item.totalAmount)}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      )}
    </article>
  );
};
