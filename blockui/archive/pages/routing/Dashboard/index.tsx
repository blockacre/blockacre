import React from "react";
import { Redirect } from "react-router";
import { Group } from "../../../generated/types";

export const DashboardPageComponent: React.FC = () => {
  // TODO: would prefer this logic to exist in the routing as part of the RBAC
  if (groups.includes(Group.Consumer)) {
    return <Redirect to="/dashboard/" />;
  }
  if (groups.includes(Group.Vendor)) {
    return <Redirect to="/fund/dashboard/" />;
  }
  return <Redirect to="/login" />;
};
