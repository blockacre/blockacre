import React, { ReactNode } from "react";
import { faLock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Gravatar from "react-gravatar";
import { NavLink as Link } from "react-router-dom";
import { User } from "../../generated/types";
import {
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  UncontrolledDropdown,
} from "../../layouts/investor/node_modules/reactstrap";

type DefaultLayoutProps = {
  children?: ReactNode;
  navigation: ReactNode;
  user: false | User;
}

export const DefaultLayout = ({ children, navigation, user }: DefaultLayoutProps) => (
  <div className="app">
    <Navbar light className="mb-4" color="light" expand="lg">
      <Container>
        <NavbarBrand tag={Link} to="/">
          Blockacre
        </NavbarBrand>
        <NavbarToggler aria-controls="basic-navbar-nav" />
        {navigation}
        {user && (
          <UncontrolledDropdown>
            <DropdownToggle caret nav className="p-0">
              <Gravatar className="rounded-circle" email={user.name} size={35} />
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem tag={Link} to="/logout">
                <FontAwesomeIcon icon={faLock} /> Logout
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        )}
      </Container>
    </Navbar>
    <Container>{children}</Container>
  </div>
);
