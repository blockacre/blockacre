import React from "react";
import { Helmet } from "react-helmet";
import { Wallet } from "../../generated/types";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
} from "../../layouts/investor/node_modules/reactstrap";
import { AmountForm } from "../AmountForm";
import { PageHeader } from "../layout/PageHeader";

type TransfersPageProps = {
  wallet: Wallet;
};

export const TransfersPage = ({ wallet }: TransfersPageProps) => (
  <article>
    <Helmet>
      <title>Transfers</title>
    </Helmet>
    <PageHeader heading="Transfers" />
    <Row>
      <Col lg={6}>
        <Card className="mb-3">
          <CardHeader tag="h5">Withdraw</CardHeader>
          <CardBody>
            <AmountForm
              currency="GBP"
              endpoint="/transactions/withdraw"
              form="withdraw"
              successMessage="Withdrawal successfully submitted"
            />
          </CardBody>
        </Card>
      </Col>
      <Col lg={6}>
        <Card className="mb-3">
          <CardHeader tag="h5">Topup</CardHeader>
          <CardBody>
            <p>
              Use the following details to set up a transfer or standing order. <br />
              Make sure you use the correct reference so we know which account to credit.
            </p>
            <hr />
            <dl>
              <dt>Reference</dt>
              <dd className="form-control form-control-lg mb-3 text-center">{wallet.ref}</dd>
              <dt>Account Name</dt>
              <dd className="form-control form-control-lg mb-3 text-center">Blockacre Ltd</dd>
              <dt>Account Number</dt>
              <dd className="form-control form-control-lg mb-3 text-center">12345678</dd>
              <dt>Sort Code</dt>
              <dd className="form-control form-control-lg mb-3 text-center">00-00-00</dd>
            </dl>
          </CardBody>
        </Card>
      </Col>
    </Row>
  </article>
);
