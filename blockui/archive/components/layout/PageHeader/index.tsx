import React, { HTMLAttributes, PropsWithChildren } from "react";

type PageHeaderProps = {
  heading: string;
} & PropsWithChildren<HTMLAttributes<HTMLElement>>

export const PageHeader = ({ children, className = "", heading, ...props }: PageHeaderProps) => (
  <React.Fragment>
    <div className={`d-flex justify-content-between ${className}`} {...props}>
      <h1 className="display-4 ">{heading}</h1>
      {children && <div>{children}</div>}
    </div>
    <hr />
  </React.Fragment>
);
