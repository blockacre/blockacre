import React, { ComponentType, ReactNode } from "react";
import { InputType } from "reactstrap/lib/Input";
import { WrappedFieldProps } from "redux-form";
import { Col, FormFeedback, FormGroup, Input as InputBS, Label } from "../../layouts/investor/node_modules/reactstrap";

type InputProps = {
  InputComponent: ComponentType<any>;
  autocomplete?: string;
  children?: ReactNode;
  label: string;
  placeholder?: string;
  type: InputType;
} & WrappedFieldProps

export const Input = ({
  children,
  input,
  InputComponent,
  type,
  placeholder,
  autocomplete,
  label,
  meta: { touched, error, invalid },
  ...props
}: InputProps) => (
  <FormGroup row className="mb-4">
    <Label for={input.name} sm={4}>
      {label}
    </Label>
    <Col sm={8}>
      <InputComponent
        {...props}
        {...input}
        autoComplete={autocomplete}
        id={input.name}
        invalid={touched && invalid}
        placeholder={placeholder}
        type={type}
      >
        {children}
      </InputComponent>
      {touched && invalid && <FormFeedback valid={!touched || !error}>{error}</FormFeedback>}{" "}
    </Col>
  </FormGroup>
);

Input.defaultProps = {
  InputComponent: InputBS,
};
