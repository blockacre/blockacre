import React from "react";
import { InputProps } from "reactstrap/lib/Input";
import { Input, InputGroup, InputGroupAddon, InputGroupText } from "../../layouts/investor/node_modules/reactstrap";

type PrefixInputProps = {
  prefix: string;
} & InputProps

export const PrefixInput = ({ prefix, ...props }: PrefixInputProps) => (
  <InputGroup>
    <InputGroupAddon addonType="prepend">
      <InputGroupText>{prefix}</InputGroupText>
    </InputGroupAddon>
    <Input {...props} />
  </InputGroup>
);
