import React, { useEffect } from "react";
import { Field, InjectedFormProps } from "redux-form";
import { apiForm } from "../../containers/ApiFormProvider";
import { Button, Form, UncontrolledAlert } from "../../layouts/investor/node_modules/reactstrap";
import { decimal } from "../../utils/normalise";
import { Input } from "../Input";
import { PrefixInput } from "../Input/PrefixInput";
import { validation } from "./validation";

export type AmountFormData = {
  amount?: number;
  currency?: string;
  id?: string;
};

export type AmountFormProps = {
  currency: string;
  successMessage: string;
};

export const AmountFormComponent = ({
  currency,
  change,
  error,
  handleSubmit,
  anyTouched,
  submitting,
  submitSucceeded,
  successMessage = "Success",
  valid,
}: AmountFormProps & InjectedFormProps<AmountFormData, AmountFormProps>) => {
  useEffect(() => {
    change("currency", currency); // add "hidden" currency
  }, [change, currency]);
  return (
    <Form onSubmit={handleSubmit}>
      {!submitting && submitSucceeded && (
        <UncontrolledAlert color="success">{successMessage}</UncontrolledAlert>
      )}
      {error && <UncontrolledAlert color="danger">{error}</UncontrolledAlert>}
      <fieldset disabled={submitting}>
        <Field
          autocomplete="off"
          component={Input}
          InputComponent={PrefixInput}
          label="Amount"
          name="amount"
          normalize={decimal}
          pattern="[\d.]+" // show numerical keyboard (number input causes issues with redux-form)
          placeholder="Enter amount"
          prefix={currency}
          type="text"
        />
        <Button block color="primary" disabled={anyTouched && !valid} size="lg" type="submit">
          {submitting ? "Loading..." : "Submit"}
        </Button>
      </fieldset>
    </Form>
  );
};

AmountFormComponent.defaultProps = { currency: "GBP" };

const withApiForm = apiForm<AmountFormData, AmountFormProps>({
  validate: validation,
});

export const AmountForm = withApiForm(AmountFormComponent);
