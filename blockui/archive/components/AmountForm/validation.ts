import memoize from "lru-memoize";
import { createValidator, number, required } from "../../utils/validation";

const amountValidation = createValidator({
  amount: [required, number],
});

export const validation = memoize(10)(amountValidation);
