import React from "react";
import { NavLink as Link } from "react-router-dom";
import { Collapse, Nav, NavItem, NavLink } from "reactstrap";

export const Navigation = () => (
  <Collapse navbar id="basic-navbar-nav">
    <Nav navbar className="mr-auto">
      <NavItem>
        <NavLink exact tag={Link} to="/dashboard/">
          Home
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink tag={Link} to="/funds/">
          Invest
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink tag={Link} to="/transfers/">
          Transfers
        </NavLink>
      </NavItem>
    </Nav>
  </Collapse>
);
