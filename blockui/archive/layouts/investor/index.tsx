import React, { ReactNode } from "react";
import { DefaultLayout } from "../../components/DefaultLayout";
import { User } from "../../types";
import { Navigation } from "./Navigation";

type ConsumerLayoutProps = {
  children: ReactNode;
  user: User;
};

export const ConsumerLayout = ({ children, user }: ConsumerLayoutProps) => (
  <DefaultLayout navigation={<Navigation />} user={user}>
    {children}
  </DefaultLayout>
);
