import { RouteConfig } from "react-router-config";
import { ConsumerLayout } from "./layouts/investor";
import { Auth } from "./modules/Auth";
import { ConsumerDashboardPage } from "./containers/ConsumerDashboardPage";
import { ConsumerFundDetailsPage } from "./containers/ConsumerFundDetailsPage/Loadable";
import { ConsumerTransfersPage } from "./containers/ConsumerTransfersPage/Loadable";
import { DashboardPage } from "./containers/DashboardPage";
import { EditFundPage, EditFundPageComponent } from "./containers/EditFundPage/Loadable";
import { FundInvestorsPage } from "./containers/FundInvestorsPage/Loadable";
import { FundListPage } from "./containers/FundListPage/Loadable";
import { FundYieldDetailsPage } from "./containers/FundYieldDetailsPage/Loadable";
import { FundYieldsPage } from "./containers/FundYieldsPage/Loadable";
import { NotFoundPage } from "./containers/NotFoundPage";
import { VendorDashboardPage } from "./containers/VendorDashboardPage";
import { VendorFundDetailsPage } from "./containers/VendorFundDetailsPage/Loadable";
import { VendorFundLayout, VendorLayout } from "./containers/VendorLayout";
import { VendorTransfersPage } from "./pages/VendorTransfersPage/Loadable";
import { injectRoutes } from "../src/utils/injectRoutes";

// see https://github.com/ReactTraining/react-router/issues/5138 for redirects
export const routes: RouteConfig[] = [
  {
    component: DashboardPage,
    exact: true,
    path: "/",
  },
  {
    component: Auth,
    exact: true,
    path: "/login",
  },
  {
    component: injectRoutes(VendorLayout),
    path: "/fund/(dashboard|new)",
    routes: [
      {
        component: VendorDashboardPage,
        exact: true,
        path: "/fund/dashboard",
      },
      {
        component: EditFundPageComponent,
        exact: true,
        path: "/fund/new",
      },
    ],
  },
  {
    component: injectRoutes(VendorFundLayout),
    path: "/fund/:id",
    routes: [
      {
        component: VendorFundDetailsPage,
        exact: true,
        path: "/fund/:id",
      },
      {
        component: EditFundPage,
        exact: true,
        path: "/fund/:id/edit",
      },
      {
        component: VendorTransfersPage,
        exact: true,
        path: "/fund/:id/transfers",
      },
      {
        component: FundInvestorsPage,
        exact: true,
        path: "/fund/:id/investors",
      },
      {
        component: FundYieldsPage,
        exact: true,
        path: "/fund/:id/yields",
      },
      {
        component: FundYieldDetailsPage,
        exact: true,
        path: "/fund/:id/yields/:timestamp",
      },
      {
        component: NotFoundPage,
        path: "*",
      },
    ],
  },
  {
    component: injectRoutes(ConsumerLayout),
    routes: [
      {
        component: ConsumerDashboardPage,
        exact: true,
        path: "/dashboard",
      },
      {
        component: FundListPage,
        exact: true,
        path: "/funds",
      },
      {
        component: ConsumerFundDetailsPage,
        exact: true,
        path: "/funds/:id",
      },
      {
        component: ConsumerTransfersPage,
        exact: true,
        path: "/transfers",
      },
      {
        component: NotFoundPage,
        path: "*",
      },
    ],
  },
];
