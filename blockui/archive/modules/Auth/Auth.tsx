import React from "react";
import {
  AmplifyAuthenticator,
  AmplifySignIn,
  AmplifySignOut,
  AmplifySignUp,
} from "@aws-amplify/ui-react";

export const Auth = () => (
  <AmplifyAuthenticator usernameAlias="email">
    <AmplifySignUp
      formFields={[
        {
          required: true,
          type: "email",
        },
        {
          required: true,
          type: "password",
        },
      ]}
      slot="sign-up"
      usernameAlias="email"
    />
    <AmplifySignIn slot="sign-in" usernameAlias="email" />
    <AmplifySignOut />
  </AmplifyAuthenticator>
);
