import { useEffect, useState } from "react";
import { Auth, CognitoUser } from "@aws-amplify/auth";
import { Group } from "../../../src/generated/types";

type Email = string | null;

type AuthUser = {
  email: Email;
  groups: Array<Group>;
};

const getEmailAndGroups = (cognitoUser: CognitoUser) => {
  // userSession could be null
  const userSession = cognitoUser.getSignInUserSession();
  const auth: AuthUser = {
    email: null,
    groups: [],
  };
  if (userSession) {
    // Grab the user's groups
    auth.groups = userSession.getIdToken().payload["cognito:groups"];
    // And email
    auth.email = cognitoUser.getUsername();
  }
  return auth;
};

export const useAuth = () => {
  const [email, setEmail] = useState(null as Email);
  const [groups, setGroups] = useState([] as Group[]);
  const [errors, setErrors] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const runQuery = async () => {
      try {
        const cognitoUser: CognitoUser = await Auth.currentAuthenticatedUser();
        const auth = getEmailAndGroups(cognitoUser);
        setEmail(auth.email);
        setGroups(auth.groups);
      } catch (error) {
        setErrors(error);
      } finally {
        setIsLoading(false);
      }
    };
    runQuery();
  }, []);
  return { email, errors, groups, isLoading };
};
